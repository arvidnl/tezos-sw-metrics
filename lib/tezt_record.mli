open Tezt

(** A value of [t] represents a Tezt records.

    A record maps Tezt test titles to their file and tags. *)
type t

(** A [test] are the same ones used by [Testowners]. *)
type test = Testowners.test

(** Parse a Tezt record from [json]. *)
val of_json : JSON.t -> t

(** Parse a tezt record from a [file], e.g. [tezt/records/1.json]. *)
val of_file : string -> t

(** [union ?on_conflict t1 t2] is the union of the records [t1] and [t2]

    If both [t1] (resp. [t2]) contains a [test1] (resp. [test2])
    titled [title], then [union t1 t2] will map [title] to
    [on_conflict title test1 test2]. The default [on_conflict] maps
    [title] to [test1] and emits a warning. *)
val union : ?on_conflict:(string -> test -> test -> test option) -> t -> t -> t

(** [unions ?on_conflict ts] is the union of records [ts].

    For details on [?on_conflict] see [union]. *)
val unions : ?on_conflict:(string -> test -> test -> test option) -> t list -> t

(** Parse a tezt record from a set of [files].

    We assume that the set of records therein are non-overlapping.  A
    Tezt warning is emitted if that is not the case, i.e. there are
    two distinct files [f1] and [f2] that both contains some test with
    the same title. *)
val of_files : string list -> t

(** [lookup test_title records] returns the [test] with the title [test_title] in [records]. *)
val lookup : string -> t -> test option
