let starts_with ~prefix s =
  let open String in
  if length prefix > length s then false else sub s 0 (length prefix) = prefix
