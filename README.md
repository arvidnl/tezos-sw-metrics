# Tezos Software Engineering Metrics

Use [Tezt](https://tezos.gitlab.io/developer/tezt.html),
[lib_performance_regression](https://tezos.gitlab.io/developer/tezt.html),
and [ocaml-gitlab](https://github.com/tmcgilchrist/ocaml-gitlab) to
produce software engineering metrics for the
[tezos/tezos](https://gitlab.com/tezos/tezos/)
projects.

## Implemented metrics

All metrics are calculated daily and are visualized at [Nomadic Labs'
Grafana
installation](https://grafana.nomadic-labs.cloud/d/tezos_sw_metrics/tezos-sweng-metrics).

The current set of implemented metrics are:

 - `bin/cloc.ml`: CLOCs per date of various parts of the [tezos/tezos](https://gitlab.com/tezos/tezos/) repo.
 - `bin/merge_metrics.ml`: Merged MRs metrics
   - Number of MRs merged per day grouped by size
   - Average merge time per day
   - Average merge time excluding weekends per, day
 - `bin/open_mrs.ml`: Number of open MRs with/without milestone and draft/non-draft
 - `bin/review_load.ml`: Table with for each developer: MRs assigned to, reviews assigned to and opened MRs by.
 - `bin/pipeline_performance.ml`: A graph of pipeline durations that
   can be filtered by triggering author, source of the pipeline and
   its ref.

## Installation

```shell
$ git submodule update --init
$ opam switch create . 4.14.2 -y --no-install
$ opam install . --deps-only --with-test
$ dune build
```

## Setting up a InfluxDB/Grafana for viewing results

The instructions are basically the same as for [Long Tezts](https://tezos.gitlab.io/developer/long-tezts.html#testing-your-benchmarks-locally).

You'll find the necessary files in
`vendors/lib_performance_regression/local-sandbox`.

In sum:

```shell
docker-compose -f vendors/lib_performance_regression/local-sandbox/docker-compose.yml up -d
```

At this point, the InfluxDB will be empty, but you can access the
Grafana instance at http://localhost:3000.

### Querying Influx Database

Using Docker and the influx command-line client, you can connect to
the sandbox network and the InfluxDB host and query it manually for
debugging purposes with the following command:

```
docker run --network local-sandbox_default -it influxdb:1.8 influx -host influxdb
```

# Configuring InfluxDB/Grafana and collecting metrics

You'll need to set `TEZT_CONFIG`, which instructs Tezt how to
communicate with the InfluxDB / Grafana instance. If you've used the
`local-sandbox` setup described above, then you can set `TEZT_CONFIG`
to the included
`vendors/lib_performance_regression/local-sandbox/tezt_config.json`:

    export TEZT_CONFIG=vendors/lib_performance_regression/local-sandbox/tezt_config.json

Now, to collect all implemented metrics, run:

    dune exec bin/main.exe

Then, browse the results at http://localhost:3000/d/tezos_sw_metrics/tezos-sweng-metrics?orgId=1.

To collect an individual metrics, pass `-f <metric file name>`, e.g.:

    dune exec bin/main.exe -- -f cloc.ml

For convenience, the root of this repo contains a script
`start-grafana-influx.sh` that starts the dockerized InfluxDB/Grafana
and sets up the `TEZT_CONFIG` variable described above. To use it,
execute:

```shell
. ./start-grafana-influx.sh
```

Note the initial `.`, which allows the script to setup the
`TEZT_CONFIG` environment variable.

In the remainder of the README, we'll assume that `TEZT_CONFIG` is properly set.

## Configuring collected metrics

The metrics should be configured with sensible defaults, but can none
the less be tweaked in various metric-specific ways. Except for the
`TEZT_CONFIG` environment variable, all configuration passes through
the Tezt arguments. Tezt arguments are passed by giving a `-a
ARG_NAME=ARG_VALUE` command-line argument, e.g.:


    dune exec bin/main.exe -- -f cloc.ml -a project_id=tezos/tezos

There are two global arguments that apply in all metrics:

 - `project_id` the GitLab project id for which to collect metrics,
   specified as a `namespace/project_name` string. Defaults to
   `tezos/tezos`.
 - `gl_access_token` this argument can be set to a GitLab access
   token, which is necessary to collect metrics from private
   projects. Can also be set through the environment variable
   `GL_ACCESS_TOKEN`.
 - `backfill` this argument should contain an datetime string on the
   format `YYYY-MM-DD HH:MM` (`HH:MM` can be omitted, defaulting to
   `00:00`). It is used to set a starting point for scraping for some
   metrics, if the time series of the given metric is empty. By
   default, data from the last 24 hours is fetched. Additionally, the
   backfill for individual metrics can be overridden (see below).
 - `marge_bot_user` the username by which marge-bot goes under,
   defaults to `nomadic-margebot`. Used by marge-bot-related metrics.

There are also metric-specific configuration values, also specific
through Tezt arguments. They're named `<metric>.<parameter>` and are
detailed below.

## `cloc.ml`: Configuring the CLOCs per date metrics

For the CLOCs per date metrics, you can configure a custom set of
source code views in a JSON file and set Tezt argument `cloc.sources`
to point to it. If `cloc.sources` is not set, it defaults to the
included `tezos-sources.json` file. See the included files
`tezos-sources.json` and `tezos-sources-fast.json` for an illustration
on how to set up custom sources.

In sum, you can run this metric with custom sources as such:

    dune exec bin/main.exe -- -f cloc.ml -a cloc.sources=tezos-sources-fast.json

### Backfilling

You can import historical CLOC data by setting the Tezt argument
`cloc.backfill` to two comma-separated datetimes on `YYYY-MM-DD HH:MM`
format. The first date is a start date and the second is a end
date. CLOCs of each configured source will be sampled at weekly
intervals between the two given dates. In this example, we sample the
sources between `2022-01-01 00:00` and `2022-09-01 23:59`:

    dune exec bin/main.exe -- -f cloc.ml -a cloc.backfill="2022-01-01 00:00,2022-09-01 23:59"

## `merge_metrics.ml`: Configuring merged MRs metrics

This file implements two metrics:

  - Number of MRs merged per day grouped by size
  - Average merge time per day
  - Average merge time per day excluding weekends

These metrics can be configured through the following Tezt arguments:

 - `merge_metrics.mrs_merged_after`: overrides the global argument `backfill`.

Examples:

 - `dune exec bin/main.exe -- -f merge_metrics.ml`
 - `dune exec bin/main.exe -- -f merge_metrics.ml -a merge_metrics.mrs_merged_after="2022-01-01 00:00"`

## `open_mrs.ml`: Configuring the "Number of open MRs with/without milestone and draft/non-draft" metrics

No specific configuration parameters.

Examples:

 - `dune exec bin/main.exe -- -f open_mrs.ml`

## `review_load.ml`: Configuring the "Review load" metrics

Fetches the number of assigned, review-assigned and opened MRs per
author.  It only looks at open MRs, so if a user is not assigned-to,
set as reviewer, nor has an open MR, they will not appear in the result.

Configuration options:

 - `review_load.track_users` a comma-separated list of usernames to
   always include in output, even if they are not implicated in any
   open MR. If unset, defaults to the value of `marge_bot_user` (see
   "Configuring collected metrics").

Examples:

 - `dune exec bin/main.exe -- -f review_load.ml -a review_load.track_users=nomadic-margebot`

## `pipeline_performance.ml`: Graph pipeline durations

Fetches the duration (wall-time and sequential time) of terminated
pipelines.

Configuration options:

 - `pipeline_performance.pipelines_finished_after`: overrides the
   global argument `backfill`.

Examples:

 - `dune exec bin/main.exe -- -f pipelines_performance.ml`

## `retried_tests.ml`: Retried (flaky) tests

Fetches retried (flaky) jobs. A job is considered flaky if fails at
least once, before finally succeeding in the same pipeline. A common
source of flaky CI jobs is flaky tests. The log of flaky jobs is
parsed to obtain more detailed information on the underlying
failure. Typically, this results in a test name.

For each flaky failure, a data point is pushed to the measurement
`tezos_tezos_retried_tests`, with the following fields and tags:

| `tezos_tezos_retried_tests`<br/> fields/tags | Type         | Description                                            |
|----------------------------------------------|--------------|--------------------------------------------------------|
| `timestamp`                                  |              | creation date of the pipeline containing the flaky job |
| `pipeline_id`                                | float        | id of the associated pipeline                          |
| `job_id`                                     | float        | id of the job                                          |
| `job_web_url`                                | string       | web url to the job                                     |
| `sha`                                        | tag          | `sha` the associated pipeline ran on                   |
| `ref`                                        | tag          | `ref` (branch name) of the associated pipeline         |
| `job_name`                                   | tag / string | name of the job                                        |
| `job_user`                                   | tag          | GitLab user that triggered the job                     |
| `failure`                                    | tag          | Contains the test name, if available                   |
| `owner`                                      | tag          | Associated test owner, see below for more info         |

Additionally, this metric pushes one datapoint in the measurement
`tezos_tezos_retried_pipelines` per terminated pipeline, tracking the
number of jobs in that pipeline (not counting retried jobs) and the
number of flaky failures of that pipeline. This datapoint has the
fields and tags:

| `tezos_tezos_retried_pipelines`<br/>fields/tags | Type   | Description                                                      |
|-------------------------------------------------|--------|------------------------------------------------------------------|
| `timestamp`                                     |        | creation date of the pipeline                                    |
| `pipeline_web_url`                              | string | web URL of the pipeline                                          |
| `jobs_total`                                    | float  | total number of jobs in the pipeline, not counting retried jobs. |
| `flakes_count`                                  | float  | total number of flaky jobs in the pipeline.                      |
| `pipeline_id`                                   | float  | id of the pipeline                                               |
| `sha`                                           | tag    | `sha` of the commit the pipeline ran on                          |
| `ref`                                           | tag    | `ref` (branch name) the pipeline ran on                          |
| `status`                                        | tag    | status of the pipeline                                           |

Configuration options:

- `retried_tests.pipelines_finished_after`: overrides the global
  argument `backfill`.
- `retried_tests.pipelines` a comma-separated list of pipeline ids.
  If given, fetch retried tests in the specified pipelines instead of
  looking for retries in all the latest pipelines.
- `retried_tests.pipelines_limit` (default: none) sets the maximum
  number of pipelines to fetch.
- `retried_tests.records_from` from where to fetch records (see
  section [Test owner association] below). can be
  `last-merged-pipeline` (default) or a pipeline id. If set to
  `last-merged-pipeline`, fetch from the most recent merge request
  pipelines from the most recently merged MR. If set to a pipeline id,
  fetch from that pipeline.
- `retried_tests.testowners_ref` from where to fetch
  `TESTOWNERS.json`. By default, TESTOWNERS is fetched from the branch
  associated with a given pipeline. `retried_tests.testowners_ref` can
  be set to an arbitrary commit ref, which is useful for testing.

Examples:

 - `dune exec bin/main.exe -- -f retried_tests.ml`

### Test owner association

For each retried test, the script attempts to associate a test
owner. This feature is currently limited to Tezt tests. Test ownership
is declared using the `TESTOWNERS.json` at the `tezt/TEZTOWNERS.json`
path of the commit on which the flaky test ran (see above for
configuration through the option `retried_tests.testowners_ref`).

This file declares test ownership based on the Tezt tags and `~file`
associated with each test. However, `retried_tests.ml` only parses
GitLab CI job logs and so knows at most the title of the flaky
test. To retrieve the `~file` and tags of the Tezt tags, we use the
*records* that is generated and stored as artifacts in the
`tezos/tezos` CI. To avoid fetching the artifacts for each failed
test, we get the records from latest pipeline merged to
`tezos/tezos`'s master. This is similar to how records are fetched
between pipelines in the `tezos/tezos` CI. See above for configuration
of record fetching through the option `retried_tests.records_from`.

For more details on the `TEZTOWNERS.json` format, see the README of
[the Tezt Test suite in
tezos/tezos](https://gitlab.com/tezos/tezos/-/blob/arvid@add-TESTOWNERS/tezt/README.md#testownersjson).

# Marge-bot related panels

Two marge-bot related panels are produced:

 - A table of marge-bot events, including merges and rejections
 - A graph of marge-bot assignment load, also including merges and
   rejections per time unit

These are grouped in the "Tezos Marge-bot Library panels"
dashboard. This dashboard should not to be confused with the
[Marge-bot
dashboard](https://grafana.nomadic-labs.cloud/d/margebot/margebot?orgId=1&refresh=1h&from=now-2d&to=now),
which is a hand-crafted dashboard. However, the latter includes panels
from the former, which are grouped together for convenience.

The metrics used by the marge-bot panels are all tagged `marge_bot`,
so they can be retrieved more conveniently. For instance, to start
scraping with an empty database (following the instructions from above
in the section "Setting up a InfluxDB/Grafana for viewing results"):

```shell
$ . ./start-grafana-influx.sh
$ dune exec bin/main.exe -- marge_bot
```

The first command will start a Grafana instance and an InfluxDB
instance running locally using Docker Compose. It sets the
`TEZT_CONFIG` environment variable such that Tezt invocation on the
second command knows where to create dashboard and write InfluxDB
datapoints. The last argument for the second command, `marge_bot`,
tells Tezt to run only those metrics that are relevant for the
marge-bot library panels. Once the second command has terminated, you
can see the results Tezos Marge-bot Library panels at
http://localhost:3000/d/tezos_marge_bot/tezos-marge-bot-library-panels?orgId=1.

By default:
 - metrics are fetched for the project [tezos/tezos](https://gitlab.com/tezos/tezos/);
 - the marge-bot user is presumed to be `@nomadic-margebot`;
 - the default branch is presumed to be `master`.

These values can be changed with the tezt arguments `project_id` and
`marge_bot_user`, and `merge_metrics.target_branch`:

```shell
$ dune exec bin/main.exe -- \
    -a gl_access_token="glpat-YOUR_TOKEN"
    -a project_id=nomadic-labs/arvid-tezos \
    -a marge_bot_user=margebot-arvid \
    -a merge_metrics.target_branch=arvid@master \
    marge_bot
```

If you want to backfill metrics from a certain date, add `-a
backfill="YYYY-MM-DD HH:MM"` to the command above. 

The above invocation will fetch metrics from the project
[nomadic-labs/arvid-tezos](https://gitlab.com/tezos/tezos/nomadic-labs/arvid-tezos),
assuming marge-bot goes under the GitLab username `margebot-arvid`
and that the target branch is `arvid@master`.

# Reports

Reports, implemented as Tezt tests, in the folder `reports` send
reports using Slack as configured in the `tezt_config.json` file at
the JSON path `alert.slack_webhook_urls`.

## `retried_tests_report.ml`

This report collects the top `retried_tests_report.limit` flaky tests
in the last `retried_tests_report.period` days per team configured in
the Tezt config.

Configuration options:

 - `retried_tests_report.period` (int): number of days for which to get flaky tests.
 - `retried_tests_report.limit` (int): maximum number of tests to include in the report.
 - `retried_tests_report.dry_run` (bool): if set to true, which is the
   default, does not actually send alerts, just prints them using
   `Log.warn`.

Example:

 - `TEZT_CONFIG=local-sandbox/tezt_config.json dune exec reports/main.exe -- -f retried_tests_report.ml`

# InfluxDB, Grafana and Slack CI configuration

The following CI variables must be set for metrics to be collected,
and reports to be sent, on the scheduled jobs for repository:

 - `INFLUXDB_DOMAIN` / `INFLUXDB_DATABASE` / `INFLUXDB_USERNAME` /
   `INFLUXDB_PASSWORD` / `GRAFANA_DOMAIN` / `GRAFANA_TOKEN`: InfluxDB
   and Grafana configuration. For more information about their
   configuration, see the [Tezt Long Test
   documentation](https://tezos.gitlab.io/developer/long-tezts.html#configuring-and-running-tezt-long-tests).
 - `SLACK_WEBHOOK_URL_DEFAULT`: Where to send alerts (including
   reports) that do not belong to a specific team.
 - `SLACK_WEBHOOK_URL_INFRASTRUCTURE`: Where to send alerts (including
   reports) for team INFRASTRUCTURE. Defaults to
   `SLACK_WEBHOOK_URL_DEFAULT` if not set.
 - `SLACK_WEBHOOK_URL_LAYER1`: Where to send alerts (including
   reports) for team LAYER1. Defaults to `SLACK_WEBHOOK_URL_DEFAULT`
   if not set.
 - `SLACK_WEBHOOK_URL_TEZOS2`: Where to send alerts (including
   reports) for team TEZOS2. Defaults to `SLACK_WEBHOOK_URL_DEFAULT`
   if not set.
 - `SLACK_WEBHOOK_URL_ETHERLINK`: Where to send alerts (including
   reports) for team ETHERLINK. Defaults to
   `SLACK_WEBHOOK_URL_DEFAULT` if not set.
