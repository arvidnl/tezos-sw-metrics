open Tezos_sw_metrics_time

let cli_get_opt f name = Cli.get ~default:None (fun x -> Some (Some (f x))) name

type repo = Path of string | Git of string

let ( >|= ) = Lwt.Infix.( >|= )

type source = {
  name : string;
  description : string;
  repo : repo;
  (* if non-empty list d, only include dirs in [d]. Otherwise, run cloc on full [repo] *)
  include_dirs : string list;
  exclude_dirs : string list;
  args : string list;
}

let source_to_string {name; _} = name

let sources () : source list =
  let source_file =
    cli_get_opt Fun.id "cloc.sources"
    |> Option.value ~default:"tezos-sources.json"
  in
  if not (Sys.file_exists source_file) then
    Test.fail
      ~__LOC__
      "Please provide comma-separated list of source dirs of Tezos repo in \
       environment variable SOURCES"
  else
    let json = JSON.parse_file source_file in
    List.map
      (fun (name, source_json) ->
        let repo =
          match
            JSON.
              ( source_json |-> "git" |> as_string_opt,
                source_json |-> "path" |> as_string_opt )
          with
          | Some url, _ -> Git url
          | _, Some path ->
              if not (Sys.file_exists path) then
                Test.fail ~__LOC__ "The dir %s does not exist" path
              else Path path
          | _ ->
              Test.fail
                ~__LOC__
                "Source %s in %s provides neither `git` nor `path` field"
                name
                source_file
        in
        {
          name;
          repo;
          description = JSON.(source_json |-> "description" |> as_string);
          include_dirs =
            JSON.(
              source_json |-> "include_dirs" |> as_list |> List.map as_string);
          exclude_dirs =
            JSON.(
              source_json |-> "exclude_dirs" |> as_list |> List.map as_string);
          args = JSON.(source_json |-> "args" |> as_list |> List.map as_string);
        })
      (JSON.as_object json)

let measurement source = sf "cloc %s" (source_to_string source)

let test_title source = sf "Tezos `%s` LOCs" (source_to_string source)

let languages =
  [
    "OCaml";
    "WebAssembly";
    "JSON";
    "Python";
    "reStructuredText";
    "JavaScript";
    "TypeScript";
    "HTML";
    "SVG";
    "XML";
    "YAML";
    "Bourne Shell";
    "Markdown";
    "Lisp";
    "make";
    "CSS";
    "C";
    "diff";
    "DOS Batch";
    "Dockerfile";
    "C/C++ Header";
    "TeX";
    "TOML";
    "SQL";
    "INI";
    "Bourne Again Shell";
  ]

let panels () =
  Grafana.(
    [Row "CLOCs"]
    @ List.map
        (fun (source : source) ->
          graphs_per_tags
            ~yaxis_format:" lines"
            ~description:source.description
            ~measurement:(measurement source)
            ~field:"code"
            ~test:(test_title source)
            ~title:(test_title source ^ ": " ^ source.description)
            ~tags:(List.map (fun l -> ("language", l)) languages)
            ~stacking:Normal
            ~fill:InfluxDB.Previous
            ())
        (sources ()))

(* (\* Executor for tests that don't take that long to run. *)
(*    Very long tests (e.g. tests that take days to run) should run on their own executor. *\) *)
(* let default_executors = Long_test.[x86_executor1] *)

let cloc ?(exclude_dirs = []) ?(extra_args = []) ~extra_include_dirs dir =
  let exclude_dir =
    if List.length exclude_dirs > 0 then
      ["--exclude-dir=" ^ String.concat "," exclude_dirs]
    else []
  in
  let args =
    ["--json"] @ exclude_dir @ extra_args @ [dir] @ extra_include_dirs
  in
  let* cloc_results = Process.run_and_read_stdout "cloc" args in
  return
    (JSON.parse ~origin:(sf "cloc %s" (String.concat "," args)) cloc_results)

let cloc_results_empty : JSON.t =
  let zero = `Float 0.0 in
  JSON.annotate ~origin:"cloc_results_empty"
  @@ `O
       (List.map
          (fun language ->
            ( language,
              `O
                [
                  ("nFiles", zero);
                  ("blank", zero);
                  ("comment", zero);
                  ("code", zero);
                ] ))
          ("SUM" :: languages))

let cloc_to_data_points cloc_results commit_hash timestamp source =
  List.filter_map
    (fun (language, value) ->
      if language <> "header" then
        let fields =
          List.map
            (fun (measure, nlines) ->
              (measure, InfluxDB.Float (float_of_int @@ JSON.as_int nlines)))
            (JSON.as_object value)
        in
        let other_fields = List.filter (fun (key, _) -> key <> "code") fields in
        let tags =
          [("language", language)]
          @ Option.fold
              ~none:[]
              ~some:(fun commit_hash -> [("sha", commit_hash)])
              commit_hash
        in
        let data_point =
          InfluxDB.data_point
            ~timestamp
            ~tags
            ~other_fields
            (measurement source)
            ("code", List.assoc "code" fields)
        in
        Some data_point
      else None)
    (JSON.as_object cloc_results)

let test_clocs ~executors =
  let backfill =
    cli_get_opt
      (fun backfill ->
        match String.split_on_char ',' backfill with
        | [date_start; date_end] ->
            Log.info "Will backfill from %s to %s" date_start date_end ;
            let reqtime = false in
            ( Time.datetime_of_string ~reqtime date_start,
              Time.datetime_of_string ~reqtime date_end )
        | _ ->
            Test.fail
              "Tezt argument variable 'backfill' must be a comma-separated \
               tuple of start datetime, end datetime on %%Y%%M%%DT%%h%%m%%s \
               format")
      "cloc.backfill"
  in
  List.iter
    (fun source ->
      Long_test.register
        ~__FILE__
        ~executors
        ~title:(test_title source)
        ~tags:["sw_metrics"; "lines"]
        ~timeout:(Hours 1)
      @@ fun () ->
      let* source_path =
        match source.repo with
        | Path s -> return s
        | Git url ->
            (*             let depth, shallow_since = *)
            (*               match backfill with *)
            (*               | Some (date_start, _) -> *)
            (*                   (None, Some (ISO8601.Permissive.Time.string_of_datetime date_start)) *)
            (*               | None -> (Some 1, None) *)
            (*             in *)
            Repo_cache.get url
      in
      let cloc_source source =
        let dir, extra_include_dirs =
          match source.include_dirs with
          | include_dir :: include_dirs ->
              let prefix d = source_path ^ "/" ^ d in
              (prefix include_dir, List.map prefix include_dirs)
          | [] -> (source_path, [])
        in
        let extra_include_dirs =
          List.filter
            (fun dir ->
              if Sys.file_exists dir then true
              else (
                Log.warn
                  "The folder %s does not exist, removing from cloc invocation"
                  dir ;
                false))
            extra_include_dirs
        in
        if not (Sys.file_exists dir) then (
          Log.warn
            "The folder %s does not exist, will return dummy empty result"
            dir ;
          return cloc_results_empty)
        else
          cloc
            ~extra_args:source.args
            ~exclude_dirs:source.exclude_dirs
            ~extra_include_dirs
            dir
      in
      match (backfill, source.repo) with
      | Some (date_start, date_end), Git _ ->
          let sub_week ts =
            let tm = Unix.gmtime ts in
            fst (Unix.mktime {tm with tm_mday = tm.tm_mday - 7})
          in
          let rec loop timestamp f =
            if timestamp >= date_start then
              let* b = f timestamp in
              if b then loop (sub_week timestamp) f else unit
            else unit
          in
          loop date_end @@ fun timestamp ->
          Log.info "Sampling at %s" (Time.string_of_datetime timestamp) ;
          let* commit_hash =
            Git.log
              ~git_path:source_path
              ~max_count:1
              ~before:(Time.string_of_datetime timestamp)
              ~format:"%H"
              ()
            >|= String.trim
          in
          if commit_hash = "" then (
            Log.warn "No commits before: %s" (Time.string_of_datetime timestamp) ;
            Lwt.return_false)
          else
            let* () = Git.checkout ~git_path:source_path commit_hash in
            let* cloc_results = cloc_source source in
            let sum_code = JSON.(cloc_results |-> "SUM" |-> "code" |> as_int) in
            let* commit_subject =
              Git.log
                ~git_path:source_path
                ~max_count:1
                ~format:"%s"
                ~ref_:commit_hash
                ()
              >|= String.trim
            in
            Log.info
              "[%s] %s: %d lines of code (%s)"
              commit_hash
              (Time.string_of_datetime timestamp)
              sum_code
              commit_subject ;
            List.iter
              Long_test.add_data_point
              (cloc_to_data_points
                 cloc_results
                 (Some commit_hash)
                 timestamp
                 source) ;
            Lwt.return_true
      | backfill, repo ->
          (match (backfill, repo) with
          | Some _, Path _ ->
              Log.warn "Will not backfill, this is not a git source"
          | _ -> ()) ;
          let timestamp = Unix.gettimeofday () in
          let* commit_hash =
            if Sys.file_exists @@ source_path ^ "/.git" then
              let* commit = Git.get_current_commit ~git_path:source_path () in
              return (Some commit)
            else return None
          in
          let* cloc_results = cloc_source source in
          List.iter
            Long_test.add_data_point
            (cloc_to_data_points cloc_results commit_hash timestamp source) ;
          Lwt.return_unit)
    (sources ())
