open Tezt
open Tezt.Base
open Tezos_sw_metrics

let () =
  Test.register
    ~__FILE__
    ~title:"TESTOWNERS: test [testowner] function"
    ~tags:["testowners"]
  @@ fun () ->
  let open Testowners in
  let testowners : testowners =
    [
      ( "l1",
        {
          tags = ["dac"; "dal"];
          path_patterns = [rex "src/proto_.*/lib_protocol/.*"];
        } );
      ( "infrastructure",
        {tags = ["binaries"]; path_patterns = [rex "^src/lib_clic"]} );
    ]
  in
  let test1 : test =
    {
      title = "my protocol test";
      file = "src/proto_alpha/lib_protocol/test/main.ml";
      tags = ["the_first_layer"];
    }
  in
  let test2 : test =
    {title = "my binaries test"; file = "tezt/test.ml"; tags = ["binaries"]}
  in
  let test3 : test =
    {title = "my random"; file = "tezt/foobar.ml"; tags = []}
  in
  let test4 : test =
    {title = "my both test"; file = "tezt/test.ml"; tags = ["binaries"; "dac"]}
  in
  let test_proto1 : test =
    {
      title = "my proto test 1";
      file = "src/proto_alpha/lib_protocol/foobar";
      tags = [];
    }
  in
  let test_proto2 : test =
    {
      title = "my proto test 1";
      file = "src/proto_018/lib_protocol/foobar";
      tags = [];
    }
  in
  (match testowner testowners test1 with ["l1"] -> () | _ -> assert false) ;
  (match testowner testowners test2 with
  | ["infrastructure"] -> ()
  | _ -> assert false) ;
  (match testowner testowners test3 with [] -> () | _ -> assert false) ;
  (match testowner testowners test4 with
  | ["l1"; "infrastructure"] -> ()
  | _ -> assert false) ;
  (match testowner testowners test_proto1 with
  | ["l1"] -> ()
  | _ -> assert false) ;
  (match testowner testowners test_proto2 with
  | ["l1"] -> ()
  | _ -> assert false) ;
  unit

let test_owners_typ : Testowners.t Check.typ =
  let pp fmt testowners =
    let pp_ownership fmt Testowners.{tags; path_patterns} =
      Format.fprintf
        fmt
        {|{tags: %s, path_patterns: %s}|}
        (String.concat ", " tags)
        (String.concat ", " (List.map Base.show_rex path_patterns))
    in
    Format.fprintf fmt "{\n" ;
    List.iter
      (fun (owner, ownership) ->
        Format.fprintf fmt {|  %s: %a,\n|} owner pp_ownership ownership)
      testowners ;
    Format.fprintf fmt "}\n"
  in
  let equal = ( = ) in
  Check.equalable pp equal

let () =
  Test.register
    ~__FILE__
    ~title:"TESTOWNERS: test [of_json] function"
    ~tags:["testowners"]
  @@ fun () ->
  let test_owners_empty = Temp.file "test_owners.json" in
  Base.write_file
    ~contents:
      {|
{
  "example_owner1": {},
  "example_owner2": {"tags": ["foobar"], "path_patterns": ["my_.*_pattern"]}
}
|}
    test_owners_empty ;
  let test_owners_empty = Testowners.of_file test_owners_empty in
  Check.(
    test_owners_empty
    = [
        ("example_owner1", Testowners.{tags = []; path_patterns = []});
        ( "example_owner2",
          Testowners.{tags = ["foobar"]; path_patterns = [rex "my_.*_pattern"]}
        );
      ])
    ~__LOC__
    test_owners_typ
    ~error_msg:"Expected %R, got %L" ;
  unit

let () =
  Test.register
    ~__FILE__
    ~title:"Tezt Records: test parsing and lookup"
    ~tags:["tezt_records"]
  @@ fun () ->
  let records1 = Temp.file "records1.json" in
  let records2 = Temp.file "records2.json" in
  Base.write_file
    records1
    ~contents:
      {| [{
    "file": "protocol > unit",
    "title": "[Unit] alpha: merkle list",
    "tags": [
      "alcotest"
    ],
    "successful_runs": {
      "total_time": "3952",
      "count": "1"
    }
  }] |} ;
  Base.write_file
    records2
    ~contents:
      {| [{
    "file": "protocol > unit",
    "title": "[Unit] 016-PtMumbai: dal slot proof",
    "tags": [
      "alcotest"
    ],
    "successful_runs": {
      "total_time": "716912",
      "count": "1"
    }
  }] |} ;
  let m = Tezt_record.of_files [records1; records2] in
  match Tezt_record.lookup "[Unit] 016-PtMumbai: dal slot proof" m with
  | Some
      {
        file = "protocol > unit";
        title = "[Unit] 016-PtMumbai: dal slot proof";
        tags = ["alcotest"];
      } ->
      unit
  | _ -> assert false

let () = Test.run ()
