open Tezos_sw_metrics.Util
open Tezos_sw_metrics.Mtc_metrics

let format_mr_metrics ?created_after ?created_before ?order_by ?sort ?limit
    ~token ~repository project_id format mrs =
  ignore repository ;
  ignore token ;
  ignore format ;
  let open Gitlab in
  let open Monad in
  let filters =
    let show_order = function
      | `Created_at -> "updated_at"
      | `Title -> "user_id"
      | `Updated_at -> "updated_at"
    in
    (match limit with None -> [] | Some limit -> [sf "limit=%d" limit])
    @ (match created_after with
      | None -> []
      | Some created_after -> [sf "created_after=%s" created_after])
    @ (match created_before with
      | None -> []
      | Some created_before -> [sf "created_before=%s" created_before])
    @ (match order_by with
      | None -> []
      | Some order_by -> [sf "order_by=%s" (show_order order_by)])
    @
    match sort with
    | None -> []
    | Some sort -> [sf "sort=%s" (Gitlab_j.string_of_sort sort)]
  in
  let* mrs = Stream.to_list mrs in
  let* () =
    eft
      "MR metrics for merge requests in project %d, got %d MRs %s\n"
      project_id
      (List.length mrs)
      (match filters with
      | [] -> ""
      | _ -> sf " [%s]" (String.concat ", " filters))
  in
  let print_row_raw ~separator row = pft "%s\n" (String.concat separator row) in
  let print_row ~format ~cols ~separator content =
    let col_content =
      List.map
        (function Ex_col col -> col.format format @@ col.of_metrics content)
        cols
    in
    print_row_raw ~separator col_content
  in
  match format with
  | CSV ->
      let separator = "," in
      let* () =
        print_row_raw ~separator
        @@ List.map (function Ex_col c -> c.title) cols
      in
      (*       Stream.iter (fun mr -> print_row ~format:CSV ~cols ~separator mr) mrs *)
      monad_list_iter mrs (fun mr -> print_row ~format:CSV ~cols ~separator mr)

let () =
  let limit = ref None in
  (* tezos/tezos *)
  let project_id = ref None in
  let local_repo_path = ref None in
  let token = ref None in
  let created_after = ref None in
  let created_before = ref None in
  let order_by = ref None in
  let sort = ref None in
  let format = ref CSV in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        "<TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        "<PROJECT_ID> Numeric GitLab project id." );
      ( "--local-repo",
        Arg.String (fun s -> local_repo_path := Some s),
        "<TOKEN> GitLab private token." );
      ( "--created-after",
        Arg.String (fun date -> created_after := Some date),
        "Only include merge requests created after" );
      ( "--created-before",
        Arg.String (fun date -> created_before := Some date),
        "Only include merge requests created before" );
      ( "--order-by",
        Arg.String
          (fun order ->
            let order =
              match order with
              | "created_at" -> `Created_at
              | "title" -> `Title
              | "updated_at" -> `Updated_at
              | _ -> assert false
            in
            order_by := Some order),
        "Order results by" );
      ( "--sort",
        Arg.String
          (fun sort_dir ->
            let sort_dir =
              match sort_dir with
              | "asc" -> `Asc
              | "desc" -> `Desc
              | _ -> assert false
            in
            sort := Some sort_dir),
        "Order results by" );
      ( "--limit",
        Arg.Int
          (fun l ->
            if l < 0 then raise (Invalid_argument "--limit must be positive")
            else limit := Some l),
        sf
          "<LIMIT> Only search up to this merge requests pipelines (defaults \
           to no limit)" );
      ( "--format",
        Arg.String
          (function
          (*           | "md" | "markdown" -> format := Markdown *)
          (*           | "ascii" -> format := Ascii *)
          (*           | "json" -> format := JSON *)
          | "csv" -> format := CSV
          | _ -> raise @@ Invalid_argument "--format must be either 'csv'"),
        " Format. Can be either 'csv'." );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options]\nOptions are:" Sys.argv.(0)
  in
  Arg.parse
    args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg ;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  let limit = !limit in
  let format = !format in
  let created_after = !created_after in
  let created_before = !created_before in
  let order_by = !order_by in
  let sort = !sort in
  let local_repo_path =
    match !local_repo_path with
    | Some local_repo_path -> local_repo_path
    | None -> failwith "Please pass --local-repo"
  in
  Lwt_main.run
  @@ Gitlab.Monad.(
       run
         (let mrs =
            get_mr_metrics
              ?created_after
              ?created_before
              ?order_by
              ?sort
              ?limit
              ~token
              ~repository:local_repo_path
              project_id
          in
          format_mr_metrics
            ?created_after
            ?created_before
            ?order_by
            ?sort
            ?limit
            ~token
            ~repository:local_repo_path
            project_id
            format
            mrs))
