(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

module type LOGGER = sig
  val info : ('a, Format.formatter, unit, unit Gitlab.Monad.t) format4 -> 'a

  val debug : ('a, Format.formatter, unit, unit Gitlab.Monad.t) format4 -> 'a
end

type logger = (module LOGGER)

(* a job (identified by its name) is 'retried' if it appears several times.
   it is flaky if it appears once with  'success'  and once with 'failed'
*)
(* should return [(job_name, [job_successes], [job_failures])] *)
type flake_result = {
  job_name : string;
  job_successes : Gitlab_t.pipeline_job list;
  job_failures : (Gitlab_t.pipeline_job * Tezos_gitlab.extra_info option) list;
}

type retried_results = {
  (* Flaky jobs in the pipeline. A job is flaky in the pipeline if it
     failed, was retried a number of times, and was finally
     successful. *)
  flakes : flake_result list;
  (* Total number of jobs in the pipeline, not including retries *)
  jobs_total : int;
}

val get_flaky_of_pipeline :
  logger ->
  ?failure_reason:Gitlab_t.pipeline_job_failure_reason ->
  ?per_page:int ->
  token:Gitlab.Token.t ->
  project_id:int ->
  pipeline_id:int ->
  unit ->
  retried_results Gitlab.Monad.t

val get_retried :
  logger ->
  ?per_page:int ->
  ?rebased_on:string ->
  ?failure_reason:Gitlab_t.pipeline_job_failure_reason ->
  ?created_after:float ->
  ?updated_after:float ->
  ?updated_before:float ->
  ?order_by:[`Id | `Ref | `Status | `Updated_at | `User_id] ->
  ?sort:Gitlab_t.sort ->
  ?username:string ->
  token:Gitlab.Token.t ->
  ?limit:int ->
  project_id:int ->
  unit ->
  (Gitlab_t.pipeline * retried_results) list Gitlab.Monad.t
