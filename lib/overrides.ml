open Tezt
open Tezt_performance_regression

module Color = struct
  type t =
    | Super_light_red
    | Light_red
    | Red
    | Semi_dark_red
    | Dark_red
    | Super_light_orange
    | Light_orange
    | Orange
    | Semi_dark_orange
    | Dark_orange
    | Super_light_yellow
    | Light_yellow
    | Yellow
    | Semi_dark_yellow
    | Dark_yellow
    | Super_light_green
    | Light_green
    | Green
    | Semi_dark_green
    | Dark_green
    | Super_light_blue
    | Light_blue
    | Blue
    | Semi_dark_blue
    | Dark_blue
    | Super_light_purple
    | Light_purple
    | Purple
    | Semi_dark_purple
    | Dark_purple
    | Rgb of (int * int * int)

  let to_alias = function
    | Super_light_red -> "super-light-red"
    | Light_red -> "light-red"
    | Red -> "red"
    | Semi_dark_red -> "semi-dark-red"
    | Dark_red -> "dark-red"
    | Super_light_orange -> "super-light-orange"
    | Light_orange -> "light-orange"
    | Orange -> "orange"
    | Semi_dark_orange -> "semi-dark-orange"
    | Dark_orange -> "dark-orange"
    | Super_light_yellow -> "super-light-yellow"
    | Light_yellow -> "light-yellow"
    | Yellow -> "yellow"
    | Semi_dark_yellow -> "semi-dark-yellow"
    | Dark_yellow -> "dark-yellow"
    | Super_light_green -> "super-light-green"
    | Light_green -> "light-green"
    | Green -> "green"
    | Semi_dark_green -> "semi-dark-green"
    | Dark_green -> "dark-green"
    | Super_light_blue -> "super-light-blue"
    | Light_blue -> "light-blue"
    | Blue -> "blue"
    | Semi_dark_blue -> "semi-dark-blue"
    | Dark_blue -> "dark-blue"
    | Super_light_purple -> "super-light-purple"
    | Light_purple -> "light-purple"
    | Purple -> "purple"
    | Semi_dark_purple -> "semi-dark-purple"
    | Dark_purple -> "dark-purple"
    | Rgb (r, g, b) -> Base.sf "#%02x%02x%02x" r g b

  let to_json color = `String (to_alias color)
end

module Match = struct
  type t = JSON.u

  let by_name name = `O [("id", `String "byName"); ("options", `String name)]
end

module Properties = struct
  type t = JSON.u

  let make id value : t = `O [("id", `String id); ("value", value)]

  let display_mode mode = make "custom.displayMode" @@ `String mode

  let hidden hidden = make "custom.hidden" @@ `Bool hidden

  let filterable filterable = make "custom.filterable" @@ `Bool filterable

  let links ?(targetBlank = true) ~title url =
    make "links"
    @@ `A
         [
           `O
             [
               ("title", `String title);
               ("url", `String url);
               ("targetBlank", `Bool targetBlank);
             ];
         ]

  let display_name name =
    `O [("id", `String "displayName"); ("value", `String name)]

  type axis_placement = Auto | Left | Right | Hidden

  let axis_placement placement =
    make "custom.axisPlacement"
    @@ `String
         (match placement with
         | Auto -> "auto"
         | Left -> "left"
         | Right -> "right"
         | Hidden -> "hidden")

  let draw_style (style : Grafana.drawstyle) =
    make "custom.drawStyle"
    @@ `String
         (match style with
         | Lines -> "lines"
         | Bars -> "bars"
         | Points -> "points")

  let unit u = make "unit" @@ `String u

  let line_width i = make "line_width" @@ `Float (float_of_int i)

  type stacking_mode = Normal | Percent (* Called '100%' in the interface *)

  let stacking ?(mode = Normal) ?(group = "A") () =
    make "custom.stacking"
    @@ `O
         [
           ( "mode",
             `String
               (match mode with Normal -> "normal" | Percent -> "percent") );
           ("group", `String group);
         ]

  let fixed_color color =
    make "color"
    @@ `O [("mode", `String "fixed"); ("fixedColor", Color.to_json color)]

  let column_width width = make "custom.width" @@ `Float (float_of_int width)

  type cell_type =
    | Auto
    | Sparkline
    | Color_text
    | Color_background
    | Gauge
    | JSON_view
    | Image

  let cell_type cell_type =
    let show_cell_type = function
      | Auto -> "auto"
      | Sparkline -> "sparkline"
      | Color_text -> "color-text"
      | Color_background -> "color-background"
      | Gauge -> "gauge"
      | JSON_view -> "json-view"
      | Image -> "image"
    in
    make "custom.cellOptions"
    @@ `O [("type", `String (show_cell_type cell_type))]

  type special_value = Null

  type mapping =
    | Value of (string * string * Color.t option) list
    | Special of (special_value * string * Color.t option)
    | Regex of (string * string * Color.t option)

  let mappings mappings : t =
    let next =
      let index = ref 0 in
      fun () ->
        incr index ;
        !index
    in
    let mapping text color_opt =
      `O
        ([("text", `String text); ("index", `Float (float_of_int (next ())))]
        @
        match color_opt with
        | Some c -> [("color", Color.to_json c)]
        | None -> [])
    in
    let special_mapping (special_value, text, color_opt) : t =
      let options : t =
        `O
          [
            ("match", `String (match special_value with Null -> "null"));
            ("result", mapping text color_opt);
          ]
      in
      `O [("type", `String "special"); ("options", options)]
    in
    let regex_mapping (pattern, text, color_opt) : t =
      let options : t =
        `O [("pattern", `String pattern); ("result", mapping text color_opt)]
      in
      `O [("type", `String "regex"); ("options", options)]
    in
    let value_mapping mappings : t =
      let options : t =
        `O
          (List.map
             (fun (value, text, color_opt) -> (value, mapping text color_opt))
             mappings)
      in
      `O [("type", `String "value"); ("options", options)]
    in
    make "mappings"
    @@ `A
         (List.map
            (function
              | Value mappings -> value_mapping mappings
              | Special mapping -> special_mapping mapping
              | Regex mapping -> regex_mapping mapping)
            mappings)

  let hide_in_area ?(viz = false) ?(legend = false) ?(tooltip = false) () =
    make "custom.hideFrom"
    @@ `O
         [
           ("viz", `Bool viz);
           ("legend", `Bool legend);
           ("tooltip", `Bool tooltip);
         ]
end

type override = JSON.u

let make matcher properties =
  `O [("matcher", matcher); ("properties", `A properties)]

let ( ==> ) matcher properties = make matcher properties

let of_list = function [] -> None | overrides -> Some (`A overrides)
