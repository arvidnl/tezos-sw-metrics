open Tezt

(** [Testowners] enables the parsing and querying of [TESTOWNERS.json] files.

    This format is described {{:
    https://gitlab.com/tezos/tezos/-/blob/master/tezt/README.md#declaring-test-ownership-with-testownersjson
    } in the README of Tezos Tezt suite} *)

(** An [ownership] is a set of Tezt test tags, path prefixes and patterns. *)
type ownership = {tags : string list; path_patterns : Base.rex list}

(** A [TESTOWNERS] file is a sequence of [owner] to [ownership] mappings *)
type testowners = (string * ownership) list

type t = testowners

(** [of_json json] parses a [TESTOWNERS.json] json value [json] *)
val of_json : JSON.t -> testowners

(** [of_file p] parses a [TESTOWNERS.json] file at [p] *)
val of_file : string -> testowners

(** [test] is the meta-data of a test. *)
type test = {title : string; file : string; tags : string list}

(** [testowner testowners test] returns the owners of [test] as per [testowners].

    If there are no owners, [[]] is returned. Otherwise, the result
    will contain, in order of appearance, all owners of [testowners]
    that declare ownership of [test]. *)
val testowner : testowners -> test -> string list
