(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

let assignments username =
  sf
    "https://gitlab.com/tezos/tezos/-/merge_requests?scope=all&state=opened&assignee_username=%s&not[author_username]=%s"
    username
    username

let reviews username =
  sf
    "https://gitlab.com/tezos/tezos/-/merge_requests?scope=all&state=opened&reviewer_username=%s&not[author_username]=%s"
    username
    username

let author_of username =
  sf
    "https://gitlab.com/tezos/tezos/-/merge_requests?scope=all&state=opened&author_username=%s"
    username

let user username = sf "https://gitlab.com/%s" username

let merge_request mr_iid =
  sf "https://gitlab.com/tezos/tezos/-/merge_requests/" ^ mr_iid

let pipeline pipeline_iid =
  sf "https://gitlab.com/tezos/tezos/-/pipelines/" ^ pipeline_iid
