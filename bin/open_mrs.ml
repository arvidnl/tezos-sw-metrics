(* This metrics collects the number of MRs open for a given project,
   and groups them by [milestone=None] / [milestone<>None] and
   [draft=yes] / [draft=no]. *)

let measurement = "tezos_tezos_open_mrs"

let test_title = "tezos/tezos open MRs"

let mr_categories =
  [
    ( "Number of MRs with no milestone, non-draft",
      "no_milestone_non_draft_mrs",
      false,
      false );
    ( "Number of MRs with no milestone, draft",
      "no_milestone_draft_mrs",
      false,
      true );
    ( "Number of MRs with a milestone, non-draft",
      "has_milestone_non_draft_mrs",
      true,
      false );
    ( "Number of MRs with a milestone, draft",
      "has_milestone_draft_mrs",
      true,
      true );
  ]

let panels () =
  Grafana.(
    [Row "Open MRs"]
    @ [
        graphs_per_tags
          ~yaxis_format:" MRs"
          ~description:
            "Number of open MRs, grouped by milestone and draft status"
          ~measurement
          ~field:"count"
          ~test:test_title
          ~title:"Open MRs"
          ~tags:
            (List.map
               (fun (_, category_tag, _, _) -> ("category", category_tag))
               mr_categories)
          ~stacking:Normal
          ~fill:InfluxDB.Previous
          ~tooltip_mode:Multi
          ();
      ])

let register ~executors =
  Long_test.register
    ~__FILE__
    ~executors
    ~title:test_title
    ~tags:["sw_metrics"; "merge_requests"; "open_mrs"]
    ~timeout:(Hours 1)
  @@ fun () ->
  let Tezt_cli_arg.
        {gl_access_token = token; project_short_ref; marge_bot_user = _} =
    Tezt_cli_arg.get_shared_opts ()
  in

  let* project =
    Gitlab.Monad.run
    @@
    let open Gitlab.Monad in
    let* projects =
      Gitlab.Project.by_short_ref ?token ~short_ref:project_short_ref ()
    in
    match Gitlab.Response.value projects with
    | Some project -> return project
    | None -> Test.fail "Could not find project %s" project_short_ref
  in
  Log.info
    "Fetching metrics from project %s: %d"
    project.project_short_name_with_namespace
    project.project_short_id ;

  let* () =
    Gitlab.Monad.(
      run
      @@
      let poll_total_exn name s =
        let* first = Gitlab.Stream.next s in
        match first with
        | Some (_val, s') -> (
            match Gitlab.Stream.total s' with
            | Some total -> return total
            | None -> Test.fail "Found no total for %s" name)
        | None -> return 0
      in

      let mrs_web_url ~milestone ~draft =
        let milestone_title = if milestone then "Any" else "None" in
        let draft = if draft then "yes" else "no" in
        sf
          "https://gitlab.com/%s/-/merge_requests?scope=all&state=opened&milestone_title=%s&draft=%s"
          project_short_ref
          milestone_title
          draft
      in

      Util.monad_list_iter_p mr_categories
      @@ fun (description, category_tag, milestone, draft) ->
      let mrs =
        Gitlab.Project.merge_requests
          ?token
          ~milestone:(if milestone then "any" else "none")
          ~wip:draft
          ~state:`Opened
          ~target_branch:"master"
          ~id:project.project_short_id
          ()
      in
      let* count = poll_total_exn category_tag mrs in
      Log.info "%s: %d (%s)" description count (mrs_web_url ~milestone ~draft) ;
      let data_point =
        InfluxDB.data_point
          ~timestamp:(Unix.gettimeofday ())
          ~tags:[("category", category_tag)]
          measurement
          ("count", InfluxDB.Float (float_of_int count))
      in
      Long_test.add_data_point data_point ;
      return ())
  in
  unit
