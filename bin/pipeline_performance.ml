open Tezos_sw_metrics_time

(* This metrics collects statistics on pipeilne. *)

let test_title = "tezos/tezos pipeline performance"

let measurement_pipelines = "tezos_tezos_pipeline_performance_pipelines"

let Tezt_cli_arg.{gl_access_token = token; project_short_ref; marge_bot_user} =
  Tezt_cli_arg.get_shared_opts ()

(* Pipelines. What information do we need?

   - [x] User+
   - [x] Sha
   - [x] Ref
   - [x] Number of jobs
   - [x] Source
   - [x] Duration+
   - [x] Queued duration+
   - [x] Started at+
   - [x] Created at+
   - [x] Finished at+
   - [x] Id
   - [x] Web_url?
   - [x] project_id
   - [x] Status
   - [x] (is an opam pipeline)
   - [x] number of retries

   (+) requires the full pipeline
*)

(* Jobs: what information do we need?

   - [ ] User
   - [ ] Sha
   - [ ] Ref
   - [ ] Duration
   - [ ] Queued duration
   - [ ] Created at
   - [ ] Started at
   - [ ] Finished at
   - [ ] Id
   - [ ] Pipelien id
   - [ ] Web_url?
   - [ ] project_id
   - [ ] Status
   - [ ] artifacts_file

   (+) requires the full pipeline
*)

module Pipeline_type = struct
  (* Based on https://gitlab.com/tezos/pipeline-profiler/-/issues/1 *)
  type t =
    | Before_merging
    | Before_merging_opam
    | Before_merging_marge
    | Before_merging_marge_opam
    | Beta_release_tag
    | Latest_release_test
    | Latest_release
    | Master_branch
    | Release_tag_test
    | Release_tag
    | Schedule_extended_test
    | Merge_train
    | Unknown

  let to_string = function
    | Before_merging -> "before_merging"
    | Before_merging_opam -> "before_merging_opam"
    | Before_merging_marge -> "before_merging_marge"
    | Before_merging_marge_opam -> "before_merging_marge_opam"
    | Beta_release_tag -> "beta_release_tag"
    | Latest_release_test -> "latest_release_test"
    | Latest_release -> "latest_release"
    | Master_branch -> "master_branch"
    | Release_tag_test -> "release_tag_test"
    | Release_tag -> "release_tag"
    | Schedule_extended_test -> "schedule_extended_test"
    | Merge_train -> "merge_train"
    | Unknown -> "unknown"

  let all =
    [
      Before_merging;
      Before_merging_opam;
      Before_merging_marge;
      Before_merging_marge_opam;
      Beta_release_tag;
      Latest_release_test;
      Latest_release;
      Master_branch;
      Release_tag_test;
      Release_tag;
      Schedule_extended_test;
      Merge_train;
      Unknown;
    ]

  let type_of_pipeline :
      default_project_id:int ->
      Gitlab_t.single_pipeline ->
      Gitlab_t.pipeline_job list ->
      t =
   fun ~default_project_id pipeline pipeline_jobs ->
    (* based on the workflow rules in [.gitlab-ci.yml] *)
    if pipeline.project_id = default_project_id then
      match pipeline.source with
      | `Merge_Request_Event -> (
          let is_merge_train =
            pipeline.ref =~ rex "refs\\/merge-requests\\/\\d+\\/train"
          in
          let is_marge = pipeline.user.user_short_username = marge_bot_user in
          let has_opam =
            List.exists
              (fun (pipeline_job : Gitlab_t.pipeline_job) ->
                String.starts_with ~prefix:"opam:" pipeline_job.name)
              pipeline_jobs
          in
          match (is_merge_train, is_marge, has_opam) with
          | true, _, _ -> Merge_train
          | false, false, false -> Before_merging
          | false, false, true -> Before_merging_opam
          | false, true, false -> Before_merging_marge
          | false, true, true -> Before_merging_marge_opam)
      | `Push -> (
          match (pipeline.tag, pipeline.ref) with
          | false, "latest-release" -> Latest_release
          | false, "master" -> Master_branch
          | false, _ -> Unknown
          | true, tag_name ->
              if tag_name =~ rex "^v\\d+\\.\\d+(-rc\\d+)?$" then Release_tag
              else if tag_name =~ rex "^v\\d+\\.\\d+\\-beta\\d*$" then
                Beta_release_tag
              else Unknown)
      | `Schedule ->
          (* We can't check the value of the TZ_SCHEDULE_KIND variable here, so we assume it is one of those pipelines *)
          Schedule_extended_test
      | _ -> Unknown
    else
      match pipeline.source with
      | `Push -> (
          match (pipeline.tag, pipeline.ref) with
          | false, "latest-release-test" -> Latest_release_test
          | false, _ -> Unknown
          | true, tag_name ->
              if tag_name =~ rex "^v\\d+\\.\\d+(?:\\-(rc|beta)\\d*)?$" then
                Release_tag_test
              else Unknown)
      | _ -> Unknown
end

let templating () =
  Grafana.
    [
      Custom
        {
          all_value = Some ".*";
          include_all = true;
          name = "pipeline_type";
          options = List.map Pipeline_type.to_string Pipeline_type.all;
        };
      Custom
        {
          all_value = Some ".*";
          include_all = true;
          name = "user";
          options = [marge_bot_user];
        };
      (*       Custom *)
      (*         { *)
      (*           all_value = Some ".*"; *)
      (*           include_all = true; *)
      (*           name = "project_id"; *)
      (*           options = ["3836952"]; *)
      (*         }; *)
      Custom
        {
          all_value = Some ".*";
          include_all = true;
          name = "ref";
          options = ["master"];
        };
      Custom
        {
          all_value = Some ".*";
          include_all = true;
          name = "status";
          options = ["success"; "failed"; "canceled"; "skipped"];
        };
      Custom
        {
          all_value = Some ".*";
          include_all = true;
          name = "source";
          options = ["merge_request_event"; "push"; "schedule"];
        };
    ]

let panels () =
  let ( <&&> ) a b = InfluxDB.And (a, b) in
  let template_filter =
    Tag ("user", MATCH, "$user")
    <&&> Tag ("ref", MATCH, "$ref")
    <&&> Tag ("status", MATCH, "$status")
    <&&> Tag ("source", MATCH, "$source")
    <&&> Tag ("pipeline_type", MATCH, "$pipeline_type")
  in
  let query_pipelines_duration =
    InfluxDB.(
      select
        [As (Function (MEAN, Field "duration"), "Duration")]
        ~from:(Measurement measurement_pipelines)
        ~where:(Grafana_time_filter <&&> template_filter)
        ~group_by:(group_by_time Grafana_interval))
  in
  let query_pipelines_jobs_total =
    InfluxDB.(
      select
        [As (Function (MEAN, Field "jobs_total"), "Jobs")]
        ~from:(Measurement measurement_pipelines)
        ~where:(Grafana_time_filter <&&> template_filter)
        ~group_by:(group_by_time Grafana_interval))
  in
  let query_pipelines_jobs_retried =
    InfluxDB.(
      select
        [As (Function (MEAN, Field "jobs_retried"), "Retried jobs")]
        ~from:(Measurement measurement_pipelines)
        ~where:(Grafana_time_filter <&&> template_filter)
        ~group_by:(group_by_time Grafana_interval))
  in
  let query_pipelines_sequential_duration =
    InfluxDB.(
      select
        [
          As
            (Function (MEAN, Field "sequential_duration"), "Sequential duration");
        ]
        ~from:(Measurement measurement_pipelines)
        ~where:(Grafana_time_filter <&&> template_filter)
        ~group_by:(group_by_time Grafana_interval))
  in
  let query_pipelines_count =
    InfluxDB.(
      select
        [As (Function (COUNT, Field "pipeline_id"), "Pipelines")]
        ~from:(Measurement measurement_pipelines)
        ~where:(Grafana_time_filter <&&> template_filter)
        ~group_by:(group_by_time Grafana_interval))
  in
  Grafana.(
    [Row "Pipeline performance"]
    @ [
        Graph
          {
            title = "Pipeline wall-time";
            description =
              "Wall time per pipeline: this is the time from start to finish \
               of the pipeline";
            queries =
              [
                Grafana.target_query
                  ~alias:"Pipelines duration"
                  query_pipelines_duration;
                Grafana.target_query
                  ~alias:"Job retries"
                  query_pipelines_jobs_retried;
                Grafana.target_query
                  ~alias:"Pipeline jobs"
                  query_pipelines_jobs_total;
              ];
            interval = None;
            yaxis_1 = Some {format = "s"; label = None};
            yaxis_2 = Some {format = "Jobs"; label = None};
            stacking = None;
            drawstyle = Lines;
            tooltip_mode = Multi;
            overrides =
              Overrides.(
                [
                  Match.by_name "Job retries"
                  ==> [
                        Properties.axis_placement Right;
                        Properties.unit "Jobs";
                        Properties.line_width 2;
                        Properties.draw_style Bars;
                      ];
                  Match.by_name "Pipeline jobs"
                  ==> [
                        Properties.axis_placement Right;
                        Properties.unit "Jobs";
                        Properties.line_width 2;
                        Properties.draw_style Bars;
                      ];
                ]
                |> of_list);
            transformations = None;
          };
        Graph
          {
            title = "Pipeline sequential duration";
            description =
              "Sequential duration per pipeline: this is the sum of the \
               duration of each job in the pipeline";
            queries =
              [
                Grafana.target_query
                  ~alias:"Pipelines sequential duration"
                  query_pipelines_sequential_duration;
                Grafana.target_query ~alias:"Pipelines" query_pipelines_count;
              ];
            interval = None;
            yaxis_1 = Some {format = "s"; label = None};
            yaxis_2 = Some {format = "Pipelines"; label = None};
            stacking = None;
            drawstyle = Lines;
            tooltip_mode = Multi;
            overrides =
              Overrides.(
                [
                  Match.by_name "Pipelines"
                  ==> [
                        Properties.axis_placement Right;
                        Properties.unit "Pipelines";
                        Properties.line_width 2;
                        Properties.draw_style Bars;
                      ];
                ]
                |> of_list);
            transformations = None;
          };
      ])

type pipeline_annotated = {
  pipeline : Gitlab_t.single_pipeline;
  pipeline_type : Pipeline_type.t;
  jobs_total : int;
  jobs_retried : int;
  finished_at : float;
  (* Sum of the duration of all jobs in the pipeline *)
  sequential_duration : float;
  is_merge_train : bool;
  mr_iid : int option;
}

let show_pipeline_status : Gitlab_t.pipeline_status -> string = function
  | `WaitingForResource -> "waiting_for_resource"
  | `Running -> "running"
  | `Failed -> "failed"
  | `Success -> "success"
  | `Pending -> "pending"
  | `Skipped -> "skipped"
  | `Preparing -> "preparing"
  | `Created -> "created"
  | `Manual -> "manual"
  | `Scheduled -> "scheduled"
  | `Canceled -> "canceled"

let show_pipeline_source : Gitlab_t.pipeline_source -> string = function
  | `Web -> "web"
  | `Trigger -> "trigger"
  | `Chat -> "chat"
  | `Ondemand_Dast_Scan -> "ondemand_dast_scan"
  | `Push -> "push"
  | `Parent_Pipeline -> "parent_pipeline"
  | `Ondemand_Dast_Validation -> "ondemand_dast_validation"
  | `Webide -> "webide"
  | `External_Pull_Request_Event -> "external_pull_request_event"
  | `Api -> "api"
  | `Pipeline -> "pipeline"
  | `External -> "external"
  | `Schedule -> "schedule"
  | `Merge_Request_Event -> "merge_request_event"

let datapoint_of_pipeline ~measurement :
    pipeline_annotated -> InfluxDB.data_point =
 fun {
       pipeline;
       pipeline_type;
       finished_at;
       jobs_total;
       jobs_retried;
       sequential_duration;
       is_merge_train;
       mr_iid;
     } ->
  InfluxDB.data_point
    ~timestamp:finished_at
    ~tags:
      [
        ("ref", pipeline.ref);
        ("status", show_pipeline_status pipeline.status);
        ("user", pipeline.user.user_short_username);
        ("source", show_pipeline_source pipeline.source);
        ("pipeline_type", Pipeline_type.to_string pipeline_type);
        ("is_merge_train", Bool.to_string is_merge_train);
      ]
    ~other_fields:
      InfluxDB.(
        [
          ("sha", String pipeline.sha);
          ("pipeline_web_url", String pipeline.web_url);
          ("jobs_total", Float (float_of_int jobs_total));
          ("jobs_retried", Float (float_of_int jobs_retried));
          ("duration", Float (pipeline.duration |> Option.value ~default:0.0));
          ( "queued_duration",
            Float (pipeline.queued_duration |> Option.value ~default:0.0) );
          ( "started_at",
            Float (pipeline.started_at |> Option.value ~default:0.0) );
          ("created_at", Float pipeline.created_at);
          ("finished_at", Float finished_at);
          ("project_id", Float (float_of_int pipeline.project_id));
          ("sequential_duration", Float sequential_duration);
        ]
        @ Option.fold
            ~none:[]
            ~some:(fun mr_iid -> [("mr_iid", Float (float_of_int mr_iid))])
            mr_iid)
    measurement
    ("pipeline_id", InfluxDB.Float (float_of_int pipeline.id))

let datapoint_of_job : Gitlab_t.pipeline_job -> InfluxDB.data_point =
 fun _ -> assert false

let register ~executors =
  Long_test.register
    ~__FILE__
    ~executors
    ~title:test_title
    ~tags:["sw_metrics"; "pipeline_performance"; "marge_bot"]
    ~timeout:(Hours 1)
  @@ fun () ->
  match token with
  | None ->
      Log.warn
        "This metric requires a GitLab access token which was not given. The \
         test is skipped. Please set the 'GL_ACCESS_TOKEN' environment \
         variable or the Tezt test argument 'gl_access_token' to fetch this \
         metric." ;
      unit
  | Some token ->
      (* Fetch all new pipelines that are finished *)
      (* Fetch all jobs from those pipelines *)
      (* [Calculate critical path] *)
      let* project, project_id =
        Gitlab.Monad.run
        @@
        let open Gitlab.Monad in
        let* projects =
          Gitlab.Project.by_short_ref ~token ~short_ref:project_short_ref ()
        in
        match Gitlab.Response.value projects with
        | Some project -> return (project, project.project_short_id)
        | None -> Test.fail "Could not find project %s" project_short_ref
      in
      Log.info
        "Fetching pipeline stats from project %s: %d"
        project.project_short_name_with_namespace
        project_id ;
      let* finished_after =
        Util.with_backfill ~__FILE__ "pipelines_finished_after" @@ fun () ->
        let* last =
          Influxdb_util.get_last_datapoint_timestamp measurement_pipelines
        in
        Option.iter
          (fun date ->
            Log.info
              "Fetching all metrics since last treated pipeline from %s (%f)"
              (Time.string_of_datetime date)
              date)
          last ;
        return last
      in
      let main =
        let open Gitlab in
        let open Monad in
        let per_page = 100 in
        let pipelines =
          Project.pipelines
            ~token
            ~project_id
            ~per_page (* TODO: this always fetches one data point too many *)
            ~updated_after:(Time.string_of_datetime finished_after)
            ~order_by:`Updated_at
            ~sort:`Desc
            ~scope:`Finished
            ()
        in
        Fun.flip Stream.iter pipelines @@ fun (pipeline : Gitlab_t.pipeline) ->
        let pipeline_id = pipeline.id in
        let* pipeline = Project.pipeline ~token ~project_id ~pipeline_id () in
        let pipeline = Response.value pipeline in
        let* jobs =
          Project.pipeline_jobs
            ~token
            ~project_id
            ~pipeline_id
            ~include_retried:true
            ~per_page
            ()
          |> Stream.to_list
        in
        let jobs_total = List.length jobs in
        let jobs_retried =
          List.fold_left
            (fun (retries, seen) (job : Gitlab_t.pipeline_job) ->
              if String_set.mem job.name seen then (retries + 1, seen)
              else (retries, String_set.add job.name seen))
            (0, String_set.empty)
            jobs
          |> fst
        in
        let pipeline_type =
          Pipeline_type.type_of_pipeline
            ~default_project_id:project_id
            pipeline
            jobs
        in
        let sequential_duration =
          List.fold_left
            (fun sequential_duration (job : Gitlab_t.pipeline_job) ->
              match job.duration with
              | None -> sequential_duration
              | Some duration -> sequential_duration +. duration)
            0.0
            jobs
        in
        let is_merge_train = pipeline_type = Merge_train in
        let mr_iid =
          match pipeline.ref =~* rex "^refs\\/merge-requests\\/(\\d+)\\/" with
          | Some mr_iid -> int_of_string_opt mr_iid
          | None -> None
        in
        match pipeline.finished_at with
        | None ->
            Log.warn
              "No [finished_at] in pipeline #%d (%s)"
              pipeline_id
              pipeline.web_url ;
            return ()
        | Some finished_at ->
            Log.info
              "Registering %s pipeline: %s"
              pipeline.web_url
              (Pipeline_type.to_string pipeline_type) ;
            let pipeline_annotated =
              {
                pipeline;
                pipeline_type;
                jobs_total;
                jobs_retried;
                finished_at;
                sequential_duration;
                is_merge_train;
                mr_iid;
              }
            in
            let data_point_pipeline =
              datapoint_of_pipeline
                ~measurement:measurement_pipelines
                pipeline_annotated
            in
            Long_test.add_data_point data_point_pipeline ;
            return ()
      in
      Gitlab.Monad.(run main)
