open Tezt
open Tezt_performance_regression

module Color : sig
  type t =
    | Super_light_red
    | Light_red
    | Red
    | Semi_dark_red
    | Dark_red
    | Super_light_orange
    | Light_orange
    | Orange
    | Semi_dark_orange
    | Dark_orange
    | Super_light_yellow
    | Light_yellow
    | Yellow
    | Semi_dark_yellow
    | Dark_yellow
    | Super_light_green
    | Light_green
    | Green
    | Semi_dark_green
    | Dark_green
    | Super_light_blue
    | Light_blue
    | Blue
    | Semi_dark_blue
    | Dark_blue
    | Super_light_purple
    | Light_purple
    | Purple
    | Semi_dark_purple
    | Dark_purple
    | Rgb of (int * int * int)

  val to_alias : t -> string

  val to_json : t -> JSON.u
end

module Match : sig
  type t

  val by_name : string -> t
end

module Properties : sig
  type t

  val display_mode : string -> t

  val hidden : bool -> t

  val filterable : bool -> t

  val links : ?targetBlank:bool -> title:string -> string -> t

  val display_name : string -> t

  type axis_placement = Auto | Left | Right | Hidden

  val axis_placement : axis_placement -> t

  val draw_style : Grafana.drawstyle -> t

  val unit : string -> t

  val line_width : int -> t

  type stacking_mode = Normal | Percent (* Called '100%' in the interface *)

  val stacking : ?mode:stacking_mode -> ?group:string -> unit -> t

  val fixed_color : Color.t -> t

  val column_width : int -> t

  type cell_type =
    | Auto
    | Sparkline
    | Color_text
    | Color_background
    | Gauge
    | JSON_view
    | Image

  val cell_type : cell_type -> t

  type special_value = Null

  type mapping =
    | Value of (string * string * Color.t option) list
    | Special of (special_value * string * Color.t option)
    | Regex of (string * string * Color.t option)

  val mappings : mapping list -> t

  val hide_in_area : ?viz:bool -> ?legend:bool -> ?tooltip:bool -> unit -> t
end

type override

val make : Match.t -> Properties.t list -> override

val ( ==> ) : Match.t -> Properties.t list -> override

val of_list : override list -> JSON.u option
