(* This metrics collects the number of MRs in the merge train for a given project. *)

let measurement_merge_train_load = "tezos_tezos_merge_train_load"

let test_title_merge_train_load = "tezos/tezos merge train load"

let merge_train_load ~executors =
  Long_test.register
    ~__FILE__
    ~executors
    ~title:test_title_merge_train_load
    ~tags:["sw_metrics"; "merge_requests"; "merge_train"; "marge_bot"]
    ~timeout:(Hours 1)
  @@ fun () ->
  let Tezt_cli_arg.
        {gl_access_token = token; project_short_ref; marge_bot_user = _} =
    Tezt_cli_arg.get_shared_opts ()
  in
  let* project =
    Gitlab.Monad.run
    @@
    let open Gitlab.Monad in
    let* projects =
      Gitlab.Project.by_short_ref ?token ~short_ref:project_short_ref ()
    in
    match Gitlab.Response.value projects with
    | Some project -> return project
    | None -> Test.fail "Could not find project %s" project_short_ref
  in
  Log.info
    "Fetching merge train load from project %s: %d"
    project.project_short_name_with_namespace
    project.project_short_id ;
  let open Gitlab.Monad in
  run
  @@
  let poll_total_exn s =
    let* first = Gitlab.Stream.next s in
    let total_opt =
      match first with
      | Some (_val, s') -> Gitlab.Stream.total s'
      | None -> Some 0
    in
    return total_opt
  in
  let trains =
    Gitlab.Project.merge_trains
      ?token
      ~scope:`Active
      ~project_id:project.project_short_id
      ()
  in
  let* count = poll_total_exn trains in
  (match count with
  | None -> Log.warn "Error when fetching merge train count."
  | Some count ->
      Log.info "Current number of active merge trains: %d" count ;
      let data_point =
        InfluxDB.data_point
          ~timestamp:(Unix.gettimeofday ())
          measurement_merge_train_load
          ("count", InfluxDB.Float (float_of_int count))
      in
      Long_test.add_data_point data_point) ;
  return ()

let register ~executors = merge_train_load ~executors
