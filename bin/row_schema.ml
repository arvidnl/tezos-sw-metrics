open Tezt_performance_regression

type ('names, 'a) fields =
  | Float : 'names -> ('names, float) fields
  | Int : 'names -> ('names, int) fields
  | String : 'names -> ('names, string) fields
  | Bool : 'names -> ('names, bool) fields
  | Fun : 'names * ('a -> InfluxDB.field_value) -> ('names, 'a) fields
  | Conv : ('a -> 'b) * ('names, 'b) fields -> ('names, 'a) fields
  | Pair : ('names, 'a) fields * ('names, 'b) fields -> ('names, 'a * 'b) fields
  | Fields3 :
      ('names, 'a) fields * ('names, 'b) fields * ('names, 'c) fields
      -> ('names, 'a * 'b * 'c) fields
  | Fields4 :
      ('names, 'a) fields
      * ('names, 'b) fields
      * ('names, 'c) fields
      * ('names, 'd) fields
      -> ('names, 'a * 'b * 'c * 'd) fields
  | Fields5 :
      ('names, 'a) fields
      * ('names, 'b) fields
      * ('names, 'c) fields
      * ('names, 'd) fields
      * ('names, 'e) fields
      -> ('names, 'a * 'b * 'c * 'd * 'e) fields
  | Fields6 :
      ('names, 'a) fields
      * ('names, 'b) fields
      * ('names, 'c) fields
      * ('names, 'd) fields
      * ('names, 'e) fields
      * ('names, 'f) fields
      -> ('names, 'a * 'b * 'c * 'd * 'e * 'f) fields

type 'a tags =
  | None : unit tags
  | String : string -> string tags
  | Bool : string -> bool tags
  | Fun : string * ('a -> string) -> 'a tags
  | Conv : ('a -> 'b) * 'b tags -> 'a tags
  | Pair : 'a tags * 'b tags -> ('a * 'b) tags

type ('names, 'fields, 'tags) t = {
  measurement : string;
  fields : ('names, 'fields) fields;
  name_to_string : 'names -> string;
  tags : 'tags tags;
}

let rec to_field_values :
    type name_type a.
    (name_type -> string) ->
    (name_type, a) fields ->
    a ->
    (string * InfluxDB.field_value) * (string * InfluxDB.field_value) list =
  let ( ++ ) (x, xs) (y, ys) = (x, xs @ [y] @ ys) in
  fun name_to_string v ->
    let single label value = ((name_to_string label, value), []) in
    match v with
    | Float lbl -> fun v -> single lbl (InfluxDB.Float v)
    | Int lbl -> fun v -> single lbl (InfluxDB.Float (float_of_int v))
    | Bool lbl -> fun v -> single lbl (InfluxDB.String (Bool.to_string v))
    | String lbl -> fun s -> single lbl (InfluxDB.String s)
    | Fun (lbl, f) -> fun v -> single lbl (f v)
    | Conv (f, other) -> fun v -> to_field_values name_to_string other (f v)
    | Pair (s1, s2) ->
        fun (v1, v2) ->
          to_field_values name_to_string s1 v1
          ++ to_field_values name_to_string s2 v2
    | Fields3 (s1, s2, s3) ->
        fun (v1, v2, v3) ->
          to_field_values name_to_string s1 v1
          ++ to_field_values name_to_string s2 v2
          ++ to_field_values name_to_string s3 v3
    | Fields4 (s1, s2, s3, s4) ->
        fun (v1, v2, v3, v4) ->
          to_field_values name_to_string s1 v1
          ++ to_field_values name_to_string s2 v2
          ++ to_field_values name_to_string s3 v3
          ++ to_field_values name_to_string s4 v4
    | Fields5 (s1, s2, s3, s4, s5) ->
        fun (v1, v2, v3, v4, v5) ->
          to_field_values name_to_string s1 v1
          ++ to_field_values name_to_string s2 v2
          ++ to_field_values name_to_string s3 v3
          ++ to_field_values name_to_string s4 v4
          ++ to_field_values name_to_string s5 v5
    | Fields6 (s1, s2, s3, s4, s5, s6) ->
        fun (v1, v2, v3, v4, v5, v6) ->
          to_field_values name_to_string s1 v1
          ++ to_field_values name_to_string s2 v2
          ++ to_field_values name_to_string s3 v3
          ++ to_field_values name_to_string s4 v4
          ++ to_field_values name_to_string s5 v5
          ++ to_field_values name_to_string s6 v6

(*         (fv1, fvs1 @ [y] @ ys @ zs @ [z] @ zs) *)
let rec to_tag_values : type a. a tags -> a -> (string * string) list =
 fun v ->
  match v with
  | None -> fun () -> []
  | String lbl -> fun s -> [(lbl, s)]
  | Bool lbl -> fun b -> [(lbl, Bool.to_string b)]
  | Fun (lbl, f) -> fun v -> [(lbl, f v)]
  | Conv (f, other) -> fun v -> to_tag_values other (f v)
  | Pair (s1, s2) ->
      fun (v1, v2) ->
        let tags1 = to_tag_values s1 v1 in
        let tags2 = to_tag_values s2 v2 in
        tags1 @ tags2

let to_data_point :
    ?timestamp:float ->
    ('names, 'fields, 'tags) t ->
    'fields ->
    'tags ->
    InfluxDB.data_point =
 fun ?timestamp
     {measurement; fields; tags; name_to_string}
     field_values
     tag_values ->
  let field, other_fields =
    to_field_values name_to_string fields field_values
  in
  let tags = to_tag_values tags tag_values in
  InfluxDB.data_point ?timestamp ~tags ~other_fields measurement field

let to_data_point1 :
    ?timestamp:float -> ('b, 'a, 'a) t -> 'a -> InfluxDB.data_point =
 fun ?timestamp {measurement; fields; tags; name_to_string} values ->
  let field, other_fields = to_field_values name_to_string fields values in
  let tags = to_tag_values tags values in
  InfluxDB.data_point ?timestamp ~tags ~other_fields measurement field
