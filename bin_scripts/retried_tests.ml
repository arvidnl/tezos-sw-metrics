open Tezos_sw_metrics_time.Time
open Tezos_sw_metrics.Retried_tests_utils
open Util

let per_page = 200

let () =
  Printexc.register_printer @@ function
  | Gitlab.Message (status_code, message) ->
      Some
        (sf
           "Gitlab error: Status code %d (%s), message: %s"
           (Cohttp.Code.code_of_status status_code)
           (Cohttp.Code.string_of_status status_code)
           (Gitlab.API.string_of_message message))
  | _ -> None

module Logger : LOGGER = struct
  let info fmt =
    let open Gitlab in
    let print s = Monad.embed (Lwt_io.printl s) in
    Format.kasprintf print fmt

  let debug fmt =
    let open Gitlab in
    let print s = Monad.embed (Lwt_io.eprintl s) in
    Format.kasprintf print fmt
end

let logger : logger = (module Logger : LOGGER)

type format = Ascii | Markdown | JSON

let format_retried ?rebased_on ?failure_reason ?updated_after ?updated_before
    ?order_by ?sort ?username ~project_id ~limit format
    (pipelines : (Gitlab_t.pipeline * retried_results) list) =
  ignore format ;
  (* group tests by reason *)
  let flakes =
    List.filter
      (function _pipeline, {flakes = _ :: _; _} -> true | _ -> false)
      pipelines
  in
  let show_order = function
    | `Id -> "id"
    | `Status -> "status"
    | `Ref -> "ref"
    | `Updated_at -> "updated_at"
    | `User_id -> "user_id"
  in
  let filters =
    [sf "limit=%d" limit]
    @ (match rebased_on with None -> [] | Some sha -> [sf "sha=%s" sha])
    @ (match updated_after with
      | None -> []
      | Some updated_after ->
          [sf "updated_after=%s" (string_of_datetime updated_after)])
    @ (match updated_before with
      | None -> []
      | Some updated_before ->
          [sf "updated_before=%s" (string_of_datetime updated_before)])
    @ (match order_by with
      | None -> []
      | Some order_by -> [sf "order_by=%s" (show_order order_by)])
    @ (match sort with
      | None -> []
      | Some sort -> [sf "sort=%s" (Gitlab_j.string_of_sort sort)])
    @ (match username with
      | None -> []
      | Some username -> [sf "username=%s" username])
    @
    match failure_reason with
    | None -> []
    | Some reason ->
        [
          sf
            "failure_reason=%s"
            (Gitlab_j.string_of_pipeline_job_failure_reason reason);
        ]
  in
  let open Lwt.Syntax in
  let* () =
    Lwt_io.printf
      "Analyzed %d pipelines in project %d%s, found %d flaky pipelines (%.2f%%)\n"
      (List.length pipelines)
      project_id
      (match filters with
      | [] -> ""
      | _ -> sf " [%s]" (String.concat ", " filters))
      (List.length flakes)
      (100.0
      *. float_of_int (List.length flakes)
      /. float_of_int (List.length pipelines))
  in
  (* group failures by job_name + extra info *)
  let by_job_reason = Hashtbl.create 5 in
  List.iter
    (fun (_pipeline, {flakes; _}) ->
      List.iter
        (fun {job_name; job_failures; _} ->
          List.iter
            (fun (job, extra_info) ->
              hashtbl_update
                by_job_reason
                ~default:[]
                (job_name, extra_info)
                (fun jobs -> job :: jobs))
            job_failures)
        flakes)
    flakes ;
  let sorted =
    List.sort
      (fun (_, flakes1) (_, flakes2) ->
        Int.compare (List.length flakes2) (List.length flakes1))
      (List.of_seq (Hashtbl.to_seq by_job_reason))
  in
  let* () = Lwt_io.printf "Count,Job name,Extra info,Example\n" in
  Lwt_list.iter_s
    (fun ((job_name, extra_info_opt), failures) ->
      Lwt_io.printf
        "%d,%s,%s,%s\n"
        (List.length failures)
        job_name
        (Format.asprintf "%S"
        @@ Format.asprintf "%a" Tezos_gitlab.pp_extra_info_opt extra_info_opt)
        (match failures with
        | failure :: _ -> failure.Gitlab_t.web_url
        | [] -> "!"))
    sorted

let () =
  let limit = ref 20 in
  (* tezos/tezos *)
  let project_id = ref None in
  let pipeline_id = ref None in
  let token = ref None in
  let failure_reason : Gitlab_t.pipeline_job_failure_reason option ref =
    ref None
  in
  let rebased_on = ref None in
  let updated_after = ref None in
  let updated_before = ref None in
  let order_by = ref None in
  let sort = ref None in
  let format = ref Markdown in
  let username = ref None in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        "<TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        "<PROJECT_ID> Numeric GitLab project id." );
      ( "--pipeline-id",
        Arg.Int (fun i -> pipeline_id := Some i),
        "<ID> Look for flaky jobs in this pipeline only. Useful for debugging."
      );
      ( "--updated-after",
        Arg.String (fun date -> updated_after := Some date),
        "Only pipelines updated after" );
      ( "--updated-before",
        Arg.String (fun date -> updated_before := Some date),
        "Only pipelines updated before" );
      ( "--order-by",
        Arg.String
          (fun order ->
            let order =
              match order with
              | "id" -> `Id
              | "status" -> `Status
              | "ref" -> `Ref
              | "updated_at" -> `Updated_at
              | "user_id" -> `User_id
              | _ -> assert false
            in
            order_by := Some order),
        "Order results by" );
      ( "--sort",
        Arg.String
          (fun sort_dir ->
            let sort_dir =
              match sort_dir with
              | "asc" -> `Asc
              | "desc" -> `Desc
              | _ -> assert false
            in
            sort := Some sort_dir),
        "Order results by" );
      ( "--rebased-on",
        Arg.String (fun sha -> rebased_on := Some sha),
        "<SHA> Only include pipelines whose ref includes the given commit. \
         Assumes a semi-linear workflow, that the argument of is commited on \
         master, and that pipelines run for branches targetting master. It \
         works by fetching the commits in the range of 'SHA..master' (but only \
         up to 500 commits). Then, it excludes any pipeline that has no \
         ancestor (searches up to the last 20 ancestors) in that range." );
      ( "--username",
        Arg.String (fun s -> username := Some s),
        "<USERNAME> Only include pipelines triggered by USERNAME." );
      ( "--failure-reason",
        Arg.String
          (fun s ->
            failure_reason :=
              Some
                (match s with
                | "script_failure" -> `Script_failure
                | "runner_system_failure" -> `Runner_system_failure
                | _ ->
                    raise
                      (Invalid_argument
                         "--failure-reason must be either 'script_failure' or \
                          'runner_system_failure'"))),
        "<REASON> Only include jobs failing of this reason. Can be either \
         'script_failure' or 'runner_system_failure'." );
      ( "--limit",
        Arg.Int
          (fun l ->
            if l < 0 then raise (Invalid_argument "--limit must be positive")
            else limit := l),
        sf
          "<LIMIT> Only search up to this many pipelines (defaults to %d)"
          !limit );
      ( "--format",
        Arg.String
          (function
          | "md" | "markdown" -> format := Markdown
          | "ascii" -> format := Ascii
          | "json" -> format := JSON
          | _ ->
              raise
              @@ Invalid_argument
                   "--format must be either 'md', 'markdown', 'json' or 'ascii'"),
        " Format. Can be either 'markdown', 'md', 'json' or 'ascii'." );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options]\nOptions are:" Sys.argv.(0)
  in
  Arg.parse
    args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg ;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  let rebased_on = !rebased_on in
  let failure_reason = !failure_reason in
  let limit = !limit in
  let format = !format in
  let updated_after =
    Option.map (datetime_of_string ~reqtime:false) !updated_after
  in
  let updated_before =
    Option.map (datetime_of_string ~reqtime:false) !updated_before
  in
  let order_by = !order_by in
  let sort = !sort in
  let username = !username in
  Lwt_main.run
  @@ Gitlab.Monad.(
       run
         (match !pipeline_id with
         | None ->
             let* retried =
               get_retried
                 logger
                 ?rebased_on
                 ?failure_reason
                 ?updated_after
                 ?updated_before
                 ?order_by
                 ?sort
                 ?username
                 ~token
                 ~limit
                 ~project_id
                 ()
             in
             embed
             @@ format_retried
                  ?rebased_on
                  ?failure_reason
                  ?updated_after
                  ?updated_before
                  ?order_by
                  ?sort
                  ?username
                  ~project_id
                  ~limit
                  format
                  retried
         | Some pipeline_id ->
             let* _retried =
               get_flaky_of_pipeline
                 logger
                 ?failure_reason
                 ~project_id
                 ~token
                 ~pipeline_id
                 ()
             in
             return ()))
