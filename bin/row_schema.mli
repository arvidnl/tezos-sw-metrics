open Tezt_performance_regression

(* A GADT for InfluxDB row schemas *)

type ('names, 'a) fields =
  | Float : 'names -> ('names, float) fields
  | Int : 'names -> ('names, int) fields
  | String : 'names -> ('names, string) fields
  | Bool : 'names -> ('names, bool) fields
  | Fun : 'names * ('a -> InfluxDB.field_value) -> ('names, 'a) fields
  | Conv : ('a -> 'b) * ('names, 'b) fields -> ('names, 'a) fields
  | Pair : ('names, 'a) fields * ('names, 'b) fields -> ('names, 'a * 'b) fields
  | Fields3 :
      ('names, 'a) fields * ('names, 'b) fields * ('names, 'c) fields
      -> ('names, 'a * 'b * 'c) fields
  | Fields4 :
      ('names, 'a) fields
      * ('names, 'b) fields
      * ('names, 'c) fields
      * ('names, 'd) fields
      -> ('names, 'a * 'b * 'c * 'd) fields
  | Fields5 :
      ('names, 'a) fields
      * ('names, 'b) fields
      * ('names, 'c) fields
      * ('names, 'd) fields
      * ('names, 'e) fields
      -> ('names, 'a * 'b * 'c * 'd * 'e) fields
  | Fields6 :
      ('names, 'a) fields
      * ('names, 'b) fields
      * ('names, 'c) fields
      * ('names, 'd) fields
      * ('names, 'e) fields
      * ('names, 'f) fields
      -> ('names, 'a * 'b * 'c * 'd * 'e * 'f) fields

type 'a tags =
  | None : unit tags
  | String : string -> string tags
  | Bool : string -> bool tags
  | Fun : string * ('a -> string) -> 'a tags
  | Conv : ('a -> 'b) * 'b tags -> 'a tags
  | Pair : 'a tags * 'b tags -> ('a * 'b) tags

type ('names, 'fields, 'tags) t = {
  measurement : string;
  fields : ('names, 'fields) fields;
  name_to_string : 'names -> string;
  tags : 'tags tags;
}

val to_field_values :
  ('names -> string) ->
  ('names, 'a) fields ->
  'a ->
  (string * InfluxDB.field_value) * (string * InfluxDB.field_value) list

val to_tag_values : 'a tags -> 'a -> (string * string) list

val to_data_point :
  ?timestamp:float ->
  ('names, 'fields, 'tags) t ->
  'fields ->
  'tags ->
  InfluxDB.data_point

val to_data_point1 :
  ?timestamp:float -> ('b, 'a, 'a) t -> 'a -> InfluxDB.data_point
