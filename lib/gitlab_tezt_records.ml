open Tezt
open Tezt.Base
open Util

type 'a response = {body : 'a; code : int}

let gitlab_project_job_artifact ~project ~job_id ~artifact_path =
  Uri.make
    ~scheme:"https"
    ~host:"gitlab.com"
    ~path:
      ("api/v4/"
      ^ sf
          "projects/%s/jobs/%d/artifacts/%s"
          (Uri.pct_encode project)
          job_id
          artifact_path)

let call_raw ?(log_request = true) ?(log_response_status = true)
    ?(log_response_body = false) uri =
  if log_request then
    Log.debug ~color:Log.Color.bold ~prefix:"RPC" "GET %s" (Uri.to_string uri) ;
  let* response, response_body = Cohttp_lwt_unix.Client.call `GET uri in
  if log_response_status then
    Log.debug
      ~prefix:"RPC"
      "RPC response: %s"
      (Cohttp.Code.string_of_status response.status) ;
  let* body = Cohttp_lwt.Body.to_string response_body in
  if log_response_body then Log.debug ~prefix:"RPC" "%s" body ;
  return {body; code = Cohttp.Code.code_of_status response.status}

let call_json ?log_request ?log_response_status ?(log_response_body = false) uri
    =
  let* response =
    call_raw ?log_request ?log_response_status ~log_response_body uri
  in
  match JSON.parse ~origin:"RPC response" response.body with
  | exception (JSON.Error _ as exn) ->
      if log_response_body then Log.debug ~prefix:"RPC" "%s" response.body ;
      raise exn
  | body ->
      if log_response_body then Log.debug ~prefix:"RPC" "%s" (JSON.encode body) ;
      return {response with body}

let call_json_opt uri : JSON.t option Lwt.t =
  let* {body; code} = call_json uri in
  if not (Cohttp.Code.is_success code) then return None else return (Some body)

let of_pipeline_job ~(project_id : int) ~(job_id : int) ~(job_name : string) :
    Tezt_record.t option Lwt.t =
  let project = string_of_int project_id in
  let record_locator_opt =
    match job_name =~* rex "^tezt (\\d+)/\\d+$" with
    | None -> (
        match job_name =~* rex "^tezt-greedy-4k (\\d+)/\\d+$" with
        | None -> (
            match job_name =~* rex "^tezt-greedy-3k (\\d+)/\\d+$" with
            | None -> None
            | Some index ->
                Some
                  ( gitlab_project_job_artifact
                      ~project
                      ~job_id
                      ~artifact_path:
                        ("tezt-results-" ^ index ^ "-memory_3k" ^ ".json")
                      (),
                    index,
                    "memory_3k" ))
        | Some index ->
            Some
              ( gitlab_project_job_artifact
                  ~project
                  ~job_id
                  ~artifact_path:
                    ("tezt-results-" ^ index ^ "-memory_4k" ^ ".json")
                  (),
                index,
                "memory_4k" ))
    | Some index ->
        Some
          ( gitlab_project_job_artifact
              ~project
              ~job_id
              ~artifact_path:("tezt-results-" ^ index ^ ".json")
              (),
            index,
            "" )
  in
  match record_locator_opt with
  | None -> return None
  | Some (uri, _index, _kind) ->
      let* json_opt = call_json_opt uri in
      return (Option.map Tezt_record.of_json json_opt)

let of_pipeline ~token ~(project_id : int) ~(pipeline_id : int) :
    Tezt_record.t Gitlab.Monad.t =
  let open Gitlab.Monad in
  let* jobs =
    Gitlab.Project.pipeline_jobs
      ~token
      ~project_id
      ~per_page:200
      ~pipeline_id
      ()
    |> Gitlab.Stream.to_list
  in
  let*! records =
    Lwt_list.filter_map_p
      (fun (job : Gitlab_t.pipeline_job) ->
        of_pipeline_job ~project_id ~job_id:job.id ~job_name:job.name)
      jobs
  in
  Log.debug "Found %d Tezt records." (List.length records) ;
  return (Tezt_record.unions records)
