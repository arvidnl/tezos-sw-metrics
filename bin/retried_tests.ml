open Tezos_sw_metrics_time

(* This metrics collects retried (i.e. flaky) tests. *)

let test_title = "tezos/tezos retried tests"

let Tezt_cli_arg.{gl_access_token = token; project_short_ref; marge_bot_user} =
  Tezt_cli_arg.get_shared_opts ()

(** A [Retried_test] data point represents the flaky failure of one test.

    For details of the fields and tags in this measurement, see
    [README.md] at the root of the project. *)
module Retried_test = struct
  let measurement = "tezos_tezos_retried_tests"

  type t = {
    timestamp : Gitlab_t.date_time;
    sha : string;
    ref : string;
    job_id : int;
    job_name : string;
    job_user : string;
    job_web_url : string;
    failure : string;
    pipeline_id : int;
    owner : string;
  }

  let to_datapoint
      {
        timestamp;
        sha;
        ref;
        job_id;
        job_name;
        job_user;
        job_web_url;
        failure;
        pipeline_id;
        owner;
      } =
    InfluxDB.(
      data_point
        ~timestamp
        ~tags:
          [
            ("sha", sha);
            ("ref", ref);
            ("job_name", job_name);
            ("job_user", job_user);
            ("failure", failure);
            ("owner", owner);
          ]
        ~other_fields:
          [
            ("pipeline_id", Float (float_of_int pipeline_id));
            ("job_web_url", String job_web_url);
            ("job_name", String job_name);
            ("owner", String owner);
          ]
        measurement
        ("job_id", Float (float_of_int job_id)))
end

(** A [Pipeline] data point represents a terminated pipeline.

    The data point contains the number of jobs total in that pipeline
    (excluding retried jobs) in [jobs_total] and the number of jobs
    that were flaky in [flaky_count].

    For more details of the fields and tags in this measurement, see
    [README.md] at the root of the project. *)
module Pipeline = struct
  let measurement = "tezos_tezos_retried_pipelines"

  type t = {
    timestamp : Gitlab_t.date_time;
    sha : string;
    ref : string;
    status : Gitlab_t.pipeline_status;
    pipeline_web_url : string;
    jobs_total : int;
    flakes_count : int;
    pipeline_id : int;
  }

  let to_datapoint
      {
        timestamp;
        sha;
        ref;
        status;
        pipeline_web_url;
        jobs_total;
        flakes_count;
        pipeline_id;
      } =
    InfluxDB.(
      data_point
        ~timestamp
        ~tags:
          [
            ("sha", sha);
            ("ref", ref);
            ("status", Gitlab_j.string_of_pipeline_status status);
          ]
        ~other_fields:
          [
            ("pipeline_web_url", String pipeline_web_url);
            ("jobs_total", Float (float_of_int jobs_total));
            ("flakes_count", Float (float_of_int flakes_count));
          ]
        measurement
        ("pipeline_id", Float (float_of_int pipeline_id)))
end

let templating () =
  Grafana.
    [
      Custom
        {
          all_value = Some ".*";
          include_all = true;
          name = "job_user";
          options = [marge_bot_user];
        };
      Custom
        {
          all_value = Some ".*";
          include_all = true;
          name = "owner";
          options =
            ["infrastructure"; "layer1"; "tezos2"; "etherlink"; "default"];
        };
    ]

let panels () =
  let flaky_tests_where =
    InfluxDB.(
      And
        ( Grafana_time_filter,
          And
            ( Tag ("job_user", MATCH, "$job_user"),
              Tag ("owner", MATCH, "$owner") ) ))
  in
  let top_flaky_tests_query =
    InfluxDB.(
      select
        [
          As (Function (COUNT, Field "job_id"), "Failures");
          As (Function (LAST, Field "job_name"), "job_name");
          As (Function (LAST, Field "job_web_url"), "job_web_url");
          As (Function (LAST, Field "owner"), "owner");
        ]
        ~from:(Measurement Retried_test.measurement)
        ~where:flaky_tests_where
        ~group_by:(Tags ["failure"]))
  in
  let all_flaky_tests_query =
    InfluxDB.(
      select
        [
          As (Field "job_name", "job_name");
          As (Field "job_web_url", "job_web_url");
          As (Tag "failure", "failure");
          As (Field "owner", "owner");
        ]
        ~from:(Measurement Retried_test.measurement)
        ~where:flaky_tests_where)
  in
  (* SELECT count(pipeline_id) as pipelines
     FROM "tezos_tezos_retried_pipelines"
     WHERE $timeFilter
     GROUP BY time($__interval) fill(0) *)
  let query_pipelines_total =
    InfluxDB.(
      select
        [Function (COUNT, Field "pipeline_id")]
        ~from:(Measurement Pipeline.measurement)
        ~group_by:(group_by_time Grafana_interval ~fill:(Value 0.0))
        ~where:Grafana_time_filter)
  in
  (* SELECT count(pipeline_id) as flaky_pipelines
     FROM "tezos_tezos_retried_pipelines"
     WHERE $timeFilter and "flakes_count" > 0
     GROUP BY time($__interval) fill(0) *)
  let query_pipelines_flaky =
    {
      query_pipelines_total with
      where =
        Some
          (InfluxDB.And
             (Grafana_time_filter, Field ("flakes_count", GT, Float 0.0)));
    }
  in
  (* SELECT sum("flakes_count")/sum("jobs_total") as flakiness
     FROM "tezos_tezos_retried_pipelines" WHERE $timeFilter
     GROUP BY time($__interval) *)
  let query_pipelines_flakiness =
    InfluxDB.(
      select
        [
          Div
            ( Function (SUM, Field "flakes_count"),
              Function (SUM, Field "jobs_total") );
        ]
        ~from:(Measurement Pipeline.measurement)
        ~group_by:(group_by_time Grafana_interval)
        ~where:Grafana_time_filter)
  in
  let table_overrides =
    Overrides.
      [
        (Match.by_name "failure" ==> Properties.[display_name "Test failure"]);
        (Match.by_name "job_web_url" ==> Properties.[hidden true]);
        (Match.by_name "owner"
        ==> Properties.
              [
                display_name "Owner";
                cell_type Color_text;
                mappings
                  [
                    Value
                      [
                        ("default", "Default", Some Color.Light_orange);
                        ( "layer1",
                          "Layer 1",
                          (* Tezos Blue *)
                          Some (Color.Rgb (0x0F, 0x61, 0xFF)) );
                        ( "tezos2",
                          "Tezos 2",
                          (* Tezos Purple *)
                          Some (Color.Rgb (0x9F, 0x32, 0x9F)) );
                        ( "etherlink",
                          "Etherlink",
                          (* Etherlink turquoise rgb(89, 173, 140) *)
                          Some (Color.Rgb (89, 173, 140)) );
                        ( "infrastructure",
                          "Infrastructure",
                          (* No one bothered to make up a color for us *)
                          Some Color.Light_red );
                      ];
                  ];
              ]);
      ]
  in
  Grafana.(
    [Row "Retried tests"]
    @ [
        Table
          {
            title = sf "Top flaky tests";
            description = "Flaky tests in the current time interval";
            queries = [target_query ~result_format:Table top_flaky_tests_query];
            sort_by = Some ("Failures", true);
            overrides =
              Overrides.(
                of_list
                  ([
                     (Match.by_name "Time" ==> Properties.[hidden true]);
                     (Match.by_name "job_name"
                     ==> Properties.
                           [
                             display_name "Example";
                             (* [:raw] ensures that the contents is not url encoded *)
                             links
                               ~title:"Go to example"
                               "${__data.fields.job_web_url:raw}";
                           ]);
                   ]
                  @ table_overrides));
            unit = None;
            transformations = None;
          };
        Graph
          {
            title = "Flaky pipelines (not affected by `job_user`/`owner`)";
            description =
              "Number of pipelines, flaky pipelines and percentage of flaky \
               jobs";
            queries =
              [
                Grafana.target_query
                  ~alias:"Pipelines total"
                  query_pipelines_total;
                Grafana.target_query
                  ~alias:"Flaky pipelines"
                  query_pipelines_flaky;
                Grafana.target_query
                  ~alias:"Flakiness per job"
                  query_pipelines_flakiness;
              ];
            interval = Some (Days 1);
            yaxis_1 = Some {format = "Pipelines"; label = None};
            yaxis_2 = Some {format = "Pipelines"; label = None};
            stacking = None;
            drawstyle = Lines;
            tooltip_mode = Multi;
            overrides =
              Overrides.(
                [
                  Match.by_name "Flakiness per job"
                  ==> [
                        Properties.axis_placement Right;
                        Properties.unit "percentunit";
                      ];
                ]
                |> of_list);
            transformations = None;
          };
        Table
          {
            title = sf "All the flaky jobs";
            description = "All flaky jobs in the current time interval";
            queries = [target_query ~result_format:Table all_flaky_tests_query];
            sort_by = Some ("Time", true);
            overrides =
              Overrides.(
                of_list
                  ([
                     (Match.by_name "job_name"
                     ==> Properties.
                           [
                             display_name "Job";
                             (* [:raw] ensures that the contents is not url encoded *)
                             links
                               ~title:"Go to job"
                               "${__data.fields.job_web_url:raw}";
                           ]);
                   ]
                  @ table_overrides));
            unit = None;
            transformations = None;
          };
      ])

module Logger : Retried_tests_utils.LOGGER = struct
  let info fmt =
    let open Gitlab in
    let print s =
      Monad.embed
        (Log.info "%s" s ;
         unit)
    in
    Format.kasprintf print fmt

  let debug fmt =
    let open Gitlab in
    let print s =
      Monad.embed
        (Log.debug "%s" s ;
         unit)
    in
    Format.kasprintf print fmt
end

let logger : Retried_tests_utils.logger =
  (module Logger : Retried_tests_utils.LOGGER)

let iter l f = l |> List.iter f

let failure_reason = `Script_failure

let get_latest_retries ~token (project : Gitlab_t.project_short) =
  let*! finished_after =
    Util.with_backfill ~__FILE__ "pipelines_finished_after" @@ fun () ->
    let* last =
      (* Checking last pipelines suffices since there is always one pipeline datapoint per retried_test datapoint *)
      Influxdb_util.get_last_datapoint_timestamp Pipeline.measurement
    in
    Option.iter
      (fun date ->
        Log.info
          "Fetching all metrics since last treated pipeline from %s (%f)"
          (Time.string_of_datetime date)
          date)
      last ;
    return last
  in
  let limit = Tezt_cli_arg.get_opt ~__FILE__ int_of_string "pipelines_limit" in
  let open Gitlab.Monad in
  (* We cannot filter based on [finished_at] directly in the [list pipelines] API call.
     Instead, we first filter on [updated_after] (since [updated_at >= finished_at].
     Then, we fetch each full pipeline to get the [finished_at] field (not present in
     the short reponse of [list pipelines]), and filter out updated pipelines that have
     terminated before [finished_after]. *)
  let pipelines =
    Gitlab.Project.pipelines
      ~token
      ~project_id:project.project_short_id
      ~per_page:100
      ~updated_after:(Time.string_of_datetime finished_after)
      ~scope:`Finished
      ()
  in
  let pipelines =
    Gitlab.Stream.map
      (fun (pipeline : Gitlab_t.pipeline) ->
        let* (full_pipeline : Gitlab_t.single_pipeline) =
          Gitlab.Project.pipeline
            ~token
            ~project_id:project.project_short_id
            ~pipeline_id:pipeline.id
            ()
          >|= Gitlab.Response.value
        in
        match full_pipeline with
        | {finished_at = Some finished_at; _} ->
            if finished_at > finished_after then return [pipeline]
            else return []
        | {finished_at = None; _} ->
            Log.warn
              "Pipeline #%d should be finished but does not have a \
               [finished_at]"
              pipeline.id ;
            return [])
      pipelines
  in
  let* pipelines =
    Gitlab.Stream.to_list
    @@
    match limit with
    | None -> pipelines
    | Some limit -> Gitlab.Stream.take limit pipelines
  in
  monad_list_concat_map_p pipelines @@ fun pipeline ->
  let* flakes =
    Retried_tests_utils.get_flaky_of_pipeline
      logger
      ~failure_reason
      ~token
      ~project_id:project.project_short_id
      ~pipeline_id:pipeline.id
      ()
  in
  return [(pipeline, flakes)]

let get_retries ~token (project : Gitlab_t.project_short) =
  let open Gitlab.Monad in
  match
    Tezt_cli_arg.get_opt
      ~__FILE__
      (fun pipeline_ids ->
        pipeline_ids |> String.trim |> String.split_on_char ','
        |> List.map int_of_string)
      "pipelines"
  with
  | None -> get_latest_retries ~token project
  | Some pipeline_ids ->
      let pipeline_of_single (pipeline : Gitlab_t.single_pipeline) :
          Gitlab_t.pipeline =
        {
          id = pipeline.id;
          iid = pipeline.iid;
          project_id = pipeline.project_id;
          status = pipeline.status;
          source = pipeline.source;
          ref = pipeline.ref;
          sha = pipeline.sha;
          web_url = pipeline.web_url;
          created_at = pipeline.created_at;
          updated_at = Option.value ~default:0.0 pipeline.updated_at;
        }
      in
      let project_id = project.project_short_id in
      Util.monad_list_map_s pipeline_ids @@ fun pipeline_id ->
      Log.info "Fetching retried tests from pipeline #%d" pipeline_id ;
      let* pipeline =
        Gitlab.Project.pipeline ~token ~project_id ~pipeline_id ()
        >|= Gitlab.Response.value >|= pipeline_of_single
      in
      let* retries =
        Retried_tests_utils.get_flaky_of_pipeline
          logger
          ~failure_reason
          ~token
          ~project_id
          ~pipeline_id
          ()
      in
      return (pipeline, retries)

let cli_get_records_from ~(project_id : int)
    ~(pipeline_job : Gitlab_t.pipeline_job) =
  Log.info "Fetching records from pipeline #%d" pipeline_job.id ;
  Gitlab_tezt_records.of_pipeline_job
    ~project_id
    ~job_id:pipeline_job.id
    ~job_name:pipeline_job.name

let owner_of_failure_opt ~test_title testowners_opt records =
  match testowners_opt with
  | Some testowners -> (
      match Tezt_record.lookup test_title records with
      | None ->
          Log.warn
            "Test %S could not be found in records, cannot establish ownership."
            test_title ;
          None
      | Some test -> (
          let owners = Testowners.testowner testowners test in
          match owners with
          | [] ->
              Log.warn "Could not establish owner of test %S" test_title ;
              None
          | [owner] ->
              Log.debug "Owner of test %S is %S" test_title owner ;
              Some owner
          | _ :: _ :: _ ->
              Log.warn
                "Expected at most one owner of %s, got %s"
                test_title
                (String.concat ", " owners) ;
              None))
  | None -> None

let get_testowners_opt :
    project:Gitlab_t.project_short ->
    pipeline:Gitlab_t.pipeline ->
    Testowners.testowners option Gitlab.Monad.t =
  let open Gitlab.Monad in
  let testowners_of_sha_opt ~(project : Gitlab_t.project_short) ~sha =
    let repo_url = project.project_short_http_url_to_repo in
    Log.info "Fetching TESTOWNERS.json from commit %S in repo %S" sha repo_url ;
    let*! testowners_res = Git_testowners.of_commit_res ~repo_url ~sha in
    let testowners_opt = Result.to_option testowners_res in
    return testowners_opt
  in
  match Tezt_cli_arg.get_opt ~__FILE__ Fun.id "testowners_ref" with
  | None -> (
      let memo : (string, Testowners.testowners option) Hashtbl.t =
        Hashtbl.create 5
      in
      fun ~project ~pipeline ->
        match Hashtbl.find_opt memo pipeline.sha with
        | Some v -> return v
        | None ->
            let* testowners_opt =
              testowners_of_sha_opt ~project ~sha:pipeline.sha
            in
            Hashtbl.add memo pipeline.sha testowners_opt ;
            return testowners_opt)
  | Some sha -> (
      let memo = ref None in
      fun ~project ~pipeline:_ ->
        match !memo with
        | None ->
            let* testowners_opt = testowners_of_sha_opt ~project ~sha in
            memo := Some testowners_opt ;
            return testowners_opt
        | Some v -> return v)

let register ~executors =
  Long_test.register
    ~__FILE__
    ~executors
    ~title:test_title
    ~tags:["sw_metrics"; "retried_tests"]
    ~timeout:(Hours 1)
  @@ fun () ->
  match token with
  | None ->
      Log.warn
        "This metric requires a GitLab access token which was not given. The \
         test is skipped. Please set the 'GL_ACCESS_TOKEN' environment \
         variable or the Tezt test argument 'gl_access_token' to fetch this \
         metric." ;
      unit
  | Some token -> (
      Gitlab.Monad.run
      @@
      let open Gitlab.Monad in
      let* project =
        let* projects =
          Gitlab.Project.by_short_ref ~token ~short_ref:project_short_ref ()
        in
        match Gitlab.Response.value projects with
        | Some project -> return project
        | None -> Test.fail "Could not find project %s" project_short_ref
      in
      Log.info
        "Fetching flaky tests from from project %s (#%d)"
        project.project_short_path_with_namespace
        project.project_short_id ;
      let* retries = get_retries ~token project in
      monad_list_iter retries
      @@ fun Retried_tests_utils.(pipeline, {flakes; jobs_total}) ->
      let data_point_pipeline =
        Pipeline.to_datapoint
          {
            timestamp = pipeline.created_at;
            sha = pipeline.sha;
            ref = pipeline.ref;
            status = pipeline.status;
            pipeline_web_url = pipeline.web_url;
            jobs_total;
            flakes_count = List.length flakes;
            pipeline_id = pipeline.id;
          }
      in
      Long_test.add_data_point data_point_pipeline ;
      monad_list_iter flakes
      @@ fun Retried_tests_utils.{job_name; job_successes = _; job_failures} ->
      monad_list_iter job_failures
      @@ fun ((pipeline_job : Gitlab_t.pipeline_job), extra_info_opt) ->
      let add_retry_data_point ?failure () =
        let* failure, owner_opt =
          match failure with
          | Some
              Tezos_gitlab.
                {description = failure_description; retry_command = _} -> (
              let*! records_opt =
                cli_get_records_from
                  ~project_id:project.project_short_id
                  ~pipeline_job
              in
              match records_opt with
              | Some records ->
                  let* testowners_opt =
                    (* find the testowners file in that git repo *)
                    get_testowners_opt ~project ~pipeline
                  in
                  let owner_opt =
                    owner_of_failure_opt
                      ~test_title:failure_description
                      testowners_opt
                      records
                  in
                  (match owner_opt with
                  | Some owner ->
                      Log.info
                        "Assigning test %S to owner %S"
                        failure_description
                        owner
                  | None ->
                      Log.info
                        "Could not assign an owner to test %S in job #%d (%s)"
                        failure_description
                        pipeline_job.id
                        pipeline_job.name) ;
                  return (failure_description, owner_opt)
              | None ->
                  Log.warn
                    "Found no records for job #%d (%s), cannot assign owner to \
                     test %S"
                    pipeline_job.id
                    pipeline_job.name
                    failure_description ;
                  return (failure_description, None))
          | None ->
              Log.info
                "No detailed failure description for job #%d (%s), cannot \
                 assign owner"
                pipeline_job.id
                pipeline_job.name ;
              (* InfluxDB tag values cannot be the empty string: we
                 use [-] as the null value for the [failure] tag. *)
              return ("-", None)
        in
        let owner =
          (* InfluxDB tag values cannot be the empty string: we
             assign the uncategorized tests to the [default]
             team. *)
          Option.value ~default:"default" owner_opt
        in
        let data_point_test =
          Retried_test.to_datapoint
            {
              timestamp = pipeline.created_at;
              sha = pipeline.sha;
              ref = pipeline.ref;
              job_id = pipeline_job.id;
              job_name;
              job_user = pipeline_job.user.user_short_username;
              job_web_url = pipeline_job.web_url;
              pipeline_id = pipeline.id;
              failure;
              owner;
            }
        in
        Long_test.add_data_point data_point_test ;
        return ()
      in
      match extra_info_opt with
      | Some (Tezos_gitlab.Extra_info {failures = _ :: _ as failures; _}) ->
          monad_list_iter failures @@ fun failure ->
          add_retry_data_point ~failure ()
      | Some _ | None -> add_retry_data_point ())
