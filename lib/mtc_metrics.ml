open Tezos_sw_metrics_time
open Util

(*

MR states:

 - Creation)
 - Assigned to first non-author
 - "Final decision": Merged / closed (for simplicity, ignore MRs that are merged after an close event)




Per MR :

 - Time spent waiting for reviewer to be assigned then time spent MR is assigned to author / reviewer
   - Can be measured on time spent being assigned to author / non-author
   - Do we have notes API in the ocaml-gitlab? yes

 - Line changes

 - Reviewers

*)

(* first version:

   each row:

     mr id, state, title, lines changed, time from created to merged/closed
*)

type format = CSV

type mr_metrics = {
  mr : Gitlab_t.merge_request_full;
  numstat : Git.numstat;
  merged_at : float;
      (*     ; *)
      (*     merge_time : int; *)
}

let show_datetime dt = sf "%s (%f)" (Time.string_of_datetime dt) dt

let show_mr_metrics {mr; numstat; merged_at} =
  sf
    "MR !%d, +%d, -%d, merged at %s"
    mr.merge_request_full_iid
    numstat.additions
    numstat.deletions
    (show_datetime merged_at)

type ('a, 'b) col = {
  title : string;
  (*   description : string; *)
  of_metrics : 'b -> 'a;
  format : format -> 'a -> string;
}

let format_string_csv s =
  let quote = ref false in
  let sanitized =
    String.map
      (function
        | '"' -> '\''
        | ',' as c ->
            quote := true ;
            c
        | c -> c)
      s
  in
  if !quote then "\"" ^ sanitized ^ "\"" else sanitized

let col_iid =
  {
    title = "MR iid";
    (*     description = ""; *)
    of_metrics = (fun metrics -> metrics.mr.Gitlab_t.merge_request_full_iid);
    format = (function CSV -> fun s -> sf "!%d" s);
  }

let col_title =
  {
    title = "MR title";
    (*     description = ""; *)
    of_metrics = (fun metrics -> metrics.mr.Gitlab_t.merge_request_full_title);
    format = (function CSV -> fun s -> format_string_csv s);
  }

let col_state =
  {
    title = "MR state";
    (*     description = ""; *)
    of_metrics = (fun metrics -> metrics.mr.Gitlab_t.merge_request_full_state);
    format =
      (function
      | CSV -> fun s -> format_string_csv (Gitlab_j.string_of_state s));
  }

let col_additions =
  {
    title = "MR additions";
    (*     description = ""; *)
    of_metrics = (fun metrics -> metrics.numstat.additions);
    format = (function CSV -> fun i -> string_of_int i);
  }

let col_deletions =
  {
    title = "MR deletions";
    (*     description = ""; *)
    of_metrics = (fun metrics -> metrics.numstat.deletions);
    format = (function CSV -> fun d -> string_of_int d);
  }

let col_changes =
  {
    title = "MR changes";
    (*     description = ""; *)
    of_metrics = (fun metrics -> metrics.numstat);
    format =
      (function
      | CSV ->
          fun {additions; deletions} -> string_of_int @@ (additions + deletions));
  }

let cols_merge_time =
  let merge_time_s metrics =
    let merged_at =
      option_get_msg
        ~error_msg:
          (sf
             "Missing merged_at field in MR !%d"
             metrics.mr.merge_request_full_iid)
        metrics.mr.merge_request_full_merged_at
    in
    merged_at -. metrics.mr.merge_request_full_created_at
  in
  [
    {
      title = "MR merge time (seconds)";
      (*       description = "Time between creation date and merge date in seconds"; *)
      of_metrics = merge_time_s;
      format = (function CSV -> string_of_float);
    };
    {
      title = "MR merge time (days)";
      (*       description = "Time between creation date and merge date in days"; *)
      of_metrics =
        (fun metrics -> merge_time_s metrics /. (24.0 *. 60.0 *. 60.0));
      format = (function CSV -> string_of_float);
    };
  ]

type 'b ex_col = Ex_col : ('a, 'b) col -> 'b ex_col

let cols =
  [
    Ex_col col_iid;
    Ex_col col_title;
    Ex_col col_state;
    Ex_col col_additions;
    Ex_col col_deletions;
    Ex_col col_changes;
  ]
  @ List.map (fun c -> Ex_col c) cols_merge_time

(* let per_page = 200 *)

(*     ?(warn : ('a, unit, string, unit Gitlab.Monad.t) format4 -> 'a = eft) *)

let get_mr_metrics ?(info : (string -> unit Lwt.t) option)
    ?(warn : (string -> unit Lwt.t) option) ?created_after ?created_before
    ?merged_after ?merged_before ?order_by ?sort ?limit ?token
    ?(target_branch = "master") ~repository project_id :
    mr_metrics Gitlab.Stream.t =
  let info fmt =
    let logger (s : string) =
      match info with None -> Lwt.return_unit | Some logger -> logger s
    in
    Format.ksprintf logger fmt
  in
  let warn fmt =
    let logger (s : string) =
      match warn with None -> Lwt.return_unit | Some logger -> logger s
    in
    Format.ksprintf logger fmt
  in
  let open Gitlab in
  let open Monad in
  let map xs f = Stream.map f xs in
  let updated_after = Option.map Time.string_of_datetime merged_after in
  let updated_before = Option.map Time.string_of_datetime merged_before in
  let mrs =
    Gitlab.Project.merge_requests
      ?token
      ?created_after
      ?created_before
      ?updated_after
      ?updated_before
      ?order_by
      ?sort
      ~target_branch
      ~state:`Merged
      ~id:project_id
      ()
  in
  let mrs = Stream.(Option.fold ~none:Fun.id ~some:take limit mrs) in
  map mrs @@ fun mr ->
  let*! () = info "Handling MR %s" mr.merge_request_web_url in
  match mr.merge_request_merged_at with
  | Some mr_merged_at ->
      let* mr =
        Gitlab.Project.merge_request
          ~project_id
          ~merge_request_iid:(string_of_int mr.merge_request_iid)
          ()
        >|= Response.value
      in
      let get_numstats () =
        let Gitlab_t.{base_sha; head_sha; start_sha = _} =
          mr.merge_request_full_diff_refs
        in
        (* Make sure [base_sha] and [head_sha] are available locally. *)
        let*! () =
          Git.fetch ~git_path:repository "origin" [head_sha; base_sha]
        in
        let*! numstat =
          Git.numstat ~git_path:repository ~commit1:base_sha ~commit2:head_sha
        in
        return [{mr; numstat; merged_at = mr_merged_at}]
      in
      let in_range_opt lower_opt upper_opt t =
        (match lower_opt with Some lower -> lower < t | None -> true)
        && match upper_opt with Some upper -> t <= upper | None -> true
      in
      if in_range_opt merged_after merged_before mr_merged_at then
        get_numstats ()
      else
        let*! () =
          info
            "Skipping MR !%d since it is not in range: not(%s < %s <= %s)"
            mr.merge_request_full_iid
            (Option.fold ~none:"∞" ~some:show_datetime merged_after)
            (show_datetime mr_merged_at)
            (Option.fold ~none:"∞" ~some:show_datetime merged_before)
        in
        return []
  | _ ->
      let*! () =
        warn
          ">> Ignoring MR !%d without 'merge_commit_sha', 'merged_at' or 'sha' \
           (see %s)\n"
          mr.merge_request_iid
          mr.merge_request_web_url
      in
      return []
