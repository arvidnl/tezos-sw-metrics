(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2024 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** {2 Aliases} *)

let string_of_datetime = ISO8601.Permissive.string_of_datetime

let datetime_of_string = ISO8601.Permissive.datetime

(** {2 Code for calculating the workday diff between two dates} *)

let day_of_week (tm : Unix.tm) =
  match tm.tm_wday with
  | 0 -> 6
  | wday when wday < 0 || wday > 6 ->
      raise (Invalid_argument "[day_of_week] day < 0 || day > 6")
  | wday -> wday - 1

type day = Mon | Tue | Wed | Thu | Fri | Sat | Sun

let day_to_wday = function
  | Mon -> 0
  | Tue -> 1
  | Wed -> 2
  | Thu -> 3
  | Fri -> 4
  | Sat -> 5
  | Sun -> 6

let wday_to_day = function
  | 0 -> Mon
  | 1 -> Tue
  | 2 -> Wed
  | 3 -> Thu
  | 4 -> Fri
  | 5 -> Sat
  | 6 -> Sun
  | _ -> raise (Invalid_argument "[day_to_wday]")

let is_day (tm : Unix.tm) day = day_of_week tm = day_to_wday day

let day_s = 24 * 3600

let tm_norm tm = tm |> Unix.mktime |> snd

let set_midnight (tm : Unix.tm) = {tm with tm_sec = 0; tm_min = 0; tm_hour = 0}

let shift_days (tm : Unix.tm) days =
  tm_norm {tm with tm_mday = tm.tm_mday + days}

(** [days_to wday tgt_day] is the number of days to the next [tgt_day].

    If [wday = day_to_wday tgt_day], then [days_to wday tgt_day = 7]. *)
let days_to tm tgt_day =
  let wday = day_of_week tm in
  let tgt_day = day_to_wday tgt_day in
  if wday > tgt_day then 7 - (wday - tgt_day)
  else if tgt_day = wday then 7
  else tgt_day - wday

(** [days_from wday tgt_day] is the number of days from the previous [tgt_day].

    Note: If [wday = day_to_wday tgt_day], then [days_from wday tgt_day = 0]. *)
let days_from tm tgt_day =
  let wday = day_of_week tm in
  let tgt_day = day_to_wday tgt_day in
  (wday + (7 - tgt_day)) mod 7

let is_midnight_of (tm : Unix.tm) day =
  tm.tm_sec = 0 && tm.tm_min = 0 && tm.tm_hour = 0 && is_day tm day

let prev_day tm day : Unix.tm =
  if is_midnight_of tm day then tm
  else shift_days (set_midnight tm) (-days_from tm day)

let next_day tm day : Unix.tm =
  if is_midnight_of tm day then tm
  else shift_days (set_midnight tm) (days_to tm day)

(** [prev_saturday t] is midnight (first timestamp, 00:00:00) at most recent Saturday.

    If [t] is midnight on a Saturday, then [prev_saturday t = t]. *)
let prev_saturday : Unix.tm -> Unix.tm = fun tm -> prev_day tm Sat

let prev_monday : Unix.tm -> Unix.tm = fun tm -> prev_day tm Mon

(** [next_monday t] is midnight (first timestamp, 00:00:00) at the next Monday.

    If [t] is midnight on a Monday, then [next_monday t = t]. *)
let next_monday : Unix.tm -> Unix.tm = fun tm -> next_day tm Mon

let next_monday_strict (tm : Unix.tm) : Unix.tm =
  if next_monday tm = tm then shift_days (set_midnight tm) 7 else next_monday tm

(** [next_saturday t] is midnight (first timestamp, 00:00:00) at the next Saturday.

    If [t] is midnight on a Saturday, then [next_saturday t = t]. *)
let next_saturday : Unix.tm -> Unix.tm = fun tm -> next_day tm Sat

let is_weekend (tm : Unix.tm) =
  match day_of_week tm |> wday_to_day with Sat | Sun -> true | _ -> false

let same_weekend start_tm end_tm : bool =
  if is_weekend start_tm && is_weekend end_tm then
    prev_saturday start_tm = prev_saturday end_tm
    && next_monday start_tm = next_monday end_tm
  else false

let round_up_to_next_workday : Unix.tm -> Unix.tm =
 fun ts -> if is_weekend ts then next_monday ts else ts

let get_timezone () = match Sys.getenv_opt "TZ" with None -> None | v -> v

let int_of_tm tm = tm |> Unix.mktime |> fst |> int_of_float

let diff_tm a b : int = int_of_tm a - int_of_tm b

let compare_tm a b : int = compare (int_of_tm a) (int_of_tm b)

let datetime_workdays_diff (start_time : float) (end_time : float) : int =
  (match get_timezone () with
  | Some tz when tz <> "UTC" ->
      failwith
        (Printf.sprintf
           "The current timezone is %s and not UTC. A bug in ISO8601 \
            (https://github.com/ocaml-community/ISO8601.ml/issues/17) might \
            result in erroneous timestamps. Set the TZ environment variable or \
            /etc/timezone to UTC for correct results."
           tz)
  | _ -> ()) ;
  if start_time > end_time then
    raise
      (Invalid_argument
         "[datetime_workdays_diff] [start_time] cannot be larger than \
          [end_time]") ;
  let start_tm = Unix.gmtime start_time in
  let end_tm = Unix.gmtime end_time in
  (* if start_time & end_time are both in the same weekend *)
  if same_weekend start_tm end_tm then int_of_float (end_time -. start_time)
  else
    (* some work time has passed between start_tm and end_time *)
    (* if start_tm is during a weekend, then we move it to the 00:00 at
       the next monday. if start_tm is not during a weekend, then
       start_tm is unchanged. *)
    let start_tm = round_up_to_next_workday start_tm in
    if compare_tm end_tm (next_saturday start_tm) <= 0 then
      diff_tm end_tm start_tm
    else if compare_tm end_tm (next_monday start_tm) <= 0 then
      diff_tm (prev_saturday end_tm) start_tm
    else
      (* end_tm is not in the same week as start_tm *)
      (* Diff: D*WW | (DDDDDWW)* | D*E *)
      let diff = diff_tm (next_saturday start_tm) start_tm in
      let weeks =
        diff_tm (prev_monday end_tm) (next_monday (next_saturday start_tm))
        / (7 * day_s)
      in
      let diff = diff + (weeks * 5 * day_s) in
      let diff = diff + diff_tm end_tm (prev_monday end_tm) in
      diff

(** / *)

module Internal_for_test = struct
  let next_monday = next_monday

  let next_monday_strict = next_monday_strict

  let prev_saturday = prev_saturday
end

(* Start tests *)

let%expect_test "test day_of_week" =
  let test s =
    let tm = datetime_of_string s |> Unix.gmtime in
    print_int (day_of_week tm)
  in
  test "2024-02-12T00:00" ;
  [%expect {| 0 |}] ;
  test "2024-02-13T00:00" ;
  [%expect {| 1 |}] ;
  test "2024-02-14T00:00" ;
  [%expect {| 2 |}] ;
  test "2024-02-17T00:00" ;
  [%expect {| 5 |}] ;
  test "2024-02-18T00:00" ;
  [%expect {| 6 |}] ;
  test "2024-02-19T00:00" ;
  [%expect {| 0 |}]

let wrap_tm_s f dt =
  let tm = dt |> datetime_of_string |> Unix.gmtime in
  let tm = f tm in
  tm |> Unix.mktime |> fst |> string_of_datetime

let%expect_test "test prev_saturday" =
  let test datetime_str =
    print_endline (wrap_tm_s prev_saturday @@ datetime_str)
  in
  test "2024-02-09T12:33" ;
  [%expect {| 2024-02-03T00:00:00 |}] ;
  test "2024-02-03T00:00:00" ;
  [%expect {| 2024-02-03T00:00:00 |}] ;
  test "2024-02-14T00:00:00" ;
  [%expect {| 2024-02-10T00:00:00 |}] ;
  test "2024-08-24T00:00:01" ;
  [%expect {| 2024-08-24T00:00:00 |}]

let%expect_test "test next_monday" =
  let test datetime_str =
    print_endline (wrap_tm_s next_monday @@ datetime_str)
  in
  test "2024-02-09T12:33" ;
  [%expect {| 2024-02-12T00:00:00 |}] ;
  test "2024-02-12T00:00:00" ;
  [%expect {| 2024-02-12T00:00:00 |}] ;
  test "2024-02-14T00:00:00" ;
  [%expect {| 2024-02-19T00:00:00 |}] ;
  test "2024-02-11T23:59:59" ;
  [%expect {| 2024-02-12T00:00:00 |}] ;
  test "2020-01-06T00:00:00" ;
  [%expect {| 2020-01-06T00:00:00 |}] ;
  test "2020-10-26T00:00:01" ;
  [%expect {| 2020-11-02T00:00:00 |}]

let%expect_test "test same_weekend_tm" =
  let test dt1_str dt2_str =
    let start_time = datetime_of_string dt1_str |> Unix.gmtime in
    let end_time = datetime_of_string dt2_str |> Unix.gmtime in
    Format.printf "%b" (same_weekend start_time end_time)
  in
  (* Both in the same weekend *)
  test "2024-02-03T00:00:00" "2024-02-04T00:00:00" ;
  [%expect {| true |}] ;
  (* Start time in work week, end time in weekend *)
  test "2024-02-02T00:00:00" "2024-02-04T00:00:00" ;
  [%expect {| false |}] ;
  (* Start time in work week, end time in next work week *)
  test "2024-02-02T00:00:00" "2024-02-05T00:00:00" ;
  [%expect {| false |}] ;
  (* Start time in week-end, end time in next week *)
  test "2024-02-04T00:00:00" "2024-02-07T00:00:00" ;
  [%expect {| false |}] ;
  (* Start time and end time in two different in weekends *)
  test "2024-02-03T00:00:00" "2024-02-10T00:00:00" ;
  [%expect {| false |}]

let%expect_test "test datetime_workdays_diff" =
  let test dt1_str dt2_str =
    let start_time = datetime_of_string dt1_str in
    let end_time = datetime_of_string dt2_str in
    print_int (datetime_workdays_diff start_time end_time / day_s)
  in
  (*   (\* Both in the same weekend *\) *)
  test "2024-02-03T00:00:00" "2024-02-04T00:00:00" ;
  [%expect {| 1 |}] ;
  (* Start time in work week, end time in weekend *)
  test "2024-02-02T00:00:00" "2024-02-04T00:00:00" ;
  [%expect {| 1 |}] ;
  (* Start time in work week, end time in next work week *)
  test "2024-02-02T00:00:00" "2024-02-05T00:00:00" ;
  [%expect {| 1 |}] ;
  (* Start time in week-end, end time in next week *)
  test "2024-02-04T00:00:00" "2024-02-07T00:00:00" ;
  [%expect {| 2 |}] ;
  (* Start time and end time in two different in weekends *)
  test "2024-02-03T00:00:00" "2024-02-10T00:00:00" ;
  [%expect {| 5 |}] ;
  (* Start time and end time in two different in weekends *)
  test "2024-02-03T00:00:00" "2024-02-14T00:00:00" ;
  [%expect {| 7 |}] ;
  test "2024-02-01T00:00:00" "2024-02-21T00:00:00" ;
  [%expect {| 14 |}] ;
  test "2024-02-10T00:00:00" "2024-02-23T00:00:00" ;
  [%expect {| 9 |}] ;
  test "2024-02-09T12:00:00" "2024-02-12T12:00:00" ;
  [%expect {| 1 |}] ;
  test "2024-02-09T12:00:00" "2024-02-09T12:00:00" ;
  [%expect {| 0 |}]

let%expect_test "test datetime_workdays_diff seconds" =
  let test dt1_str dt2_str =
    let start_time = datetime_of_string dt1_str in
    let end_time = datetime_of_string dt2_str in
    print_int @@ datetime_workdays_diff start_time end_time
  in
  test "2020-04-27T00:00:01" "2020-04-27T00:00:01" ;
  [%expect {| 0 |}] ;
  test "2024-10-04T22:04:57" "2024-10-05T00:00:01" ;
  [%expect {| 6903 |}]
