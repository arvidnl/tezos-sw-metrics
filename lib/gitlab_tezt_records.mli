(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** [of_pipeline_job ~project ~job_id ~job_name] fetches the tezt records of a given job. *)
val of_pipeline_job :
  project_id:int -> job_id:int -> job_name:string -> Tezt_record.t option Lwt.t

(** [of_pipeline ~project ~pipeline_id] fetches the tezt records of a given pipeline. *)
val of_pipeline :
  token:Gitlab.Token.t ->
  project_id:int ->
  pipeline_id:int ->
  Tezt_record.t Gitlab.Monad.t
