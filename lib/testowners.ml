open Tezt
open Tezt.Base

type ownership = {tags : string list; path_patterns : rex list}

type testowners = (string * ownership) list

type t = testowners

let of_json (json : JSON.t) : testowners =
  let as_string_list json = JSON.(json |> as_list |> List.map as_string) in
  json |> JSON.as_object
  |> List.map @@ fun (product, ownership_json) ->
     ( product,
       JSON.
         {
           tags = ownership_json |-> "tags" |> as_string_list;
           path_patterns =
             ownership_json |-> "path_patterns" |> as_string_list
             |> List.map rex;
         } )

let of_file (path : string) : testowners = of_json (Tezt.JSON.parse_file path)

type test = {title : string; file : string; tags : string list}

let is_test_owner (ownership : ownership) (test : test) : bool =
  List.exists
    (fun path_pattern -> test.file =~ path_pattern)
    ownership.path_patterns
  || List.exists (fun tag -> List.mem tag test.tags) ownership.tags

let testowner (testowners : testowners) (test : test) =
  List.filter_map
    (fun (owner, ownership) ->
      if is_test_owner ownership test then Some owner else None)
    testowners
