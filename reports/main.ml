(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(* Warning: Please, be sure this function is called at first before using
   any function from [Long_test] to avoid undesired behaviour regarding
   the loading of the configuration. *)
let () = Long_test.init ()

let () =
  let executors = Long_test.[x86_executor1] in
  Retried_tests_report.register ~executors ;
  Test.run ()
