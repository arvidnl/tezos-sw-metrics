(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2024 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_sw_metrics_time.Time
open Tezos_sw_metrics_time.Time.Internal_for_test

let arb_datetime_int =
  let min = datetime_of_string "2024-01-01T00:00:00" |> int_of_float in
  let max = datetime_of_string "2025-01-01T00:00:00" |> int_of_float in
  QCheck2.Gen.int_range min max

let string_of_datetimei i = i |> float_of_int |> string_of_datetime

let pp_dti fmt dti = Format.fprintf fmt "%s" (string_of_datetimei dti)

let long_factor = 100

let wrap_tm f dti =
  let tm = f @@ Unix.gmtime (float_of_int dti) in
  tm |> Unix.mktime |> fst |> int_of_float

let day_s = 24 * 3600

let week_s = day_s * 7

let test_next_monday =
  QCheck2.Test.make
    ~name:"next_monday monotone"
    ~long_factor
    ~print:string_of_datetimei
    arb_datetime_int
  @@ fun date ->
  let next_monday = (wrap_tm next_monday) date in
  if next_monday >= date && next_monday < date + week_s then true
  else
    QCheck2.Test.fail_reportf
      "Expected next_monday of %a to be larger, got %a"
      pp_dti
      date
      pp_dti
      next_monday

let test_next_monday_strict =
  QCheck2.Test.make
    ~name:"next_monday_strict monotone"
    ~long_factor
    ~print:string_of_datetimei
    arb_datetime_int
  @@ fun date ->
  let next_monday_strict = (wrap_tm next_monday_strict) date in
  next_monday_strict > date && next_monday_strict <= date + week_s

let test_next_monday_involutive =
  QCheck2.Test.make
    ~name:"next_monday involutive"
    ~long_factor
    ~print:string_of_datetimei
    arb_datetime_int
  @@ fun date ->
  let next_monday1 = (wrap_tm next_monday) date in
  let next_monday2 = (wrap_tm next_monday) next_monday1 in
  if next_monday1 = next_monday2 then true
  else
    (QCheck2.Test.fail_reportf
       "Expected next_monday(%a) = %a to equal next_monday^2(%a) = %a"
       pp_dti
       date
       pp_dti
       next_monday1
       pp_dti
       date
       pp_dti)
      next_monday2

let test_prev_saturday =
  QCheck2.Test.make
    ~name:"prev_saturday monotone"
    ~long_factor
    ~print:string_of_datetimei
    arb_datetime_int
  @@ fun date ->
  let prev_saturday = (wrap_tm prev_saturday) date in
  if not (prev_saturday <= date) then
    QCheck2.Test.fail_reportf
      "Expected prev_saturday of %a to be smaller, got %a"
      pp_dti
      date
      pp_dti
      prev_saturday
  else if not (date - week_s <= prev_saturday) then
    QCheck2.Test.fail_reportf
      "Did not prev_saturday of %a to less than exactly one week prior (%a), \
       got %a"
      pp_dti
      date
      pp_dti
      (date - week_s)
      pp_dti
      prev_saturday
  else true

let test_prev_saturday_involutive =
  QCheck2.Test.make
    ~name:"prev_saturday involutive"
    ~long_factor
    ~print:string_of_datetimei
    arb_datetime_int
  @@ fun date ->
  let prev_saturday = wrap_tm prev_saturday in
  prev_saturday date = prev_saturday (prev_saturday date)

let test_datetime_workdays_diff =
  QCheck2.Test.make
    ~long_factor
    ~name:"datetime_workdays_diff"
    QCheck2.Gen.(pair arb_datetime_int nat)
  @@ fun (start_time, diff) ->
  let end_time = start_time + diff in
  let workdays_diff =
    datetime_workdays_diff (float_of_int start_time) (float_of_int end_time)
  in
  if not (workdays_diff <= diff) then
    QCheck2.Test.fail_reportf
      "Expected the workday diff (%d) between\n\
       > %s and\n\
       > %s\n\
       to be less than the diff (%d)"
      workdays_diff
      (start_time |> string_of_datetimei)
      (end_time |> string_of_datetimei)
      diff
  else true

let () =
  if get_timezone () <> Some "UTC" then (
    print_endline "Expected timezone UTC" ;
    exit 1) ;
  QCheck_runner.run_tests_main
    [
      test_next_monday;
      test_next_monday_involutive;
      test_next_monday_strict;
      test_prev_saturday;
      test_prev_saturday_involutive;
      test_datetime_workdays_diff;
    ]
