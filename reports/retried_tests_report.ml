(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

type top_retried_test = {
  failure : string;
  failures : int;
  job_name : string;
  job_web_url : string;
  owner : string;
}

let get_top_retried_tests ~since ~limit ~team =
  let query =
    let time_filter =
      (* time >= now() - SINCE and time <= now() *)
      InfluxDB.(And (Time (GE, Sub (Now, since)), Time (LE, Time_value Now)))
    in
    InfluxDB.(
      select
        [
          As (Function (COUNT, Field "job_id"), "failures");
          As (Function (LAST, Field "job_name"), "job_name");
          As (Function (LAST, Field "job_web_url"), "job_web_url");
        ]
        (* Hack: hard-coded measurement name *)
        ~from:(Measurement "tezos_tezos_retried_tests")
        ~where:(And (time_filter, Tag ("owner", EQ, team)))
        ~group_by:(Tags ["failure"]))
  in
  let* top_retried_tests_opt =
    (* Note: we use [unsafe_query] to access the data points that do
       not belong to the current Tezt test (the current Tezt test
       being the report itself). We want to read data points from the
       Tezt test [retried_tests] *)
    Long_test.unsafe_query query @@ fun result ->
    let tests =
      List.map
        (function
          | tags, [datapoint] ->
              InfluxDB.
                {
                  failure = get_tag "failure" tags;
                  failures = get "failures" JSON.as_int datapoint;
                  job_name = get "job_name" JSON.as_string datapoint;
                  job_web_url = get "job_web_url" JSON.as_string datapoint;
                  owner = team;
                }
          | _, series ->
              Test.fail
                "Expected a single data point, got %d"
                (List.length series))
        result
      (* Do sorting and limiting in code, I don't trust/understand InfluxQL's LIMIT / SLIMIT clauses *)
      |> List.sort (fun {failures = f1; _} {failures = f2; _} ->
             Int.compare f2 f1)
    in
    (List.length tests, take limit tests)
  in
  match top_retried_tests_opt with
  | None -> Test.fail "Could not get the top tests?"
  | Some top_tests -> return top_tests

let limit = Tezt_cli_arg.get ~__FILE__ ~default:5 int_of_string_opt "limit"

let period_days =
  Tezt_cli_arg.get ~__FILE__ ~default:14 int_of_string_opt "period_days"

let dry_run =
  Tezt_cli_arg.get ~__FILE__ ~default:true bool_of_string_opt "dry_run"

let retried_test_report ~executors team =
  Long_test.register
    ~__FILE__
    ~executors
    ~team
    ~title:("Retried tests report: " ^ team)
    ~tags:["sw_metrics"; "retried_tests"]
    ~timeout:(Hours 1)
  @@ fun () ->
  let* total, top_tests =
    get_top_retried_tests ~since:(D period_days) ~limit ~team
  in
  (match top_tests with
  | [] -> Log.info "No flaky tests found for team %s" team
  | _ ->
      let message =
        String.concat "\n"
        @@ [
             sf
               "Top %d%s flaky the last %d days (product: %s):"
               limit
               (if total > limit then sf " (out of %d)" total else "")
               period_days
               (String.capitalize_ascii team);
             "";
           ]
        @ List.map
            (function
              | {failure; failures; job_name = _; job_web_url = _; owner = _} ->
                  sf
                    " - %S (%d failure%s)"
                    failure
                    failures
                    (if failures > 1 then "s" else ""))
            top_tests
      in
      let report =
        Format.kasprintf @@ fun s ->
        if dry_run then Log.info "%s" s
        else Long_test.report ~test_header:false "%s" s
      in
      report "%s" message) ;
  unit

let register ~executors =
  (* Explicitly add the 'default' team. Perhaps it should be included
     in the result of {!Long_test.teams} rather? *)
  List.iter (retried_test_report ~executors) ("default" :: Long_test.teams ())
