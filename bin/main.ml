open Util

(* Warning: Please, be sure this function is called at first before using
   any function from [Long_test] to avoid undesired behaviour regarding
   the loading of the configuration. *)
let () = Long_test.init ()

let () =
  Long_test.update_grafana_dashboard
    {
      uid = "tezos_sw_metrics";
      title = "Tezos SWENG Metrics";
      description = "Tezos Software engineering Metrics";
      panels =
        Cloc.panels () @ Merge_metrics.panels () @ Open_mrs.panels ()
        @ Review_load.panels ();
      templating = [];
    } ;
  Long_test.update_grafana_dashboard
    {
      uid = "tezos_flaky_tests";
      title = "Tezos Flaky Tests";
      description = "Flaky tests in tezos/tezos";
      panels = Retried_tests.panels ();
      templating = Retried_tests.templating ();
    } ;
  Long_test.update_grafana_dashboard
    {
      uid = "tezos_pipeline_performance";
      title = "Tezos Pipeline Performance";
      description = "Pipeline performance in tezos/tezos";
      panels = Pipeline_performance.panels ();
      templating = Pipeline_performance.templating ();
    } ;
  Long_test.update_grafana_dashboard
    {
      uid = "tezos_marge_bot";
      title = "Tezos Marge-bot Library panels";
      description =
        "Panels for the Marge-bot dashboard. This is not the Marge-bot \
         dashboard itself. The marge-bot dashboard is manually crafted, but it \
         includes generated panels that are grouped in this dashboard.";
      panels = Marge_bot.panels ();
      templating = [];
    }

(* Register a printer for [GitLab.Message] exceptions. When thrown by
   Gitlab on API errors, they will cause individual tests to fail, and
   the message is logged through {!Tezt.Log.error}. *)
let () =
  Printexc.register_printer (function
      | Gitlab.Message (status, message) ->
          Some
            (sf
               "%s: %s"
               (Cohttp.Code.string_of_status status)
               (Gitlab.API.string_of_message message))
      | _ -> None)

let () =
  let executors = Long_test.[x86_executor1] in
  let () =
    let timezone =
      match Sys.getenv_opt "TZ" with
      | None -> (
          try Some (String.trim (Base.read_file "/etc/timezone"))
          with _ -> None)
      | v -> v
    in
    match timezone with
    | Some tz when tz <> "UTC" ->
        Log.warn
          "The current timezone is %s and not UTC. A bug in ISO8601 \
           (https://github.com/ocaml-community/ISO8601.ml/issues/17) might \
           result in erroneous timestamps. Set the TZ environment variable or \
           /etc/timezone to UTC for correct results."
          tz
    | _ -> ()
  in
  (* Register your tests here. *)
  Cloc.test_clocs ~executors ;
  Merge_metrics.register ~executors ;
  Open_mrs.register ~executors ;
  Review_load.register ~executors ;
  Retried_tests.register ~executors ;
  Pipeline_performance.register ~executors ;
  Marge_bot.register ~executors ;
  Merge_train_load.register ~executors ;
  (* [Test.run] must be the last function to be called. *)
  Test.run ()
