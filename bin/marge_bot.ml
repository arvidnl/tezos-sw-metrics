open Tezos_sw_metrics_time

let measurement_marge_bot_rejections = "tezos_tezos_marge_bot_rejections"

let test_title_marge_bot_rejections = "tezos/tezos marge-bot rejections"

let Tezt_cli_arg.{gl_access_token = token; project_short_ref; marge_bot_user} =
  Tezt_cli_arg.get_shared_opts ()

module Marge_bot_rejection_reason = struct
  type t =
    | CI_failed
    | CI_timeout
    | CI_canceled
    | Draft
    | Rebase_conflict
    | Insufficient_approvals
    | Unresolved_discussions
    | Forbidden_commit_titles
    | Broken_on_the_inside
    | Couldnt_add_to_merge_train
    | Unknown

  let all_reasons =
    [
      CI_failed;
      CI_timeout;
      CI_canceled;
      Draft;
      Rebase_conflict;
      Insufficient_approvals;
      Unresolved_discussions;
      Forbidden_commit_titles;
      Broken_on_the_inside;
      Couldnt_add_to_merge_train;
      Unknown;
    ]

  let to_tag = function
    | CI_failed -> "ci_failed"
    | CI_timeout -> "ci_timeout"
    | CI_canceled -> "ci_canceled"
    | Draft -> "draft"
    | Rebase_conflict -> "rebase_conflicts"
    | Insufficient_approvals -> "insufficient_approvals"
    | Unresolved_discussions -> "unresolved_discussions"
    | Forbidden_commit_titles -> "forbidden_commit_titles"
    | Broken_on_the_inside -> "broken_on_the_inside"
    | Couldnt_add_to_merge_train -> "couldnt_add_to_merge_train"
    | Unknown -> "unknown"

  let to_color =
    let open Overrides.Color in
    function
    (* Following reasons typically indicates flakiness in the infrastructure *)
    | CI_failed -> Super_light_red
    | CI_timeout -> Light_red
    | Broken_on_the_inside -> Dark_red
    (* Following reasons typically indicates user-caused error *)
    | Draft -> Super_light_orange
    | CI_canceled -> Light_orange
    | Insufficient_approvals -> Orange
    | Unresolved_discussions -> Semi_dark_orange
    | Forbidden_commit_titles -> Dark_orange
    (* Not sure if this makes sense *)
    | Couldnt_add_to_merge_train -> Orange
    (* Rebase conflicts are not really the fault of anyone *)
    | Rebase_conflict -> Yellow
    (* Unknown reasons indicate an issue in the [tezos-sw-metrics] *)
    | Unknown -> Dark_purple

  let to_pretty = function
    | CI_failed -> "CI failed"
    | CI_timeout -> "CI timeout"
    | CI_canceled -> "CI canceled"
    | Draft -> "Attempted to merge draft"
    | Rebase_conflict -> "Rebase conflicts"
    | Insufficient_approvals -> "Insufficient approvals"
    | Unresolved_discussions -> "Unresolved discussions"
    | Forbidden_commit_titles -> "Forbidden commit titles"
    | Broken_on_the_inside -> "Broken on the inside"
    | Couldnt_add_to_merge_train -> "Couldn't add to merge train"
    | Unknown -> "Unknown"

  let of_note_body = function
    | "I couldn't merge this branch: CI failed!" -> CI_failed
    | "I couldn't merge this branch: CI is taking too long." -> CI_timeout
    | "I couldn't merge this branch: Someone canceled the CI." -> CI_canceled
    | "I couldn't merge this branch: Sorry, I can't merge requests which have \
       unresolved discussions!" ->
        Unresolved_discussions
    | "I couldn't merge this branch: Sorry, I can't merge requests marked as \
       Work-In-Progress!" ->
        Draft
    | "I couldn't merge this branch: got conflicts while rebasing, your \
       problem now..." ->
        Rebase_conflict
    | "I'm broken on the inside, please somebody fix me... :cry:" ->
        Broken_on_the_inside
    | note_body ->
        if
          String.starts_with
            ~prefix:"I couldn't merge this branch: Insufficient approvals"
            note_body
        then Insufficient_approvals
        else if
          note_body
          =~ rex "Sorry, I can't merge requests with forbidden commit titles"
        then Forbidden_commit_titles
        else if
          (* Handle 'early failures'. *)
          note_body
          =~ rex
               "CI pipeline #\\d+ for MR !\\d+ contains failed jobs .*, \
                rejecting early!"
        then CI_failed
        else if
          String.starts_with
            ~prefix:"I couldn't add this MR to the merge train."
            note_body
        then Couldnt_add_to_merge_train
        else (
          Log.debug "Ignoring note with unknown body: %S" note_body ;
          Unknown)
end

(* Margebot assignment load *)
let panels () =
  let margebot_load_query =
    InfluxDB.(
      select
        [Function (MEAN, Field "assigned_to")]
        ~from:(Measurement Review_load.measurement_assignment_load)
        ~where:(And (Tag ("username", EQ, marge_bot_user), Grafana_time_filter))
        ~group_by:(group_by_time ~fill:Previous Grafana_interval))
  in
  let margebot_merged_query =
    InfluxDB.(
      select
        [Function (COUNT, Field Merge_metrics.(show_column Mr_iid))]
        ~from:(Measurement Merge_metrics.mr_stats_schema.measurement)
        ~where:Grafana_time_filter
        ~group_by:(group_by_time ~fill:(Value 0.0) Grafana_interval))
  in
  let rejected_alias reason =
    sf "Rejected MRs (%s)" (Marge_bot_rejection_reason.to_tag reason)
  in
  let margebot_rejected_query reason =
    InfluxDB.(
      (* SELECT COUNT("mr_iid") FROM "tezos_tezos_marge_bot_rejections" WHERE $timeFilter GROUP BY time($__interval) fill(none)
      *)
      select
        [Function (COUNT, Field "event_id")]
        ~from:(Measurement measurement_marge_bot_rejections)
        ~where:
          (And
             ( Tag
                 ( "rejection_reason",
                   EQ,
                   Marge_bot_rejection_reason.to_tag reason ),
               Grafana_time_filter ))
        ~group_by:(group_by_time Grafana_interval))
  in
  let merge_rate_query window_size_hours =
    InfluxDB.(
      (* SELECT
           DERIVATIVE(CUMULATIVE_SUM(COUNT("mr_iid")), 2h)/2
         FROM tezos_tezos_mr_merge_time_diff_size
         WHERE $timeFilter
         GROUP BY time(2h) fill(0)
      *)
      select
        [
          Div
            ( Function
                ( DERIVATIVE (H window_size_hours),
                  Function (CUMULATIVE_SUM, Function (COUNT, Field "mr_iid")) ),
              Int window_size_hours );
        ]
        ~from:(Measurement Merge_metrics.mr_stats_schema.measurement)
        ~where:Grafana_time_filter
        ~group_by:(group_by_time (H window_size_hours) ~fill:(Value 0.0)))
  in
  let merge_train_load_query =
    InfluxDB.(
      select
        [Function (MEAN, Field "count")]
        ~from:(Measurement Merge_train_load.measurement_merge_train_load)
        ~where:Grafana_time_filter
        ~group_by:(group_by_time ~fill:Previous Grafana_interval))
  in
  Grafana.
    [
      (* Graphs assignments to marge, merged & rejected MRs *)
      Graph
        {
          title = "Marge-bot Assignment Load / Merged MRs";
          description =
            "Number of MRs assigned to Marge-bot / Number of MRs merged";
          queries =
            [
              target_query ~alias:"MRs assigned to Marge" margebot_load_query;
              target_query ~alias:"Merged MRs" margebot_merged_query;
              target_query ~alias:"Merge Rate (2h)" (merge_rate_query 2);
              target_query ~alias:"Active merge trains" merge_train_load_query;
            ]
            @ List.map
                (fun reason ->
                  let alias = rejected_alias reason in
                  target_query ~alias (margebot_rejected_query reason))
                Marge_bot_rejection_reason.all_reasons;
          interval = Some (Minutes 30);
          yaxis_1 = Some {format = "MRs"; label = Some "Margebot MRs"};
          yaxis_2 = None;
          stacking = None;
          drawstyle = Lines;
          tooltip_mode = Multi;
          overrides =
            Overrides.(
              of_list
                ([
                   (Match.by_name "MRs assigned to Marge"
                   ==> Properties.[fixed_color Color.Light_yellow]);
                   (Match.by_name "Active merge trains"
                   ==> Properties.[fixed_color Color.Super_light_purple]);
                   (Match.by_name "Merged MRs"
                   ==> Properties.
                         [
                           draw_style Bars;
                           stacking ();
                           fixed_color Color.Dark_green;
                         ]);
                   (Match.by_name "Merge Rate (2h)"
                   ==> Properties.[fixed_color Color.Light_blue]);
                 ]
                @ List.map
                    (fun reason ->
                      Match.by_name (rejected_alias reason)
                      ==> Properties.
                            [
                              draw_style Bars;
                              stacking ();
                              fixed_color
                                (Marge_bot_rejection_reason.to_color reason);
                            ])
                    Marge_bot_rejection_reason.all_reasons));
          transformations = None;
        };
      (* A table of marge-bot events, including processing and queue
         time per MR. *)
      ((* The Marge-bot Event table is a bit of a mongrel.

          It mixes data from three different sources:
          - The Marge bot rejections series (as scraped in this file)
          - The Merged MRs series (as scraped in {!Merge_metrics})
          - Failed merge train pipelines (as scraped in {!Pipeline_performance})

          It's not very easy to extend. *)
       let rejection_table_query =
         (* SELECT mr_iid as "MR", rejection_reason as "Reason", body as "Message"
            FROM "tezos_tezos_marge_bot_rejections"
            WHERE $timeFilter *)
         InfluxDB.(
           select
             [
               As (Field "mr_iid", "MR");
               As (Field "is_merge_train", "MT");
               As (Field "margebot_assigned_last", "Queue time");
               As (Field "head_pipeline_duration", "CI time");
               As (Tag "rejection_reason", "Reason");
               As (Field "body", "Message");
               Field "head_pipeline_id";
             ]
             ~from:(Measurement measurement_marge_bot_rejections)
             ~where:Grafana_time_filter
             ~order_by:Time_desc)
       in
       let merges_table_query =
         (* SELECT mr_iid as "MR", rejection_reason as "Reason", body as "Message"
            FROM "tezos_tezos_marge_bot_rejections"
            WHERE $timeFilter *)
         InfluxDB.(
           select
             [
               (* Make sure that column names align with those in the the query above *)
               As (Field Merge_metrics.(show_column Mr_iid), "MR");
               As (Field Merge_metrics.(show_column Is_merge_train), "MT");
               As
                 ( Field Merge_metrics.(show_column Margebot_assigned_last),
                   "Queue time" );
               As
                 ( Field Merge_metrics.(show_column Head_pipeline_duration),
                   "CI time" );
               (* We have no equivalent for [Reason] here. It will be
                  empty, and the override defined below will replace the
                  empty value with "Merged". This is a hack. *)
               As (Field Merge_metrics.(show_column Mr_title), "Message");
               Field Merge_metrics.(show_column Head_pipeline_id);
             ]
             ~from:(Measurement Merge_metrics.mr_stats_schema.measurement)
             ~where:Grafana_time_filter
             ~order_by:Time_desc)
       in
       let failed_merge_train_pipelines_query =
         (* SELECT
            "mr_iid"::field as "MR", "duration"::field as "CI time", "is_merge_train"::tag as "MT", "status"::tag as "Reason", "pipeline_id"::field as "head_pipeline_id"
            FROM "tezos_tezos_pipeline_performance_pipelines"
            WHERE "is_merge_train" = 'true' and status != 'success' AND $timeFilter
            ORDER BY time DESC *)
         InfluxDB.(
           select
             [
               As (Field "mr_iid", "MR");
               As (Tag "is_merge_train", "MT");
               (* We have no equivalent for [Queue time] here unfortunately -- these cells will be empty in the table. *)
               As (Field "duration", "CI time");
               As (Tag "status", "Reason");
               (* We have no equivalent for [Message] here unfortunately -- these cells will be empty in the table. *)
               As (Field "pipeline_id", "head_pipeline_id");
             ]
             ~from:(Measurement Pipeline_performance.measurement_pipelines)
             ~where:
               (And
                  ( And
                      ( Tag ("is_merge_train", EQ, "true"),
                        Tag ("status", NE, "success") ),
                    Grafana_time_filter ))
             ~order_by:Time_desc)
       in
       let overrides =
         Overrides.(
           of_list
             [
               (Match.by_name "Time" ==> Properties.[column_width 170]);
               (Match.by_name "Reason"
               ==> Properties.
                     [
                       column_width 200;
                       cell_type Color_text;
                       mappings
                         [
                           Value
                             (List.map
                                (fun reason ->
                                  Marge_bot_rejection_reason.
                                    ( to_tag reason,
                                      to_pretty reason,
                                      Some (to_color reason) ))
                                Marge_bot_rejection_reason.all_reasons);
                           Special (Null, "Merged", Some Color.Dark_green);
                         ];
                     ]);
               (Match.by_name "MR"
               ==> Properties.
                     [
                       links
                         ~title:"Go to MR"
                         (Gitlab_url.merge_request "${__data.fields.MR}");
                       column_width 60;
                     ]);
               (Match.by_name "Message"
               ==> Properties.
                     [
                       mappings
                         [
                           Regex
                             ("I couldn't merge this branch: (.*)", "$1", None);
                         ];
                     ]);
               (Match.by_name "Queue time"
               ==> Properties.
                     [
                       (* Duration time: HH:MM:SS *)
                       unit "dthms";
                       column_width 95;
                     ]);
               (Match.by_name "CI time"
               ==> Properties.
                     [
                       (* Duration time: HH:MM:SS *)
                       unit "dthms";
                       column_width 95;
                       links
                         ~title:"Go to pipeline"
                         (Gitlab_url.pipeline
                            "${__data.fields.head_pipeline_id}");
                     ]);
               (Match.by_name "head_pipeline_id" ==> Properties.[hidden true]);
               (Match.by_name "MT"
               ==> Properties.
                     [
                       column_width 30;
                       mappings
                         [Value [("true", "🚂", None); ("false", "🐌", None)]];
                     ]);
             ])
       in
       Table
         {
           title = "Marge-bot events";
           description = "MRs merged or rejected by marge-bot";
           queries =
             [
               (* Placing the merges_table last makes for a nicer ordering of columns *)
               target_query ~result_format:Table rejection_table_query;
               target_query ~result_format:Table merges_table_query;
               target_query
                 ~result_format:Table
                 failed_merge_train_pipelines_query;
             ];
           sort_by = None;
           overrides;
           unit = None;
           transformations =
             Transformations.of_list
               [Merge; Sort_by {field = "Time"; direction = Desc}];
         });
      (* Show all final pipelines *)
      (let final_pipelines_table_query =
         InfluxDB.(
           select
             [
               Field "pipeline_id";
               Field "mr_iid";
               Field "duration";
               Tag "is_merge_train";
               Field "pipeline_web_url";
               Tag "status";
             ]
             ~from:(Measurement Pipeline_performance.measurement_pipelines)
             ~where:
               (And
                  ( Or
                      ( Tag ("user", EQ, marge_bot_user),
                        Tag ("is_merge_train", EQ, "true") ),
                    Grafana_time_filter ))
             ~order_by:Time_desc)
       in
       Table
         {
           title = "Final pipelines";
           description =
             "Final pre-merge pipelines as triggered by marge-bot or by merge \
              trains";
           queries =
             [target_query ~result_format:Table final_pipelines_table_query];
           sort_by = None;
           overrides = None;
           unit = None;
           transformations =
             Transformations.of_list
               [Sort_by {field = "Time"; direction = Desc}];
         });
    ]

let marge_bot_rejections ~executors =
  Long_test.register
    ~__FILE__
    ~executors
    ~title:test_title_marge_bot_rejections
    ~tags:["sw_metrics"; "merge_requests"; "marge_bot_rejections"; "marge_bot"]
    ~timeout:(Hours 1)
  @@ fun () ->
  match token with
  | None ->
      Log.warn
        "This metric requires a GitLab access token which was not given. The \
         test is skipped. Please set the 'GL_ACCESS_TOKEN' environment \
         variable or the Tezt test argument 'gl_access_token' to fetch this \
         metric." ;
      unit
  | Some token ->
      Log.info "Fetching marge-bot rejections" ;
      let* created_after =
        Util.with_backfill ~__FILE__ "marge_bot_rejections_created_after"
        @@ fun () ->
        let* last =
          Influxdb_util.get_last_datapoint_timestamp
            measurement_marge_bot_rejections
        in
        Option.iter
          (fun date ->
            Log.info
              "Fetching all metrics since last event MR from %s (%f)"
              (Time.string_of_datetime date)
              date)
          last ;
        return last
      in
      let main =
        let open Gitlab in
        let open Monad in
        let* project =
          let* projects =
            Gitlab.Project.by_short_ref ~token ~short_ref:project_short_ref ()
          in
          match Gitlab.Response.value projects with
          | Some project -> return project
          | None -> Test.fail "Could not find project %s" project_short_ref
        in
        let* events =
          User.events
            ~token
            ~per_page:100
              (* TODO: this always fetches one data point too many *)
            ~action:`Commented
              (* TODO: https://gitlab.com/nomadic-labs/tezos-sw-metrics/-/issues/11
                 The GitLab API is unreliable when the [~after:(Time.string_of_datetime created_after)] parameter is passed.
                 As a work-around, we do not set this parameter and instead
                 filter manually using [stream_take_while_map]. *)
            ~sort:`Desc
            ~id:marge_bot_user
            ()
          |> stream_take_while_map @@ fun (event : Gitlab_t.event) ->
             (* TODO: this does not handle the fact that two events can have the exact same [created_at] *)
             if event.event_created_at > created_after then Some event else None
        in
        monad_list_iter events @@ fun (event : Gitlab_t.event) ->
        match event.event_note with
        | None ->
            Log.debug "Ignoring non-note event %d" event.event_id ;
            return ()
        | Some note ->
            let rejection_reason =
              Marge_bot_rejection_reason.of_note_body note.note_body
            in
            let mr_iid = note.note_noteable_iid in
            Log.info
              "MR !%d was rejected (reason: %s). Marge-bot note id #%d, \
               message: %s"
              mr_iid
              (Marge_bot_rejection_reason.to_tag rejection_reason)
              event.event_id
              note.note_body ;
            let rejected_at = event.event_created_at in
            let* head_pipeline_opt =
              (* Find the most recent pipeline on the MR
                 previous to the crdate of this note. *)
              let pipelines =
                Gitlab.Project.merge_request_pipelines
                  ~token
                  ~project_id:project.project_short_id
                  ~merge_request_iid:mr_iid
                  ()
              in
              let* head_pipeline_opt =
                Stream.fold
                  (fun head_pipeline_opt (pipeline : Gitlab_t.pipeline) ->
                    return
                    @@
                    if pipeline.created_at > rejected_at then head_pipeline_opt
                    else
                      match head_pipeline_opt with
                      | None -> Some pipeline
                      | Some head_pipeline ->
                          (* both head_pipeline and pipeline are
                             created before the event, so we're
                             interested in the most recent. *)
                          if head_pipeline.created_at > pipeline.created_at then
                            Some head_pipeline
                          else Some pipeline)
                  None
                  pipelines
              in
              let* head_pipeline_opt =
                match head_pipeline_opt with
                | None -> return None
                | Some head_pipeline ->
                    let* pipeline =
                      Gitlab.Project.pipeline
                        ~token
                        ~project_id:project.project_short_id
                        ~pipeline_id:head_pipeline.id
                        ()
                      >|= Response.value
                    in
                    return (Some pipeline)
              in
              return head_pipeline_opt
            in
            let* margebot_assigned_last =
              Merge_metrics.get_last_margebot_assignment
                ~token
                ~project_id:project.project_short_id
                ~marge_bot_user
                ~before:rejected_at
                ~merge_request_iid:mr_iid
                ()
            in
            let margebot_assigned_last =
              match margebot_assigned_last with
              | Some assignment ->
                  Log.info
                    "Last marge-bot assignment: %s"
                    (Time.string_of_datetime assignment) ;
                  rejected_at -. assignment
              | None -> 0.0
            in
            let body =
              match String.index_opt note.note_body '\n' with
              | Some new_line_pos -> String.sub note.note_body 0 new_line_pos
              | None -> note.note_body
            in
            let head_pipeline_fields, is_merge_train =
              match head_pipeline_opt with
              | Some head_pipeline ->
                  let duration =
                    match
                      (head_pipeline.duration, head_pipeline.started_at)
                    with
                    | Some duration, _ -> Some duration
                    | None, Some started_at -> Some (rejected_at -. started_at)
                    | None, None -> None
                  in
                  ( InfluxDB.(
                      [
                        ( "head_pipeline_id",
                          Float (float_of_int head_pipeline.id) );
                        ("head_pipeline_ref", String head_pipeline.ref);
                      ]
                      @ Option.fold
                          ~none:[]
                          ~some:(fun duration ->
                            [("head_pipeline_duration", Float duration)])
                          duration),
                    head_pipeline.ref
                    =~ rex "refs\\/merge-requests\\/\\d+\\/train" )
              | None -> ([], false)
            in
            let data_point_event =
              InfluxDB.data_point
                ~timestamp:rejected_at
                measurement_marge_bot_rejections
                ~tags:
                  [
                    ( "rejection_reason",
                      Marge_bot_rejection_reason.to_tag rejection_reason );
                  ]
                ~other_fields:
                  (InfluxDB.
                     [
                       ("event_id", Float (float_of_int event.event_id));
                       ("mr_iid", Float (float_of_int mr_iid));
                       ("margebot_assigned_last", Float margebot_assigned_last);
                       ("is_merge_train", String (Bool.to_string is_merge_train));
                     ]
                  @ head_pipeline_fields)
                ("body", String body)
            in
            Long_test.add_data_point data_point_event ;
            return ()
      in
      Gitlab.Monad.(run main)

let register ~executors = marge_bot_rejections ~executors
