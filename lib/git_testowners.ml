open Tezt
open Tezt.Base

let process_check_and_read_stdout_res process =
  let* status = Process.wait process in
  match Process.validate_status status with
  | Result.Ok () ->
      let* output = Lwt_io.read (Process.stdout process) in
      return (Result.Ok output)
  | Result.Error e -> return (Result.Error e)

let of_commit_res ~repo_url ~sha =
  Log.info "Fetching %s in %s" sha repo_url ;
  let* git_path = Repo_cache.get_at_sha ~url:repo_url sha in
  let origin = sha ^ ":" ^ "tezt/TESTOWNERS.json" in
  let process = Git.spawn_show ~git_path [origin] in
  let* json_res = process_check_and_read_stdout_res process in
  return
    (json_res
    |> Result.map (JSON.parse ~origin)
    |> Result.map Testowners.of_json)
