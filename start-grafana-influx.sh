#!/bin/sh

docker-compose -f vendors/lib_performance_regression/local-sandbox/docker-compose.yml up --force-recreate -d
export TEZT_CONFIG=vendors/lib_performance_regression/local-sandbox/tezt_config.json
