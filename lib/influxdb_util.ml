open Tezt
open Tezt.Base
open Tezt_performance_regression
open Tezos_sw_metrics_time

let get_last_datapoint_timestamp measurement =
  let select =
    InfluxDB.(
      select [All] ~from:(Measurement measurement) ~order_by:Time_desc ~limit:1)
  in
  Log.debug "[get_last_datapoint_timestamp] %S" (InfluxDB.show_select select) ;
  let* result =
    Long_test.query select @@ fun result ->
    match result with
    | [] -> None
    | [(_tags, [data_point])] ->
        let time = InfluxDB.get "time" JSON.as_string data_point in
        Log.debug " --> %s" time ;
        Some time
    | _ ->
        Test.fail
          "[get_last_datapoint_timestamp] unexpectedly received more than one \
           data point"
  in
  return (Option.join result |> Option.map Time.datetime_of_string)
