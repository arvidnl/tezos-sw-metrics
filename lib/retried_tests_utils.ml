open Tezt.Base
open Tezos_sw_metrics_time
open Util

type flake_result = {
  job_name : string;
  job_successes : Gitlab_t.pipeline_job list;
  job_failures : (Gitlab_t.pipeline_job * Tezos_gitlab.extra_info option) list;
}

type retried_results = {flakes : flake_result list; jobs_total : int}

module type LOGGER = sig
  val info : ('a, Format.formatter, unit, unit Gitlab.Monad.t) format4 -> 'a

  val debug : ('a, Format.formatter, unit, unit Gitlab.Monad.t) format4 -> 'a
end

type logger = (module LOGGER)

let get_flaky_of_pipeline (logger : logger) ?failure_reason ?(per_page = 100)
    ~token ~project_id ~pipeline_id () : retried_results Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let module Logger = (val logger) in
  let job_status = Hashtbl.create 5 in
  let add_status (job : Gitlab_t.pipeline_job) =
    hashtbl_update
      ~default:([], [])
      job_status
      job.name
      (fun (succeded, failures) ->
        match job.status with
        | `Success -> (job :: succeded, failures)
        | `Failed -> (
            match (failure_reason, job.failure_reason) with
            | None, _ -> (succeded, job :: failures)
            | Some r, Some r' when r = r' -> (succeded, job :: failures)
            | Some _, Some _ -> (succeded, failures)
            | Some _, None ->
                (* I don't think this is possible since the job is failed, but who knows? *)
                assert false)
        | _ -> (succeded, failures))
  in
  let* failed_jobs =
    return
    @@ Gitlab.Project.pipeline_jobs
         ~token
         ~project_id
         ~per_page
         ~pipeline_id
         ~scope:`Failed
         ~include_retried:true
         ()
  in
  let* failed_jobs = Stream.to_list failed_jobs in
  let* jobs_total =
    let all_jobs =
      Gitlab.Project.pipeline_jobs
        ~token
        ~project_id
        ~per_page:1
        ~pipeline_id
        ~include_retried:false
        ()
    in
    let poll_total_exn s =
      let* first = Gitlab.Stream.next s in
      match first with
      | Some (_val, s') -> (
          match Gitlab.Stream.total s' with
          | Some total -> return total
          | None ->
              failwith
                (sf
                   "Couldn't find total number of jobs in pipeline #%d"
                   pipeline_id))
      | None -> return 0
    in
    poll_total_exn all_jobs
  in
  if List.length failed_jobs = 0 then
    let* () =
      Logger.info
        "Pipeline #%d contains no failed jobs (thus no flaky) out of %d jobs"
        pipeline_id
        jobs_total
    in
    return {flakes = []; jobs_total}
  else
    let succeeded_jobs =
      Gitlab.Project.pipeline_jobs
        ~token
        ~project_id
        ~per_page
        ~pipeline_id
        ~scope:`Success
        ~include_retried:true
        ()
    in
    let* succeeded_jobs = Stream.to_list succeeded_jobs in
    let jobs = failed_jobs @ succeeded_jobs in
    List.iter add_status jobs ;
    let flakes =
      List.filter_map
        (fun (job_name, (job_successes, job_failures)) ->
          match (job_successes, job_failures) with
          (* both are non empty *)
          | _ :: _, _ :: _ ->
              let job_failures = List.map (fun f -> (f, None)) job_failures in
              Some {job_name; job_successes; job_failures}
          | _ -> None)
        (List.of_seq (Hashtbl.to_seq job_status))
    in
    let* flakes =
      monad_list_map_p flakes @@ fun {job_name; job_successes; job_failures} ->
      let* job_failures =
        monad_list_map_p job_failures (fun (job, _) ->
            let+ extra_info =
              Tezos_gitlab.get_extra_info ~token ~project_id job
            in
            (job, extra_info))
      in
      return {job_name; job_successes; job_failures}
    in
    let flake_count = List.length flakes in
    let* () =
      Logger.info
        "Pipeline #%d contains %d flaky jobs out of %d jobs:"
        pipeline_id
        flake_count
        jobs_total
    in
    let* () =
      monad_list_iter flakes @@ fun {job_name; job_successes; job_failures} ->
      let* () = Logger.info " - Job '%s':" job_name in
      let* () =
        monad_list_iter job_failures (fun (failure, extra_info) ->
            Logger.info
              "   - Failure: %s [reason: %s]"
              failure.Gitlab_t.web_url
              (Tezos_gitlab.show_extra_info_opt extra_info))
      in
      let* () =
        monad_list_iter job_successes (fun successes ->
            Logger.info "   - Success: %s" successes.Gitlab_t.web_url)
      in
      return ()
    in
    return {flakes; jobs_total}

(* should return [(pipeline_id & url & created_at, [job_name, [successes], [failures]])] *)
let get_retried (logger : logger) ?(per_page = 100) ?rebased_on ?failure_reason
    ?created_after ?updated_after ?updated_before ?order_by ?sort ?username
    ~token ?limit ~project_id () :
    (Gitlab_t.pipeline * retried_results) list Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let module Logger = (val logger) in
  let* updated_after =
    match (rebased_on, updated_after, created_after) with
    | Some _, Some _, None | Some _, Some _, Some _ | Some _, None, Some _ ->
        raise
        @@ Invalid_argument
             "cannot give '--rebased-on' with 'updated_after' or \
              'created_after'"
    | Some sha, None, None ->
        let* commit = Gitlab.Project.Commit.commit ~token ~project_id ~sha () in
        let commit = Gitlab.Response.value commit in
        let created_at = commit.commit_created_at in
        let+ () =
          Logger.info
            "Restricting to pipelines rebased on %s (updated_after: %s)"
            sha
            (Time.string_of_datetime created_at)
        in
        Some created_at
    | None, Some updated_after, _ -> return (Some updated_after)
    | None, _, Some created_after -> return (Some created_after)
    | _ -> return None
  in
  let* pipelines =
    let updated_after = Option.map Time.string_of_datetime updated_after in
    let updated_before = Option.map Time.string_of_datetime updated_before in
    return
    @@ Gitlab.Project.pipelines
         ~token
         ~project_id
         ~per_page
         ?updated_after
         ?updated_before
         ?order_by
         ?sort
         ?username
         ~scope:`Finished
         ()
  in
  let pipelines =
    match created_after with
    | None -> pipelines
    | Some created_after ->
        Stream.map
          (fun (pipeline : Gitlab_t.pipeline) ->
            if pipeline.created_at > created_after then return [pipeline]
            else return [])
          pipelines
  in
  let* pipelines =
    Stream.to_list
    @@
    match limit with
    | None -> pipelines
    | Some limit -> Stream.take limit pipelines
  in
  let* commits_base_master_range =
    match rebased_on with
    | None -> return String_set.empty
    | Some base_sha ->
        let ref_name = sf "%s~1..master" base_sha in
        let* commits =
          return
          @@ Gitlab.Project.Commit.commits ~token ~project_id ~ref_name ()
        in
        let commit_limit = 500 in
        let* commits = stream_take_list (commit_limit + 2) commits in
        let all_but_last xs =
          match List.rev xs with [] -> [] | _ :: xs -> List.rev xs
        in
        let commits = all_but_last commits in
        let n_commits = List.length commits in
        if n_commits = 0 then
          let* () =
            Logger.info
              "Couldn't find any commits in the range %s, terminating"
              ref_name
          in
          exit 1
        else if n_commits = commit_limit + 1 then
          let* () =
            Logger.info
              "Range %s contains more than the limit of %d commits, terminating"
              ref_name
              commit_limit
          in
          exit 1
        else
          let+ () =
            Logger.info "Fetched %d commits in the range %s" n_commits ref_name
          in
          String_set.of_list
            (List.map (fun (c : Gitlab_t.commit) -> c.commit_id) commits)
  in
  monad_list_concat_map_p pipelines @@ fun pipeline ->
  let* rebased =
    match rebased_on with
    | None -> return true
    | Some _ ->
        let* commits =
          return
          @@ Gitlab.Project.Commit.commits
               ~token
               ~project_id
               ~ref_name:pipeline.sha
               ()
        in
        let+ commit_opt =
          stream_limited_find
            ~limit:20
            (fun (commit : Gitlab_t.commit) ->
              String_set.mem commit.commit_id commits_base_master_range)
            commits
        in
        Option.is_some commit_opt
  in
  if rebased then
    let* () =
      Logger.info
        "Pipeline #%d created at %s:"
        pipeline.id
        (Time.string_of_datetime pipeline.created_at)
    in
    let* flakes =
      get_flaky_of_pipeline
        logger
        ?failure_reason
        ~token
        ~project_id
        ~pipeline_id:pipeline.id
        ()
    in
    return [(pipeline, flakes)]
  else
    let* () =
      Logger.info
        "Pipeline #%d created at %s (ignored, not rebased)"
        pipeline.id
        (Time.string_of_datetime pipeline.created_at)
    in
    return []
