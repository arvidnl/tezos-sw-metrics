# Changelog

## Development Version

### Breaking Changes

- The default value of `retried_tests_report.dry_run` is now `true`.

### New Features

### Bug Fixes

- Fix the sorting and limiting of flake report results.
