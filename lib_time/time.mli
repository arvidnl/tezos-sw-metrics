(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2024 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Alias for {!ISO8601.Permissive.string_of_datetime} *)
val string_of_datetime : float -> string

(** Alias for {!ISO8601.Permissive.datetime} *)
val datetime_of_string : ?reqtime:bool -> string -> float

(** The current timezone as per the [TZ] environment variable. *)
val get_timezone : unit -> string option

(** Returns the number of work days between two timestamps.

    [datetime_workdays_diff start_time end_time] is the number of
    seconds between [start_time] to [end_time], excluding any weekend time.

    However, if [start_time] and [end_time] fall in the same week-end,
    then we return the difference between the two. *)
val datetime_workdays_diff : float -> float -> int

(** / *)

(** Internals for testing *)
module Internal_for_test : sig
  val next_monday : Unix.tm -> Unix.tm

  val next_monday_strict : Unix.tm -> Unix.tm

  val prev_saturday : Unix.tm -> Unix.tm
end
