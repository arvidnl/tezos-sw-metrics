open Tezt

let expand_var_name ~__FILE__ name =
  if not (Filename.check_suffix __FILE__ ".ml") then
    Test.fail "[get_opt] argument [~__FILE__] should have the suffix .ml"
  else
    let file = Filename.(chop_suffix (basename __FILE__) ".ml") in
    file ^ "." ^ name

let get_shared_opt f name =
  Cli.get ~default:None (fun x -> Some (Some (f x))) name

let get ~__FILE__ ?default f name =
  let name = expand_var_name ~__FILE__ name in
  Log.info "Reading '%s'" name ;
  Cli.get ?default f name

let get_opt ~__FILE__ f name =
  let name = expand_var_name ~__FILE__ name in
  Log.info "Reading '%s'" name ;
  get_shared_opt f name

type shared_opts = {
  project_short_ref : string;
  gl_access_token : Gitlab.Token.t option;
  marge_bot_user : string;
}

let get_shared_opts () : shared_opts =
  let gl_access_token =
    Option.map Gitlab.Token.of_string
    @@
    match get_shared_opt Fun.id "gl_access_token" with
    | Some token -> Some token
    | None -> Sys.getenv_opt "GL_ACCESS_TOKEN"
  in
  let project_short_ref =
    match get_shared_opt Fun.id "project_id" with
    | Some short_ref -> short_ref
    | None -> "tezos/tezos"
  in
  let marge_bot_user =
    match get_shared_opt Fun.id "marge_bot_user" with
    | Some short_ref -> short_ref
    | None -> "nomadic-margebot"
  in
  {project_short_ref; gl_access_token; marge_bot_user}
