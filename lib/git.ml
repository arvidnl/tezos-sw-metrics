open Tezt
open Tezt.Base

let optional_switch_short name switch = if switch then [sf "-%c" name] else []

let optional_switch name switch = if switch then ["--" ^ name] else []

let optional_arg name f = function None -> [] | Some x -> ["--" ^ name; f x]

let optional_set_arg name f = function
  | None -> []
  | Some x -> ["--" ^ name ^ "=" ^ f x]

let optional_short_arg name f = function
  | None -> []
  | Some x -> ["-" ^ name; f x]

let optional_string_arg name = optional_arg name Fun.id

let optional_short_string_arg name = optional_short_arg name Fun.id

(* let switch flag b = if b then [flag] else [] *)

(* let option : ?to_string:('a -> string) -> 'a -> 'a option -> string list = *)
(*  fun ?to_string option_name value_opt -> *)
(*   let to_string = Option.value ~default:Fun.id to_string in *)
(*   Option.fold ~some:(fun p -> [option_name; to_string p]) ~none:[] value_opt *)

(* let opt_arg : ?to_string:('a -> string) -> 'a option -> string list = *)
(*  fun ?to_string arg_opt -> *)
(*   let arg_opt = Option.map to_string arg_opt in *)
(*   Option.fold ~some:(fun arg -> arg) ~none:[] arg_opt *)

let git ?git_path args =
  Process.run "git" @@ optional_short_string_arg "C" git_path @ args

let spawn ?git_path args =
  Process.spawn "git" @@ optional_short_string_arg "C" git_path @ args

let git_and_read_stdout ?git_path args =
  Process.run_and_read_stdout "git"
  @@ optional_short_string_arg "C" git_path
  @ args

let status ?git_path files =
  git_and_read_stdout
    ?git_path
    (["status"; "--untracked-files=no"; "--porcelain"] @ files)

let is_clean ?git_path files =
  let* output = status ?git_path files in
  return (output = "")

let check_clean ?git_path files =
  let* output = status ?git_path files in
  Check.((output = "") string ~error_msg:"Expected a clean repo, found: %L") ;
  unit

let get_current_branch ?git_path () =
  Lwt.(
    git_and_read_stdout ?git_path ["rev-parse"; "--abbrev-ref"; "HEAD"]
    >|= String.trim)

let get_current_commit ?git_path ?(short = false) () =
  Lwt.(
    git_and_read_stdout ?git_path
    @@ ["rev-parse"]
    @ optional_switch "short" short
    @ ["HEAD"]
    >|= String.trim)

let branch_exists ?git_path branch =
  let* matches = git_and_read_stdout ?git_path ["branch"; "--list"; branch] in
  return @@ (matches = "  " ^ branch ^ "\n")

let create_or_checkout ?git_path branch =
  let* exists = branch_exists ?git_path branch in
  git ?git_path @@ ["checkout"]
  @ optional_switch_short 'b' (not exists)
  @ [branch]

let create_and_checkout ?git_path branch =
  git ?git_path ["checkout"; "-b"; branch]

let checkout ?git_path branch = git ?git_path ["checkout"; branch]

let clone ?depth ?shallow_since ?directory repository =
  (* git clone --depth 1 git@gitlab.com:tezos/tezos.git $(mktemp -d) *)
  git @@ ["clone"]
  @ optional_arg "depth" string_of_int depth
  @ optional_arg "shallow-since" Fun.id shallow_since
  @ [repository] @ Option.to_list directory

let pull ?git_path ?depth () =
  (* git clone --depth 1 git@gitlab.com:tezos/tezos.git $(mktemp -d) *)
  git ?git_path @@ ["pull"] @ optional_arg "depth" string_of_int depth

let fetch ?git_path ?depth ?shallow_since ?deepen remote ref_spec =
  git ?git_path @@ ["fetch"]
  @ optional_arg "depth" string_of_int depth
  @ optional_arg "deepen" string_of_int deepen
  @ optional_arg "shallow-since" Fun.id shallow_since
  @ [remote] @ ref_spec

let spawn_show ?git_path objects = spawn ?git_path @@ ["show"] @ objects

let show ?git_path objects =
  Process.check_and_read_stdout @@ spawn_show ?git_path objects

let commit ?git_path ?(files = []) message =
  git ?git_path @@ ["commit"] @ files @ ["-m"; message]

let reset ?git_path ?(hard = false) branch =
  git ?git_path @@ ["reset"] @ optional_switch "hard" hard @ [branch]

let remote_get_url ?git_path remote =
  Lwt.(
    git_and_read_stdout ?git_path ["remote"; "get-url"; remote] >|= String.trim)

let push ?git_path remote = git ?git_path ["push"; remote]

let stash ?git_path () = git ?git_path ["stash"]

let stash_pop ?git_path () = git ?git_path ["stash"; "pop"]

(* let fresh_branch_name ?git_path prefix = *)
(*   let* existing_branches = *)
(*     git_and_read_stdout *)
(*       ?git_path *)
(*       [ *)
(*         "branch"; *)
(*         "--all"; *)
(*         "--list"; *)
(*         test_remote ^ "/" ^ prefix ^ "--*"; *)
(*         prefix ^ "--*"; *)
(*       ] *)
(*   in *)
(*   let indexes = *)
(*     List.filter_map *)
(*       (fun branch_name -> *)
(*         let parts = String.split_on_char '-' branch_name in *)
(*         if List.length parts == 0 then None *)
(*         else List.nth parts (List.length parts - 1) |> int_of_string_opt) *)
(*       (String.split_on_char '\n' existing_branches) *)
(*   in *)
(*   let next_index = succ @@ List.fold_left Stdlib.max 0 indexes in *)
(*   return @@ prefix ^ "--" ^ string_of_int next_index *)

let archive ?git_path ?(paths = []) ?(commit = "HEAD") ~output_file () =
  git ?git_path @@ ["archive"; "--output"; output_file; commit] @ paths

let branch_remove ?git_path ?(force = false) branch_name =
  git ?git_path
  @@ ["branch"; "--delete"; branch_name]
  @ optional_switch "force" force

let branch_remove_remote ?git_path ?(force = false) ~remote ~branch_name () =
  (* $ git push -d <remote_name> <branch_name> *)
  git ?git_path
  @@ ["push"; "--delete"; remote; branch_name]
  @ optional_switch "force" force

let symbolic_ref ?git_path ref_ =
  git_and_read_stdout ?git_path ["symbolic-ref"; ref_]

let log ?git_path ?max_count ?before ?format ?ref_ () =
  git_and_read_stdout ?git_path
  @@ ["log"]
  @ optional_arg "max-count" string_of_int max_count
  @ optional_arg "before" Fun.id before
  @ optional_set_arg "format" Fun.id format
  @ Option.to_list ref_

let repack ?git_path ?(remove_redundant = false) () =
  git ?git_path @@ ["repack"] @ optional_switch_short 'd' remove_redundant

module Worktree = struct
  let add ?git_path ?commit_ish path =
    git ?git_path @@ ["worktree"; "add"; path] @ Option.to_list commit_ish

  let remove ?git_path ?(force = false) work_dir =
    git ?git_path
    @@ ["worktree"; "remove"; work_dir]
    @ optional_switch "force" force
end

type numstat = {additions : int; deletions : int}

let numstat ~git_path ~commit1 ~commit2 =
  let* diff =
    git_and_read_stdout
      ~git_path
      ["diff"; sf "%s..%s" commit1 commit2; "--numstat"]
  in
  let diff_lines = String.split_on_char '\n' (String.trim diff) in
  let additions, deletions =
    List.fold_left
      (fun (additions, deletions) line ->
        match String.split_on_char '\t' line with
        | [additions'; deletions'; _path] ->
            ( additions
              + (Option.value ~default:0 @@ int_of_string_opt additions'),
              deletions
              + (Option.value ~default:0 @@ int_of_string_opt deletions') )
        | _ -> failwith "Unexpected ")
      (0, 0)
      diff_lines
  in
  return {additions; deletions}

let commit_parents ~git_path ~commit =
  let* s = git_and_read_stdout ~git_path ["rev-parse"; sf "%s^@" commit] in
  return (String.split_on_char '\n' (String.trim s))
