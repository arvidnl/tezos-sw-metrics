open Tezos_sw_metrics_time

type mr_stats = {
  mr_iid : string;
  mr_title : string;
  merged_time : float;
      (** Time from [created_at] to [merged_at], in seconds. *)
  merged_time_workseconds : int;
      (** Time from [created_at] to [merged_at], excluding weekends.

      See {!Time.datetime_workdays_diff} for details on how this is defined. *)
  additions : int;
  deletions : int;
  changes : int;
  created_at : float;
  merged_at : float;
  margebot_assigned_last : float;
      (** Time from last assignment to marge-bot to [merged_at], in seconds. *)
  is_merge_train : bool;
  head_pipeline_id : int;
  head_pipeline_ref : string;
  head_pipeline_duration : float;
}

type column =
  | Mr_iid
  | Mr_title
  | Merged_time
  | Merged_time_workseconds
  | Additions
  | Deletions
  | Changes
  | Created_at
  | Merged_at
  | Margebot_assigned_last
  | Is_merge_train
  | Head_pipeline_id
  | Head_pipeline_ref
  | Head_pipeline_duration

let show_column = function
  | Mr_iid -> "mr_iid"
  | Mr_title -> "mr_title"
  | Merged_time -> "merged_time"
  | Merged_time_workseconds -> "merged_time_workseconds"
  | Additions -> "additions"
  | Deletions -> "deletions"
  | Changes -> "changes"
  | Created_at -> "created_at"
  | Merged_at -> "merged_at"
  | Margebot_assigned_last -> "margebot_assigned_last"
  | Is_merge_train -> "is_merge_train"
  | Head_pipeline_id -> "head_pipeline_id"
  | Head_pipeline_ref -> "head_pipeline_ref"
  | Head_pipeline_duration -> "head_pipeline_duration"

let mr_stats_schema =
  Row_schema.
    {
      measurement = "tezos_tezos_mr_merge_time_diff_size";
      name_to_string = show_column;
      fields =
        Conv
          ( (fun {
                   mr_iid;
                   merged_time;
                   merged_time_workseconds;
                   additions;
                   deletions;
                   changes;
                   created_at;
                   merged_at;
                   margebot_assigned_last;
                   is_merge_train;
                   mr_title;
                   head_pipeline_id;
                   head_pipeline_ref;
                   head_pipeline_duration;
                 } ->
              ( ( mr_iid,
                  merged_time,
                  merged_time_workseconds,
                  additions,
                  deletions,
                  changes ),
                ( created_at,
                  merged_at,
                  margebot_assigned_last,
                  is_merge_train,
                  mr_title ),
                (head_pipeline_id, head_pipeline_ref, head_pipeline_duration) )),
            Fields3
              ( Fields6
                  ( String Mr_iid,
                    Float Merged_time,
                    Int Merged_time_workseconds,
                    Int Additions,
                    Int Deletions,
                    Int Changes ),
                Fields5
                  ( Float Created_at,
                    Float Merged_at,
                    Float Margebot_assigned_last,
                    Bool Is_merge_train,
                    String Mr_title ),
                Fields3
                  ( Int Head_pipeline_id,
                    String Head_pipeline_ref,
                    Float Head_pipeline_duration ) ) );
      tags = None;
    }

let panels () =
  (* Offset time groups by 4 days such that weeks start on monday instead of thursday (??)
     From: https://community.grafana.com/t/group-by-week-data-displayed-on-thursday/3805/3 *)
  let offset = InfluxDB.D 4 in
  let size_graph =
    let query ?upper ?lower () =
      InfluxDB.(
        (* SELECT count(mr_iid) FROM
           (SELECT * FROM "tezos_tezos_mr_merge_time_diff_size" WHERE "additions" + "deletions" >= 1000)
           WHERE $timeFilter GROUP BY time(24h) fill(0)
        *)
        select
          [Function (COUNT, Field (show_column Mr_iid))]
          ~from:
            (Select
               (select
                  [All]
                  ~from:(Measurement mr_stats_schema.measurement)
                  ?where:
                    (let lower =
                       Option.map
                         (fun lower ->
                           Field (show_column Changes, GE, Float lower))
                         lower
                     in
                     let upper =
                       Option.map
                         (fun upper ->
                           Field (show_column Changes, LE, Float upper))
                         upper
                     in
                     match (lower, upper) with
                     | Some lower, Some upper -> Some (And (lower, upper))
                     | None, Some _ -> upper
                     | Some _, None -> lower
                     | None, None -> None)))
          ~where:Grafana_time_filter
          ~group_by:(group_by_time ~offset ~fill:F_none (D 1)))
    in
    let queries =
      [
        Grafana.target_query ~alias:"< 100 LOCs" @@ query ~upper:100.0 ();
        Grafana.target_query ~alias:"100 <= LOCs < 1000"
        @@ query ~lower:100.0 ~upper:1000.0 ();
        Grafana.target_query ~alias:"LOCs > 1000" @@ query ~lower:1000.0 ();
      ]
    in
    Grafana.Graph
      {
        title = "MRs by size";
        description = "MRs grouped by size and day";
        queries;
        interval = None;
        yaxis_1 = Some {format = ""; label = Some "Count"};
        yaxis_2 = None;
        stacking = Some Normal;
        drawstyle = Bars;
        tooltip_mode = Multi;
        overrides = None;
        transformations = None;
      }
  in
  let merge_time_graph =
    (* SELECT average(merged_time) FROM "tezos_tezos_mr_merge_time_diff_size" WHERE $timeFilter *)
    let group_by =
      InfluxDB.(group_by_time ~offset ~fill:F_none Grafana_interval)
    in
    let from = InfluxDB.(Measurement mr_stats_schema.measurement) in
    let where = InfluxDB.Grafana_time_filter in
    let query_merged_time =
      InfluxDB.(
        select
          [Function (MEAN, Field (show_column Merged_time))]
          ~from
          ~group_by
          ~where)
    in
    let query_merged_time_workdays =
      InfluxDB.(
        select
          [Function (MEAN, Field (show_column Merged_time_workseconds))]
          ~from
          ~group_by
          ~where)
    in
    let query_count =
      InfluxDB.(
        select
          [Function (COUNT, Field (show_column Merged_time))]
          ~from
          ~group_by
          ~where)
    in
    let overrides =
      Overrides.(
        [
          Match.by_name "Count"
          ==> [
                Properties.axis_placement Right;
                Properties.draw_style Bars;
                Properties.unit "MRs";
              ];
        ]
        |> of_list)
    in
    Grafana.Graph
      {
        title = "Average merge time per day";
        description =
          "On each day the average number of days MRs merged that day was open.";
        queries =
          [
            Grafana.target_query ~alias:"Merge time in days" query_merged_time;
            Grafana.target_query
              ~alias:"Merge time in workdays"
              query_merged_time_workdays;
            Grafana.target_query ~alias:"Count" query_count;
          ];
        interval = Some (Days 1);
        yaxis_1 = Some {format = "s"; label = None};
        yaxis_2 = Some {format = "none"; label = None};
        stacking = None;
        drawstyle = Lines;
        tooltip_mode = Multi;
        overrides;
        transformations = None;
      }
  in
  let week_merge_proportion_graph interval =
    (* SELECT average(merged_time) FROM "tezos_tezos_mr_merge_time_diff_size" WHERE $timeFilter *)
    let threshold = 5.0 *. 24.0 *. 3600.0 in
    let group_by =
      InfluxDB.(group_by_time ~offset ~fill:F_none Grafana_interval)
    in
    let from = InfluxDB.(Measurement mr_stats_schema.measurement) in
    let query_merged_le_workweek =
      InfluxDB.(
        select
          [Function (COUNT, Field (show_column Merged_time_workseconds))]
          ~from
          ~group_by
          ~where:
            (And
               ( Field (show_column Merged_time_workseconds, LE, Float threshold),
                 Grafana_time_filter )))
    in
    let query_merged_gt_workweek =
      InfluxDB.(
        select
          [Function (COUNT, Field (show_column Merged_time_workseconds))]
          ~from
          ~group_by
          ~where:
            (And
               ( Field (show_column Merged_time_workseconds, GT, Float threshold),
                 Grafana_time_filter )))
    in
    let query_merged_count =
      InfluxDB.(
        select
          [Function (COUNT, Field (show_column Merged_time_workseconds))]
          ~from
          ~group_by
          ~where:Grafana_time_filter)
    in
    let alias_merged_le_workweek = "Merged in less than one week" in
    let alias_merged_gt_workweek = "Merged in more than one week" in
    let alias_total = "Total" in
    Grafana.Graph
      {
        title =
          sf
            "Proportion of MRs merged in less than one work week (per %s)"
            (Grafana.string_of_duration interval);
        description =
          sf
            "The proportion of MRs that was merged within 5 work days (per %s)."
            (Grafana.string_of_duration interval);
        queries =
          [
            Grafana.target_query
              ~alias:alias_merged_le_workweek
              query_merged_le_workweek;
            Grafana.target_query
              ~alias:alias_merged_gt_workweek
              query_merged_gt_workweek;
            Grafana.target_query ~alias:alias_total query_merged_count;
          ];
        interval = Some interval;
        yaxis_1 = Some {format = "percentunit"; label = None};
        yaxis_2 = Some {format = "MRs"; label = None};
        stacking = Some Normal;
        drawstyle = Lines;
        tooltip_mode = Multi;
        overrides =
          Overrides.(
            [
              Match.by_name alias_merged_le_workweek
              ==> [
                    Properties.axis_placement Right;
                    Properties.draw_style Bars;
                    Properties.unit "MRs";
                  ];
              Match.by_name alias_merged_gt_workweek
              ==> [
                    Properties.axis_placement Right;
                    Properties.draw_style Bars;
                    Properties.unit "MRs";
                  ];
              Match.by_name alias_total
              ==> [
                    Properties.hide_in_area
                      ~viz:true
                      ~legend:true
                      ~tooltip:true
                      ();
                  ];
            ]
            |> of_list);
        transformations =
          Transformations.of_list
            [
              Calculate_field
                {
                  reducer = Sum;
                  operation =
                    Binary
                      {
                        left = alias_merged_le_workweek;
                        bin_op = Div;
                        right = alias_total;
                      };
                  replace_fields = false;
                };
            ];
      }
  in
  (* percent of MRs merged within the hour after assignment to marge *)
  let margebot_queuetime_proportion_graph interval =
    let threshold = 1.0 *. 3600.0 in
    let group_by =
      InfluxDB.(group_by_time ~offset ~fill:F_none Grafana_interval)
    in
    let from = InfluxDB.(Measurement mr_stats_schema.measurement) in
    let query_queuetime_le_threshold =
      InfluxDB.(
        select
          [Function (COUNT, Field (show_column Margebot_assigned_last))]
          ~from
          ~group_by
          ~where:
            (And
               ( Field (show_column Margebot_assigned_last, LE, Float threshold),
                 Grafana_time_filter )))
    in
    let query_queuetime_gt_threshold =
      InfluxDB.(
        select
          [Function (COUNT, Field (show_column Margebot_assigned_last))]
          ~from
          ~group_by
          ~where:
            (And
               ( Field (show_column Margebot_assigned_last, GT, Float threshold),
                 Grafana_time_filter )))
    in
    let query_total =
      InfluxDB.(
        select
          [Function (COUNT, Field (show_column Merged_time_workseconds))]
          ~from:(Measurement mr_stats_schema.measurement)
          ~group_by
          ~where:Grafana_time_filter)
    in
    let alias_queuetime_gt_threshold = "More than 1 hour in queue" in
    let alias_queuetime_le_threshold = "Less than 1 hour in queue" in
    let alias_total = "Total" in
    Grafana.Graph
      {
        title =
          sf
            "Proportion of MRs that spent less than one hour in marge-bot \
             queue (per %s)"
            (Grafana.string_of_duration interval);
        description =
          sf
            "The proportion of MRs that spent less than one hour in \
             marge-bot's queue (per %s)."
            (Grafana.string_of_duration interval);
        queries =
          [
            Grafana.target_query
              ~alias:alias_queuetime_le_threshold
              query_queuetime_le_threshold;
            Grafana.target_query
              ~alias:alias_queuetime_gt_threshold
              query_queuetime_gt_threshold;
            Grafana.target_query ~alias:alias_total query_total;
          ];
        interval = Some interval;
        yaxis_1 = Some {format = "percentunit"; label = None};
        yaxis_2 = Some {format = "MRs"; label = None};
        stacking = Some Normal;
        drawstyle = Lines;
        tooltip_mode = Multi;
        overrides =
          Overrides.(
            [
              Match.by_name alias_queuetime_le_threshold
              ==> [
                    Properties.axis_placement Right;
                    Properties.draw_style Bars;
                    Properties.unit "MRs";
                  ];
              Match.by_name alias_queuetime_gt_threshold
              ==> [
                    Properties.axis_placement Right;
                    Properties.draw_style Bars;
                    Properties.unit "MRs";
                  ];
              Match.by_name alias_total
              ==> [
                    Properties.hide_in_area
                      ~viz:true
                      ~legend:true
                      ~tooltip:true
                      ();
                  ];
            ]
            |> of_list);
        transformations =
          Transformations.of_list
            [
              Calculate_field
                {
                  reducer = Sum;
                  operation =
                    Binary
                      {
                        left = alias_queuetime_le_threshold;
                        bin_op = Div;
                        right = alias_total;
                      };
                  replace_fields = false;
                };
            ];
      }
  in
  Grafana.
    [
      Row "MRs metrics";
      size_graph;
      merge_time_graph;
      week_merge_proportion_graph (Days 1);
      week_merge_proportion_graph (Weeks 1);
      margebot_queuetime_proportion_graph (Days 1);
      margebot_queuetime_proportion_graph (Weeks 1);
    ]

let get_last_margebot_assignment ~project_id ~token ~marge_bot_user ?before
    ~merge_request_iid () : float option Gitlab.Monad.t =
  let open Gitlab.Monad in
  let notes =
    (* Notes as sorted by [created_at] by default *)
    Gitlab.Project.Notes.Merge_request.list
      ~token
      ~project_id
      ~merge_request_iid:(string_of_int merge_request_iid)
      ~sort:`Desc
      ()
  in
  let* note_opt =
    Gitlab.Stream.find
      (fun (note : Gitlab_t.note) ->
        let is_before =
          match before with
          | Some before -> note.note_created_at < before
          | None -> true
        in
        let body = note.note_body in
        is_before && body =~ rex ("assigned to @" ^ marge_bot_user))
      notes
  in
  match note_opt with
  | Some (note, _remaining_notes) -> return (Some note.note_created_at)
  | None ->
      Log.warn
        "Found no assignment to marge_bot_user (@%s) for !%d: queue/processing \
         time will not be measured"
        marge_bot_user
        merge_request_iid ;
      return None

let register ~executors =
  Long_test.register
    ~__FILE__
    ~executors
    ~title:"tezos/tezos merge time vs diff size metrics"
    ~tags:["sw_metrics"; "merge_requests"; "diff_size"; "marge_bot"]
    ~timeout:(Hours 1)
  @@ fun () ->
  (*

Find all merged merge requests updated since DATE.

Filter out all MRs whose merged_at is anterior to DATE.

DATE is either set by a cli arg, or it is the merged_date for the last data point.

Get the diff stat for each MR.

Push it to influxdb.

     *)
  let* merged_after =
    Util.with_backfill ~__FILE__ "mrs_merged_after" @@ fun () ->
    let* last =
      Influxdb_util.get_last_datapoint_timestamp mr_stats_schema.measurement
    in
    Option.iter
      (fun date ->
        Log.info
          "Fetching all metrics since last treated MR from %s (%f)"
          (Time.string_of_datetime date)
          date)
      last ;
    return last
  in
  let merged_before =
    Tezt_cli_arg.get_opt ~__FILE__ Fun.id "mrs_merged_before"
    |> Option.map @@ fun date_string ->
       let date = Time.datetime_of_string ~reqtime:false date_string in
       Log.info
         "Restricting to MRs merged before (inclusive): %s (%f)"
         (Time.string_of_datetime date)
         date ;
       date
  in
  let target_branch =
    Tezt_cli_arg.get ~__FILE__ ~default:"master" Option.some "target_branch"
  in
  let Tezt_cli_arg.{gl_access_token = token; project_short_ref; marge_bot_user}
      =
    Tezt_cli_arg.get_shared_opts ()
  in

  let* project =
    Gitlab.Monad.run
    @@
    let open Gitlab.Monad in
    let* projects =
      Gitlab.Project.by_short_ref ?token ~short_ref:project_short_ref ()
    in
    match Gitlab.Response.value projects with
    | Some project -> return project
    | None -> Test.fail "Could not find project %s" project_short_ref
  in
  Log.info
    "Fetching metrics from project %s: %d"
    project.project_short_name_with_namespace
    project.project_short_id ;

  let* repository =
    let* git_path =
      Repo_cache.get
        ~default_branch:target_branch
        project.project_short_http_url_to_repo
    in
    return git_path
  in
  Log.info "Repository in %s" repository ;

  let open Gitlab.Monad in
  run
  @@
  let mrs =
    Mtc_metrics.get_mr_metrics
      ~warn:(fun s ->
        Log.warn "%s" s ;
        unit)
      ~info:(fun s ->
        Log.info "%s" s ;
        unit)
      ?token
      ~merged_after
      ?merged_before
      ~repository
      ~target_branch
      ~order_by:`Updated_at
      ~sort:`Asc
      project.project_short_id
  in
  let get_last_margebot_assignment =
    match token with
    | Some token ->
        get_last_margebot_assignment
          ~token
          ~project_id:project.project_short_id
          ~marge_bot_user
    | None ->
        Log.warn "No token, will not get margebot assignments" ;
        fun ?before ~merge_request_iid () ->
          ignore (before, merge_request_iid) ;
          return None
  in
  mrs
  |> Gitlab.Stream.iter
     @@ fun (Mtc_metrics.{mr; numstat; merged_at} as metrics) ->
     Log.info "Treating MR: %s" (Mtc_metrics.show_mr_metrics metrics) ;
     let created_at = mr.merge_request_full_created_at in
     let merge_time_seconds = merged_at -. created_at in
     let merge_time_workseconds =
       Time.datetime_workdays_diff created_at merged_at
     in
     let* margebot_assigned_last =
       get_last_margebot_assignment
         ~merge_request_iid:mr.merge_request_full_iid
         ()
     in
     (* Time from assignment to marge-bot to merge, e.g. queue time.

        TODO: handle target branches here, such that we remove
        time spent with a non-master target_branch ? Finally, I'm
        not sure it makes any sense to do so. If we assume the MR
        was part of a stack, then the time spent with a non-master
        target_branch is also queue time. Removing queue time
        makes sense only if we consider that non-master targetting
        MRs are somehow not meant to be merged (but why then
        assign to marge?). *)
     let margebot_assigned_last =
       match margebot_assigned_last with
       | Some assignment ->
           Log.info
             "Last marge-bot assignment: %s"
             (Time.string_of_datetime assignment) ;
           merged_at -. assignment
       | None -> 0.0
     in
     let head_pipeline_opt = mr.merge_request_full_head_pipeline in
     let ( is_merge_train,
           head_pipeline_id,
           head_pipeline_ref,
           head_pipeline_duration ) =
       match head_pipeline_opt with
       | None ->
           Log.info "No head_pipeline, assuming non-merge train" ;
           (false, 0, "", 0.0)
       | Some head_pipeline ->
           Log.info "Ref of head pipeline: %s" head_pipeline.ref ;
           let is_merge_train =
             head_pipeline.ref =~ rex "refs\\/merge-requests\\/\\d+\\/train"
           in
           Log.info
             "Ref of head pipeline: %s, is this a merge train pipeline: %b."
             head_pipeline.ref
             is_merge_train ;
           ( is_merge_train,
             head_pipeline.id,
             head_pipeline.ref,
             Option.value ~default:0.0 head_pipeline.duration )
     in
     let mr_stats =
       {
         mr_iid = string_of_int mr.merge_request_full_iid;
         mr_title = mr.merge_request_full_title;
         merged_time = merge_time_seconds;
         merged_time_workseconds = merge_time_workseconds;
         additions = numstat.additions;
         deletions = numstat.deletions;
         changes = numstat.additions + numstat.deletions;
         created_at;
         merged_at;
         margebot_assigned_last;
         is_merge_train;
         head_pipeline_id;
         head_pipeline_ref;
         head_pipeline_duration;
       }
     in
     Long_test.add_data_point
       (Row_schema.to_data_point
          ~timestamp:merged_at
          mr_stats_schema
          mr_stats
          ()) ;
     return ()
