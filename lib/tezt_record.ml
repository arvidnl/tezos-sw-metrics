open Tezt
open Tezt.Base

type test = Testowners.test

type t = test String_map.t

let empty = String_map.empty

let of_json (json : JSON.t) : t =
  let open JSON in
  let test_of_record_json (test_json : JSON.t) : test =
    {
      file = test_json |-> "file" |> as_string;
      title = test_json |-> "title" |> as_string;
      tags = test_json |-> "tags" |> as_list |> List.map as_string;
    }
  in
  json |> as_list
  |> List.fold_left
       (fun t test_json ->
         let test = test_of_record_json test_json in
         String_map.add test.title test t)
       String_map.empty

let of_file (path : string) : t = of_json (JSON.parse_file path)

let union ?on_conflict (t1 : t) (t2 : t) : t =
  let join =
    Option.value
      ~default:(fun test_title test1 _test2 ->
        Log.warn
          "The test %S appears twice, which should not be possible"
          test_title ;
        Some test1)
      on_conflict
  in
  String_map.union join t1 t2

let unions ?on_conflict ts = List.fold_left (union ?on_conflict) empty ts

let of_files (paths : string list) : t = paths |> List.map of_file |> unions

let lookup title records : test option = String_map.find_opt title records
