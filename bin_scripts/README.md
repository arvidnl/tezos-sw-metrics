# Tezos CI scripts

This folder contains a bunch of scripts for the
[tezos/tezos](https://gitlab.com/tezos/tezos) project. Most of them
will have little use for other people than me, but here are some
instructions anyhow.

## Installation

These scripts are in a subfolder of a fork of the
[ocaml-gitlab](https://github.com/tmcgilchrist/ocaml-gitlab/)
project. This makes it easier to upstream patches I make to that
library.

To just use the scripts, the general procedure would be:

 - create a switch
 - install the `ocaml-gitlab` dependencies
 - do `dune build`

I don't think there are any other dependencies to install. Sorry for
the poor installation instructions, if you can help me improve them or
improve the layout of this repo, do not hesitate.

## Overview

Here are the scripts that might have some use for other peoples:

 - `testing_effort`: Used to generate the agenda for the testing effort
 - `retried_tests`: Detects flaky tests by looking for retried jobs
 - `diagnose`: Dignose the failures in a given job
 - `mtc_metrics`: Merge team coordination metrics
 - `sections`: Printing the timing of a specific job

## Usage

### `diagnose`

It is used to diagnose failures on a tezos/tezos pipelines. In particular, to diagnose issues on the extended pipelines. Example:


Example invocation:

```
$ dune exec ./diagnose.exe -- --private-token glpat- ... --format md 566131974
```

Example output:

> Found 3 failures in pipeline [#566131974](https://gitlab.com/tezos/tezos/-/pipelines/566131974) in project 3836952:
> 
>  - job [#2603259062](https://gitlab.com/tezos/tezos/-/jobs/2603259062) (`install_opam_focal`):
> 
>    To retry the full job, do: `./docs/scripts/test_doc_scripts.sh install-opam-focal`
> 
>  - job [#2603259061](https://gitlab.com/tezos/tezos/-/jobs/2603259061) (`install_opam_bionic`):
> 
>    To retry the full job, do: `./docs/scripts/test_doc_scripts.sh install-opam-bionic`
> 
>  - job [#2603259036](https://gitlab.com/tezos/tezos/-/jobs/2603259036) (`documentation:linkcheck`):
> 
>    To retry the full job, do: `make -C docs linkcheck`
> 
> Create issue: https://gitlab.com/tezos/tezos/-/issues/new?issue%5Btitle%5D=Extended%20test%20suite%20failure:%20%60install_opam_focal%60%2C%20%60install_opam_bionic%60%2C%20%60documentation:linkcheck%60%20(2022-06-17%20--%20...)&issue%5Bdescription%5D=Found%203%20failures%20in%20pipeline%20%5B%23566131974%5D(https://gitlab.com/tezos/tezos/-/pipelines/566131974)%20in%20project%203836952:%0A%0A%20-%20job%20%5B%232603259062%5D(https://gitlab.com/tezos/tezos/-/jobs/2603259062)%20(%60install_opam_focal%60):%0A%0A%20%20%20To%20retry%20the%20full%20job%2C%20do:%20%60./docs/scripts/test_doc_scripts.sh%20install-opam-focal%60%0A%0A%20-%20job%20%5B%232603259061%5D(https://gitlab.com/tezos/tezos/-/jobs/2603259061)%20(%60install_opam_bionic%60):%0A%0A%20%20%20To%20retry%20the%20full%20job%2C%20do:%20%60./docs/scripts/test_doc_scripts.sh%20install-opam-bionic%60%0A%0A%20-%20job%20%5B%232603259036%5D(https://gitlab.com/tezos/tezos/-/jobs/2603259036)%20(%60documentation:linkcheck%60):%0A%0A%20%20%20To%20retry%20the%20full%20job%2C%20do:%20%60make%20-C%20docs%20linkcheck%60%0A%

Note the `Create issue` link: it'll help you open a new issue for such problems with a title and description pre-formatted.

Note the commands for retrying the jobs: they'll help you bisect the issue.
