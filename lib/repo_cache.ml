open Tezt.Base

let dir_of_url url =
  let sanitized =
    let sanitize_char = function
      | ( 'a' .. 'z'
        | 'A' .. 'Z'
        | '0' .. '9'
        | '_' | '-' | '.' | ' ' | '(' | ')' ) as x ->
          x
      | _ -> '-'
    in
    String.map sanitize_char url
  in
  "_repo_cache/" ^ sanitized

let get_at_sha ?(depth = 1) ~url sha =
  let directory = dir_of_url url in
  let* () =
    if not (Sys.file_exists directory && Sys.is_directory directory) then
      Git.clone ~directory ~depth:1 url
    else unit
  in
  let* () = Git.fetch ~git_path:directory ~depth "origin" [sha] in
  let* () = Git.checkout ~git_path:directory sha in
  return directory

let get ?depth ?shallow_since ?default_branch url =
  let directory = dir_of_url url in
  let* () =
    if Sys.file_exists directory && Sys.is_directory directory then
      let* default_branch =
        match default_branch with
        | Some default_branch -> return default_branch
        | None ->
            let* default_branch =
              Git.symbolic_ref ~git_path:directory "refs/remotes/origin/HEAD"
            in
            let default_branch =
              default_branch |> String.split_on_char '/' |> List.rev |> List.hd
              |> String.trim
            in
            return default_branch
      in
      let* () =
        Git.fetch
          ~git_path:directory
          ?depth
          ?shallow_since
          "origin"
          [default_branch]
      in
      Git.checkout ~git_path:directory ("origin/" ^ default_branch)
    else Git.clone ~directory ?depth ?shallow_since url
  in
  return directory
