(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Tezt

type direction = Asc | Desc

type reducer = Sum

type bin_op = Div

type operation = Binary of {bin_op : bin_op; left : string; right : string}

type transform =
  | Merge
  | Sort_by of {field : string; direction : direction}
  | Calculate_field of {
      reducer : reducer;
      operation : operation;
      replace_fields : bool;
    }

let encode : transform -> JSON.u =
  let f id options : JSON.u = `O [("id", `String id); ("options", options)] in
  function
  | Merge -> f "merge" (`O [])
  | Sort_by {field; direction} ->
      f
        "sortBy"
        (`O
          [
            ("fields", `O []);
            ( "sort",
              `A
                [
                  `O
                    (("field", `String field)
                    ::
                    (match direction with
                    | Desc -> [("desc", `Bool true)]
                    | Asc -> []));
                ] );
          ])
  | Calculate_field {reducer; operation; replace_fields} ->
      let encode_reducer = function Sum -> `String "sum" in
      let encode_operation = function
        | Binary {bin_op; left; right} ->
            let operator = match bin_op with Div -> "/" in
            [
              ("mode", `String "binary");
              ( "binary",
                `O
                  [
                    ("left", `String left);
                    ("operator", `String operator);
                    ("right", `String right);
                  ] );
            ]
      in
      f
        "calculateField"
        (`O
          (encode_operation operation
          @ [
              ("reduce", `O [("reducer", encode_reducer reducer)]);
              ("replaceFields", `Bool replace_fields);
            ]))

(*
   {
      "id": "calculateField",
      "options": {
        "mode": "binary",
        "reduce": {
          "reducer": "sum"
        },
        "binary": {
          "left": "Fast",
          "operator": "/",
          "right": "Total"
        },
        "replaceFields": true
      }
    }
*)

let of_list ts = Some (`A (List.map encode ts))
