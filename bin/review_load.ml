(* This metrics collects the number of MRs open for a given project,
   and groups them by [milestone=None] / [milestone<>None] and
   [draft=yes] / [draft=no]. *)

let measurement_assignment_load = "tezos_tezos_assignement_load"

let test_title_assignment_load = "tezos/tezos assignment load"

type role = Author | Reviewer | Assignee

type user_mrs = {
  assigned_to : Gitlab_t.merge_request list;
  reviewer_of : Gitlab_t.merge_request list;
  author_of : Gitlab_t.merge_request list;
}

let user_mrs_empty : user_mrs =
  {assigned_to = []; reviewer_of = []; author_of = []}

type user_tbl = (string, user_mrs) Hashtbl.t

let update ~default tbl key f_value =
  match f_value (Hashtbl.find_opt tbl key |> Option.value ~default) with
  | None -> Hashtbl.remove tbl key
  | Some value -> Hashtbl.replace tbl key value

let add (tbl : user_tbl) user role mr =
  let open Gitlab_t in
  let user_neq u1 u2 = u1.user_short_id <> u2.user_short_id in
  let username = user.user_short_username in
  update ~default:user_mrs_empty tbl username @@ fun user_mrs ->
  Some
    (* Only add the user if they are not the author of the MR --
       we don't care about self-assignments *)
    (match role with
    | Assignee when user_neq mr.merge_request_author user ->
        Log.debug "%s assigned to !%d" username mr.merge_request_iid ;
        {user_mrs with assigned_to = mr :: user_mrs.assigned_to}
    | Reviewer when user_neq mr.merge_request_author user ->
        Log.debug "%s reviewer of !%d" username mr.merge_request_iid ;
        {user_mrs with reviewer_of = mr :: user_mrs.reviewer_of}
    | Author ->
        Log.debug "%s author of !%d" username mr.merge_request_iid ;
        {user_mrs with author_of = mr :: user_mrs.author_of}
    | _ -> user_mrs)

type assignment = {
  assigned_to : int;
  reviewer_of : int;
  author_of : int;
  username : string;
}

type column = Assigned_to | Reviewer_of | Author_of | Username

let show_column = function
  | Assigned_to -> "assigned_to"
  | Reviewer_of -> "reviewer_of"
  | Author_of -> "author_of"
  | Username -> "username"

type tags = Username

let assignments_schema =
  Row_schema.
    {
      measurement = measurement_assignment_load;
      name_to_string = show_column;
      fields =
        Conv
          ( (fun {assigned_to; reviewer_of; author_of; username} ->
              (assigned_to, reviewer_of, author_of, username)),
            Fields4
              (Int Assigned_to, Int Reviewer_of, Int Author_of, String Username)
          );
      tags = Conv ((fun {username; _} -> username), String "username");
    }

(* Review load panels *)
let review_load_panel () =
  (* SELECT "username"::field, "assigned_to"::field, "reviewer_of"::field FROM (
        SELECT LAST("assigned_to") as "assigned_to",
               LAST("reviewer_of") as "reviewer_of"
        FROM "tezos_tezos_assignement_load"
        WHERE $timeFilter GROUP BY "username") WHERE
     ("assigned_to" > 0 or "reviewer_of" > 0) *)
  let assigned_to = "assigned_to" in
  let reviewer_of = "reviewer_of" in
  let author_of = "author_of" in
  let table_query =
    InfluxDB.(
      select
        [
          Field "username"; Field assigned_to; Field reviewer_of; Field author_of;
        ]
        ~from:
          (Select
             (select
                [
                  As (Function (LAST, Field assigned_to), assigned_to);
                  As (Function (LAST, Field reviewer_of), reviewer_of);
                  As (Function (LAST, Field author_of), author_of);
                ]
                ~from:(Measurement measurement_assignment_load)
                ~where:Grafana_time_filter
                ~group_by:(Tags ["username"])))
        ~where:
          (Or
             ( Or
                 ( Field (assigned_to, GT, Float 0.0),
                   Field (reviewer_of, GT, Float 0.0) ),
               Field (author_of, GT, Float 0.0) )))
  in
  let format_date timestamp =
    let time = Unix.localtime timestamp in
    sf
      "%d-%02d-%02d %02d:%02d:%02d"
      (time.tm_year + 1900)
      time.tm_mon
      time.tm_mday
      time.tm_hour
      time.tm_min
      time.tm_sec
  in
  let assigned_to_display_name = "Assigned MRs, non-author" in
  Grafana.(
    Table
      {
        title = sf "Review load at %s" (Unix.gettimeofday () |> format_date);
        description = "Number of MRs assigned / to review per user";
        queries = [target_query ~result_format:Table table_query];
        sort_by = Some (assigned_to_display_name, true);
        overrides =
          Overrides.(
            of_list
              [
                (Match.by_name "Time" ==> Properties.[hidden true]);
                (Match.by_name "username"
                ==> Properties.
                      [
                        filterable true;
                        links
                          ~title:"${__data.fields.username}'s GitLab profile"
                          (Gitlab_url.user "${__data.fields.username}");
                        display_name "User";
                      ]);
                (Match.by_name assigned_to
                ==> Properties.
                      [
                        display_mode "basic";
                        links
                          ~title:"See assigned MRs"
                          (Gitlab_url.assignments "${__data.fields.username}");
                        display_name assigned_to_display_name;
                      ]);
                (Match.by_name reviewer_of
                ==> Properties.
                      [
                        display_mode "basic";
                        links
                          ~title:"See MRs to review"
                          (Gitlab_url.reviews "${__data.fields.username}");
                        display_name "MRs to review, non-author";
                      ]);
                (Match.by_name author_of
                ==> Properties.
                      [
                        display_mode "basic";
                        links
                          ~title:"See opened, authored MRs"
                          (Gitlab_url.author_of "${__data.fields.username}");
                        display_name "Authored, open MRs";
                      ]);
              ]);
        transformations = None;
        unit = Some "MRs";
      })

let panels () =
  Grafana.[Row "MRs assignment and review load"; review_load_panel ()]

let assignment_load ~executors =
  Long_test.register
    ~__FILE__
    ~executors
    ~title:test_title_assignment_load
    ~tags:["sw_metrics"; "merge_requests"; "review_load"; "marge_bot"]
    ~timeout:(Hours 1)
  @@ fun () ->
  let Tezt_cli_arg.{gl_access_token = token; project_short_ref; marge_bot_user}
      =
    Tezt_cli_arg.get_shared_opts ()
  in

  let* project =
    Gitlab.Monad.run
    @@
    let open Gitlab.Monad in
    let* projects =
      Gitlab.Project.by_short_ref ?token ~short_ref:project_short_ref ()
    in
    match Gitlab.Response.value projects with
    | Some project -> return project
    | None -> Test.fail "Could not find project %s" project_short_ref
  in
  Log.info
    "Fetching review load from project %s: %d"
    project.project_short_name_with_namespace
    project.project_short_id ;

  let users_always_track =
    Tezt_cli_arg.get_opt ~__FILE__ (String.split_on_char ',') "track_users"
    |> Option.value ~default:[marge_bot_user]
  in

  let* () =
    Gitlab.Monad.(
      run
      @@
      (* Get all open MRs *)
      (* for each MR, get assignees and reviewers *)
      (* [get cloc of MR [and gzipped cloc]] *)
      (* map usernames to [ assigned mr + size, reviewer mr + size ]) *)
      (* send one [assignment] data-point per username *)
      let tbl : user_tbl = Hashtbl.create 5 in
      (* Add empty slots for users from [users_always_track]. This
         avoids gaps in the data series. *)
      List.iter
        (fun username -> Hashtbl.add tbl username user_mrs_empty)
        users_always_track ;
      let mrs =
        Gitlab.Project.merge_requests
          ?token
          ~state:`Opened
          ~target_branch:"master"
          ~per_page:100
          ~id:project.project_short_id
          ()
      in

      let* () =
        mrs
        |> Gitlab.Stream.iter @@ fun (mr : Gitlab_t.merge_request) ->
           (match mr.merge_request_assignee with
           | None -> ()
           | Some assigned_to -> add tbl assigned_to Assignee mr) ;
           (mr.merge_request_reviewers
           |> List.iter @@ fun reviewer -> add tbl reviewer Reviewer mr) ;
           add tbl mr.merge_request_author Author mr ;
           return ()
      in
      let timestamp = Unix.gettimeofday () in

      (tbl |> Hashtbl.to_seq |> List.of_seq
      |> List.iter
         @@ fun (username, ({assigned_to; reviewer_of; author_of} : user_mrs))
           ->
         let assignement =
           {
             username;
             assigned_to = List.length assigned_to;
             reviewer_of = List.length reviewer_of;
             author_of = List.length author_of;
           }
         in
         Long_test.add_data_point
           (Row_schema.to_data_point1 ~timestamp assignments_schema assignement)
      ) ;
      return ())
  in

  unit

let register ~executors = assignment_load ~executors
