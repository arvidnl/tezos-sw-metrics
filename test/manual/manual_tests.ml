open Tezt
open Tezt.Base
open Tezos_sw_metrics

let tezos_tezos_project = 3836952

let () =
  Test.register
    ~__FILE__
    ~title:"Git TESTOWNERS.json -- remote repo"
    ~tags:["testowners"; "manual"; "slow"; "remote"]
  @@ fun () ->
  let sha = "b79d2372f880f957d6bc4a0e40b7a65ee4fd3970" in
  let repo_url = "https://gitlab.com/tezos/tezos.git" in
  let* testowners_res = Git_testowners.of_commit_res ~repo_url ~sha in
  match testowners_res with
  | Result.Ok _testowners -> unit
  | Result.Error _e ->
      Test.fail
        "Expected to find a valid TESTOWNERS.json file in commit %s of the \
         repository at %s"
        sha
        repo_url

let () =
  Test.register
    ~__FILE__
    ~title:"GitLab fetch Tezt records -- remote pipeline"
    ~tags:["tezt_records"; "manual"; "slow"; "remote"]
  @@ fun () ->
  (* We only keep records for 3 days. So this [default] needs to change often. *)
  let pipeline_id = Tezt.Cli.get_int ~default:1051040615 "pipeline_id" in
  let* records =
    Gitlab.Monad.run
    @@ Gitlab_tezt_records.of_pipeline
         ~token:(Gitlab.Token.of_string @@ Sys.getenv "GL_ACCESS_TOKEN")
         ~project_id:tezos_tezos_project
         ~pipeline_id
  in
  let test_title =
    "alpha: Smart rollup types octez conversions: roundtrip (roundtrip inbox)"
  in
  match Tezt_record.lookup test_title records with
  | Some
      {
        file =
          "src/proto_alpha/lib_sc_rollup_node/test/test_octez_conversions.ml";
        title;
        tags = ["alcotezt"; "slow"; "alpha"];
      }
    when title = test_title ->
      unit
  | _ ->
      Test.fail
        "Couldn't find the records in pipeline %d, or the records did not \
         contain the test %S"
        pipeline_id
        test_title

let () =
  Test.register
    ~__FILE__
    ~title:"GitLab fetch Tezt records -- remote pipeline job"
    ~tags:["tezt_records"; "manual"; "slow"; "remote"]
  @@ fun () ->
  (* We only keep records for 3 days. So this [default] needs to change often. *)
  let job_id, job_name =
    ( Tezt.Cli.get_int ~default:5385752760 "job_id",
      Tezt.Cli.get_string ~default:"tezt 10/60" "job_name" )
  in
  let* records_opt =
    Gitlab_tezt_records.of_pipeline_job
      ~project_id:tezos_tezos_project
      ~job_id
      ~job_name
  in
  let records =
    match records_opt with
    | None -> Test.fail "Couldn't find any records in the job %d" job_id
    | Some records -> records
  in
  let test_title = "kernel tx_no_verify run (hash, v1)" in
  match Tezt_record.lookup test_title records with
  | Some
      {
        file = "src/lib_scoru_wasm/regressions/tezos_scoru_wasm_regressions.ml";
        title;
        tags = ["regression"; "wasm_2_0_0"; "tx_no_verify"; "hash"; "v1"];
      }
    when title = test_title ->
      unit
  | Some {file; title; tags} ->
      Test.fail
        "Unexpected file (%s), title (%s) or tags (%s)"
        file
        title
        (String.concat "," tags)
  | _ ->
      Test.fail
        "The records of job %d did not contain the test %S"
        job_id
        test_title

let () = Test.run ()
