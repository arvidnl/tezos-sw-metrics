FROM ocaml/opam:alpine-ocaml-4.14 as builder

WORKDIR tezos_sw_metrics

RUN sudo apk add gmp-dev libev-dev

COPY tezos_sw_metrics.opam tezos_sw_metrics.opam.locked ./

ARG OPAMSOLVERTIMEOUT
ENV OPAMSOLVERTIMEOUT=${OPAMSOLVERTIMEOUT:-0}

RUN opam install --deps-only --with-test . --yes --locked

COPY . .

RUN sudo chown -R 1000:1000 $(pwd)

RUN opam exec dune build bin/main.exe reports/main.exe

FROM alpine

WORKDIR tezos_sw_metrics

RUN apk add gmp-dev libev-dev cloc git

COPY --from=builder "/home/opam/tezos_sw_metrics/_build/default/bin/main.exe" /glcisk/bin/metrics.exe

COPY --from=builder "/home/opam/tezos_sw_metrics/_build/default/reports/main.exe" /glcisk/bin/reports.exe

ENV PATH="${PATH}:/glcisk/bin/"
