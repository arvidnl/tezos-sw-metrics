#!/bin/sh

set -eu

cat << EOF
{
    "influxdb": {
        "url": "http://$INFLUXDB_DOMAIN:8086",
        "database": "$INFLUXDB_DATABASE",
        "measurement_prefix": "",
        "timeout": 20,
        "credentials": {
            "username": "$INFLUXDB_USERNAME",
            "password": "$INFLUXDB_PASSWORD"
        }
    },
    "grafana": {
        "url": "https://$GRAFANA_DOMAIN/api",
        "api_token": "$GRAFANA_TOKEN",
        "data_source": "InfluxDB",
        "timeout": 30
    },
    "reports": {
        "slack_webhook_urls": {
            "default": "${SLACK_WEBHOOK_URL_DEFAULT}",
            "infrastructure": "${SLACK_WEBHOOK_URL_INFRASTRUCTURE:-$SLACK_WEBHOOK_URL_DEFAULT}",
            "layer1": "${SLACK_WEBHOOK_URL_LAYER1:-$SLACK_WEBHOOK_URL_DEFAULT}",
            "tezos2": "${SLACK_WEBHOOK_URL_TEZOS2:-$SLACK_WEBHOOK_URL_DEFAULT}",
            "etherlink": "${SLACK_WEBHOOK_URL_ETHERLINK:-$SLACK_WEBHOOK_URL_DEFAULT}"
        }
    },
    "test_data_path" : "/path/to/your/folder"
}
EOF
