(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Tezt

type direction = Asc | Desc

type reducer = Sum

type bin_op = Div

type operation = Binary of {bin_op : bin_op; left : string; right : string}

type transform =
  | Merge
  | Sort_by of {field : string; direction : direction}
  | Calculate_field of {
      reducer : reducer;
      operation : operation;
      replace_fields : bool;
    }

val of_list : transform list -> JSON.u option
