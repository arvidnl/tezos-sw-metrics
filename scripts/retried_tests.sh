#!/bin/bash

if [ -n "${TRACE:-}" ]; then set -x; fi

set -eu

LIMIT=${LIMIT:-500}

outfile=retried_tests_"$(date +%Y-%m-%d)"
outfile="$outfile.log"

echo "Output in $outfile"
if [ -f _build/default/bin_scripts/retried_tests.exe ]; then
    cmd=_build/default/bin_scripts/retried_tests.exe;
else
    cmd="dune exec ./bin_scripts/retried_tests.exe --"
fi
$cmd \
             --private-token "${GL_ACCESS_TOKEN:-}" \
             --limit "$LIMIT" \
             --failure-reason script_failure \
             $(if [ -n "${GLUSERNAME:-}" ]; then echo "--username $GLUSERNAME"; fi) \
             "$@" \
    | tee "$outfile"
